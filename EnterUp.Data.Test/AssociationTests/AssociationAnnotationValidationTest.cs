﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.AssociationTests {
    /// <summary>
    /// This class checks all of the annotation validation for the Association class. 
    /// </summary>
    public class AssociationAnnotationValidationTest {
        [Fact]
        public void NonMemberDrawPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                NonMemberDrawPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Draw Priority",
                "The value for Non-Member Draw Priority must be between 1 and 9.");
            testObject.NonMemberDrawPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Draw Priority");
        }

        [Fact]
        public void NonMemberDrawPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                NonMemberDrawPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Draw Priority");

            testObject.NonMemberDrawPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Draw Priority",
                "The value for Non-Member Draw Priority must be between 1 and 9.");
        }

        [Fact]
        public void PermitDrawPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                PermitDrawPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Permit Draw Priority",
                "The value for Permit Draw Priority must be between 1 and 9.");
            testObject.PermitDrawPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Permit Draw Priority");
        }

        [Fact]
        public void PermitDrawPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                PermitDrawPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Permit Draw Priority");

            testObject.PermitDrawPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Permit Draw Priority",
                "The value for Permit Draw Priority must be between 1 and 9.");
        }

        [Fact]
        public void MemberDrawPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MemberDrawPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Member Draw Priority",
                "The value for Member Draw Priority must be between 1 and 9.");
            testObject.MemberDrawPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Member Draw Priority");
        }

        [Fact]
        public void MemberDrawPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MemberDrawPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Member Draw Priority");

            testObject.MemberDrawPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Member Draw Priority",
                "The value for Member Draw Priority must be between 1 and 9.");
        }

        
        [Fact]
        public void NonMemberSchedulingPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                NonMemberSchedulingPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Scheduling Priority",
                "The value for Non-Member Scheduling Priority must be between 1 and 9.");
            testObject.NonMemberSchedulingPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Scheduling Priority");
        }

        [Fact]
        public void NonMemberSchedulingPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                NonMemberSchedulingPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Scheduling Priority");

            testObject.NonMemberSchedulingPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Non Member Scheduling Priority",
                "The value for Non-Member Scheduling Priority must be between 1 and 9.");
        }

        [Fact]
        public void PermitSchedulingPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                PermitSchedulingPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Permit Scheduling Priority",
                "The value for Permit Scheduling Priority must be between 1 and 9.");
            testObject.PermitSchedulingPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Permit Scheduling Priority");
        }

        [Fact]
        public void PermitSchedulingPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                PermitSchedulingPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Permit Scheduling Priority");

            testObject.PermitSchedulingPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Permit Scheduling Priority",
                "The value for Permit Scheduling Priority must be between 1 and 9.");
        }

        [Fact]
        public void MemberSchedulingPriority_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MemberSchedulingPriority = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Member Scheduling Priority",
                "The value for Member Scheduling Priority must be between 1 and 9.");
            testObject.MemberSchedulingPriority = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Member Scheduling Priority");
        }

        [Fact]
        public void MemberSchedulingPriority_Max() {
            const int value = 9;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MemberSchedulingPriority = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Member Scheduling Priority");

            testObject.MemberSchedulingPriority = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Member Scheduling Priority",
                "The value for Member Scheduling Priority must be between 1 and 9.");
        }

        [Fact]
        public void MaximumBuddies_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MaximumBuddies = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Maximum Buddies",
                "The value for Maximum Buddies must be between 1 and 10.");

            testObject.MaximumBuddies = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Maximum Buddies");
        }

        [Fact]
        public void MaximumBuddies_Max() {
            const int value = 10;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                MaximumBuddies = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Maximum Buddies");

            testObject.MaximumBuddies = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Maximum Buddies",
                "The value for Maximum Buddies must be between 1 and 10.");
        }

        [Fact]
        public void ContactEmail_Blank_No_Error() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactEmail = string.Empty
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Contact Email");
            testObject.ContactEmail = null;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Contact Email");
        }

        [Fact]
        public void ContactEmail_Max_Length() {
            const int length = 255;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactEmail = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Email",
                "The Contact Email field has a maximum length of 255 characters.");

            testObject.ContactEmail += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Email",
                "The Contact Email field has a maximum length of 255 characters.");
        }

        [Fact]
        public void EntryPhone_Blank_No_Error() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                EntryPhone = string.Empty
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Entry Phone");
            testObject.EntryPhone = null;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Entry Phone");
        }
        [Fact]
        public void EntryPhone_Max_Length() {
            const int length = 20;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                EntryPhone = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Entry Phone",
                "The Entry Phone field has a maximum length of 20 characters.");

            testObject.EntryPhone += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Entry Phone",
                "The Entry Phone field has a maximum length of 20 characters.");
        }

        [Fact]
        public void ContactPhone_Blank_No_Error() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactPhone = string.Empty
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Contact Phone");
            testObject.ContactPhone = null;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Contact Phone");
        }
        [Fact]
        public void ContactPhone_Max_Length() {
            const int length = 20;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactPhone = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Phone",
                "The Contact Phone field has a maximum length of 20 characters.");

            testObject.ContactPhone += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Phone",
                "The Contact Phone field has a maximum length of 20 characters.");
        }

        [Fact]
        public void ContactOrganization_Null() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactOrganization = null
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactOrganization_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactOrganization = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactOrganization_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactOrganization = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");

            testObject.ContactOrganization += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactOrganization_Max_Length() {
            const int length = 255;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactOrganization = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");

            testObject.ContactOrganization += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Organization",
                "The Contact Organization field is require and must be between 5 and 255 characters.");
        }

        
        [Fact]
        public void ContactName_Null() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactName = null
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactName_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactName = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactName_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactName = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");

            testObject.ContactName += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void ContactName_Max_Length() {
            const int length = 255;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                ContactName = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");

            testObject.ContactName += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contact Name",
                "The Contact Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void Abbreviation_Null() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                Abbreviation = null
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");
        }

        [Fact]
        public void Abbreviation_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                Abbreviation = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");
        }

        [Fact]
        public void Abbreviation_Min_Length() {
            const int length = 2;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                Abbreviation = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");

            testObject.Abbreviation += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");
        }

        [Fact]
        public void Abbreviation_Max_Length() {
            const int length = 50;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association testObject = new Association {
                Abbreviation = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");

            testObject.Abbreviation += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Abbreviation",
                "The Abbreviation field is require and must be between 2 and 50 characters.");
        }

        [Fact]
        public void Name_Null() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association association = new Association {
                Name = null
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void Name_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association association = new Association {
                Name = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void Name_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association association = new Association {
                Name = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");

            association.Name += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");
        }

        [Fact]
        public void Name_Max_Length() {
            const int length = 255;
            ValidationHelper validationHelper = new ValidationHelper(null);
            Association association = new Association {
                Name = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");

            association.Name += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(association),
                "Name",
                "The Name field is require and must be between 5 and 255 characters.");
        }
    }
}