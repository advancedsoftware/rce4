﻿using System.Linq;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.AssociationTests {
    /// <summary>
    /// This class will test the basic CRUD operations for an association.
    /// </summary>
    public class AssociationDataOperationTest : BaseTestClass {
        [Fact]
        public void ValidAssociationSaved() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            RodeoContext.Associations.Add(AssociationGenerator.CreateAssociation(1));
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                Assert.Equal(1, db.Associations.Count());
            }
        }
    }
}
