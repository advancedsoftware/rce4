﻿using Xunit;

namespace EnterUp.Data.Test.AssociationTests
{
    /// <summary>
    /// This class is used to test any property marked as NotMapped.
    /// </summary>
    public class AssociationPropertyTest {
        [Fact]
        public void IsDeletedFalse_Null() {
            Association association = new Association { DeletedById = null };
            Assert.False(association.IsDeleted);
        }

        [Fact]
        public void IsDeletedTrue() {
            Association association = new Association { DeletedById = "1" };
            Assert.True(association.IsDeleted);

        }
    }
}
