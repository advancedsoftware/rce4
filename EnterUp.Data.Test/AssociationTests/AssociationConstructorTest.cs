﻿using Xunit;

namespace EnterUp.Data.Test.AssociationTests {
    public class AssociationConstructorTest {
        [Fact]
        public void NameBlank() {
            Assert.Equal(string.Empty, new Association().Name);
        }
        [Fact]
        public void AbbreviationBlank() {
            Assert.Equal(string.Empty, new Association().Abbreviation);
        }
        [Fact]
        public void ContactNameBlank() {
            Assert.Equal(string.Empty, new Association().ContactName);
        }
        [Fact]
        public void ContactOrganizationBlank() {
            Assert.Equal(string.Empty, new Association().ContactOrganization);
        }
        [Fact]
        public void ContactPhoneBlank() {
            Assert.Equal(string.Empty, new Association().ContactPhone);
        }
        [Fact]
        public void DeletedByNull() {
            Assert.Null(new Association().DeletedById);
        }
        [Fact]
        public void EntryPhoneBlank() {
            Assert.Equal(string.Empty, new Association().EntryPhone);
        }
        [Fact]
        public void ContactEmailBlank() {
            Assert.Equal(string.Empty, new Association().ContactEmail);
        }
        [Fact]
        public void MaximumBuddies4() {
            Assert.Equal(4, new Association().MaximumBuddies);
        }
        [Fact]
        public void MemberSchedulingPriority1() {
            Assert.Equal(1, new Association().MemberSchedulingPriority);
        }
        [Fact]
        public void PermitSchedulingPriority2() {
            Assert.Equal(2, new Association().PermitSchedulingPriority);
        }
        [Fact]
        public void NonMemberSchedulingPriority4() {
            Assert.Equal(4, new Association().NonMemberSchedulingPriority);
        }
        [Fact]
        public void MemberDrawPriority1() {
            Assert.Equal(1, new Association().MemberDrawPriority);
        }
        [Fact]
        public void PermitDrawPriority2() {
            Assert.Equal(2, new Association().PermitDrawPriority);
        }
        [Fact]
        public void NonMemberDrawPriority() {
            Assert.Equal(4, new Association().NonMemberDrawPriority);
        }
        [Fact]
        public void IsPartnerAllowedOutsideBuddyGroupFalse() {
            Assert.False(new Association().IsPartnerAllowedOutsideBuddyGroup);
        }
        [Fact]
        public void AllowPermitsToEnterOnlineFalse() {
            Assert.False(new Association().AllowPermitsToEnterOnline);
        }
        [Fact]
        public void AllowNonMembersToEnterOnlineFalse() {
            Assert.False(new Association().AllowNonMembersToEnterOnline);
        }
        [Fact]
        public void IsContestantUserCreatedFalse() {
            Assert.False(new Association().IsContestantUserCreated);
        }
        [Fact]
        public void IsFirstPreferenceSlackAllowedFalse() {
            Assert.False(new Association().IsFirstPreferenceSlackAllowed);
        }
        [Fact]
        public void IsEntryFormDownLoaded() {
            Assert.False(new Association().IsEntryFormDownLoaded);
        }
        [Fact]
        public void IsAgeVerifiedFalse() {
            Assert.False(new Association().IsAgeVerified);
        }
        [Fact]
        public void IsPaymentVerifiedFalse() {
            Assert.False(new Association().IsPaymentVerified);
        }
        [Fact]
        public void IsPointsFalse() {
            Assert.False(new Association().IsPoints);
        }
        [Fact]
        public void IsActiveTrue() {
            Assert.True(new Association().IsActive);
        }

        [Fact]
        public void AssociationEvents_Not_Null () {
            Assert.NotNull(new Association().AssociationEvents);
        }
    }
}
