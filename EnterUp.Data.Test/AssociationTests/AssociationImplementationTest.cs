﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.AssociationTests {
    public class AssociationImplementationTest {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Association() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Association() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Association() as IValidator);
        }
    }
}
