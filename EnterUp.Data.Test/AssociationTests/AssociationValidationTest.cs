﻿using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.AssociationTests {
    /// <summary>
    /// This class checks all of the annotation validation for the Association class. 
    /// </summary>
    public class AssociationValidationTest : BaseTestClass {
        public AssociationValidationTest() {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
        }

        [Fact]
        public void PropertyErrors_Populated () {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Association association = AssociationGenerator.CreateAssociation(1);
                association.Name = string.Empty;
                EnterUpDataException ex = 
                    Assert.Throws<EnterUpDataException>(() => association.Validate(context.ValidationHelper));
                Assert.NotNull(ex.PropertyErrors);
                Assert.Single(ex.PropertyErrors);
            }
        }

        [Fact]
        public void GetAnnotationErrorsCalled () {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
            var validationHelper = Substitute.For<ValidationHelper>(RodeoContext);
            validationHelper.When(v => v.GetAnnotationErrors(Arg.Any<object>())).DoNotCallBase();
            RodeoContext.ValidationHelper = validationHelper;
            RodeoContext.Add(AssociationGenerator.CreateAssociation(1));
            RodeoContext.SaveChanges("1");
            validationHelper.Received(1).GetAnnotationErrors(Arg.Any<object>());
        }

        [Fact]
        public void ExceptionThrowIfInvalid () {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Association association = AssociationGenerator.CreateAssociation(1);
                association.Name = string.Empty;
                Assert.Throws<EnterUpDataException>(() => association.Validate(context.ValidationHelper));
            }
        }

        [Fact]
        public void NoExceptionIfValid () {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Association association = AssociationGenerator.CreateAssociation(1);
                association.Validate(context.ValidationHelper);
            }
        }
    }
}