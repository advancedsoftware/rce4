﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.IValidatorTests {
    /// <summary>
    /// This class will verify the SaveChanges method handles the IValidator entities correctly.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class IValidatorSaveTest : BaseTestClass {
        [Fact]
        public void GetIValidators_Deleted_Not_Returned() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Associations.Add(AssociationGenerator.CreateAssociation(1));
                context.SaveChanges("1");
                List<AssociationEvent> events = AssociationEventGenerator.GetList(1);
                foreach (AssociationEvent associationEvent in events) {
                    context.AssociationEvents.Add(associationEvent);
                }
                context.SaveChanges("1");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                List<AssociationEvent> associationEvents = context.AssociationEvents.ToList();
                context.Entry(associationEvents[0]).State = EntityState.Deleted;
                Assert.Empty(context.GetIValidators());
            }
        }
        [Fact]
        public void GetIValidators_Unmodified_Not_Returned() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Associations.Add(AssociationGenerator.CreateAssociation(1));
                context.SaveChanges("1");
                List<AssociationEvent> events = AssociationEventGenerator.GetList(1);
                foreach (AssociationEvent associationEvent in events) {
                    context.AssociationEvents.Add(associationEvent);
                }
                context.SaveChanges("1");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Assert.Empty(context.GetIValidators());
            }
        }
        [Fact]
        public void GetIValidators_Modified_Returned() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Associations.Add(AssociationGenerator.CreateAssociation(1));
                context.SaveChanges("1");
                List<AssociationEvent> events = AssociationEventGenerator.GetList(1);
                foreach (AssociationEvent associationEvent in events) {
                    context.AssociationEvents.Add(associationEvent);
                }
                context.SaveChanges("1");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                List<AssociationEvent> associationEvent = context.AssociationEvents.ToList();
                associationEvent[0].Name += "A";
                Assert.Single(context.GetIValidators());
            }
        }
        [Fact]
        public void GetIValidators_Added_Returned() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Associations.Add(AssociationGenerator.CreateAssociation(1));
                context.SaveChanges("1");
                List<AssociationEvent> events = AssociationEventGenerator.GetList(1);
                foreach (AssociationEvent associationEvent in events) {
                    context.AssociationEvents.Add(associationEvent);
                }
                Assert.Equal(5, context.GetIValidators().Count());
            }
        }
        [Fact]
        public void GetIValidators_No_Exception_New_Context() {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Assert.Empty(context.GetIValidators());
            }
        }
        [Fact]
        public void GetIValidators_Returns_Empty_List() {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Assert.Empty(context.GetIValidators());
            }
        }
        [Fact]
        public void GetIValidators_Returns_Single() {
            List<AssociationEvent> testObjects = AssociationEventGenerator.GetList(1);
            RodeoContext.AssociationEvents.Add(testObjects[0]);
            Assert.Single(RodeoContext.GetIValidators());
        }
        [Fact]
        public void GetIValidators_Returns_Multiple() {
            List<AssociationEvent> testObjects = AssociationEventGenerator.GetList(1);
            RodeoContext.AssociationEvents.Add(testObjects[0]);
            RodeoContext.AssociationEvents.Add(testObjects[1]);
            RodeoContext.AssociationEvents.Add(testObjects[2]);
            Assert.Equal(3, RodeoContext.GetIValidators().Count());
        }
        [Fact]
        public void GetIValidators_Returns_Only_IValidators() {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Add(new AssociationEvent());
                context.Add(new AssociationEvent());
                context.Add(new UserClaim());
                context.Add(new UserClaim());
                context.Add(new UserClaim());
                Assert.Equal(2, context.GetIValidators().Count());
            }
        }
        [Fact]
        public void HandleIValidators_Called_With_GetIValidators() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>(GetContextBuilderForTest().Options)) {
                context.When(c => c.GetIValidators()).Throw<ArithmeticException>();
                List<AssociationEvent> testObjects = AssociationEventGenerator.GetList(1);
                // ReSharper disable once RedundantCast
                // ReSharper disable once TryCastAlwaysSucceeds
                Assert.NotNull(testObjects[0] as IValidator);
                foreach (AssociationEvent associationEvent in testObjects) {
                    context.AssociationEvents.Add(associationEvent);
                }
                Assert.Throws<ArithmeticException>(() => context.SaveChanges("1"));
            }
        }
        [Fact]
        public void HandleIValidatorCalled_Once() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>(GetContextBuilderForTest().Options)) {
                context.When(c => c.HandleIValidators(Arg.Any<IEnumerable<IValidator>>())).DoNotCallBase();
                AssociationEvent testObject = AssociationEventGenerator.GetList(1)[2];
                testObject.Name = "HandleIValidatorCalled_Once";
                // ReSharper disable once RedundantCast
                // ReSharper disable once TryCastAlwaysSucceeds
                Assert.NotNull(testObject as IValidator);
                context.AssociationEvents.Add(testObject);
                context.SaveChanges("1");
                context.Received(1).HandleIValidators(Arg.Any<IEnumerable<IValidator>>());
            }
        }

        [Fact]
        public void HandleIValidatorCalls_Validate_SingleObject() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>()) {
                AssociationEvent testObject = Substitute.For<AssociationEvent>();
                testObject.When(t => t.Validate(Arg.Any<ValidationHelper>())).DoNotCallBase();
                List<AssociationEvent> iValidators = new List<AssociationEvent> { testObject };
                context.HandleIValidators(iValidators);
                testObject.Received(1).Validate(Arg.Any<ValidationHelper>());
            }
        }

        [Fact]
        public void HandleIValidatorCalls_Validate_MultipleObject() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>()) {
                AssociationEvent testObject = Substitute.For<AssociationEvent>();
                testObject.When(t => t.Validate(Arg.Any<ValidationHelper>())).DoNotCallBase();
                List<AssociationEvent> iValidators = new List<AssociationEvent> { testObject, testObject, testObject };
                context.HandleIValidators(iValidators);
                testObject.Received(3).Validate(Arg.Any<ValidationHelper>());
            }
        }
        [Fact]
        public void CurrentContextValidationObjectPassed() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>()) {
                IValidationHelper helper = context.ValidationHelper;
                AssociationEvent testObject = Substitute.For<AssociationEvent>();
                testObject.When(t => t.Validate(helper)).DoNotCallBase();
                List<AssociationEvent> iValidators = new List<AssociationEvent> { testObject, testObject, testObject };
                context.HandleIValidators(iValidators);
                //Verified this one fails.
                //testObject.Received(3).Validate(new ValidationHelper(context));
                testObject.Received(3).Validate(context.ValidationHelper);
            }
        }
        [Fact]
        public void ExceptionNotHandled() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>()) {
                AssociationEvent testObject = Substitute.For<AssociationEvent>();
                testObject.When(t => t.Validate(Arg.Any<ValidationHelper>())).Throw<Exception>();
                List<AssociationEvent> iValidators = new List<AssociationEvent> { testObject, testObject, testObject };
                Assert.Throws<Exception>(() => context.HandleIValidators(iValidators));
            }
        }
        [Fact]
        public void EnterUpDataExceptionNotHandled() {
            using (RodeoContext context = Substitute.ForPartsOf<RodeoContext>()) {
                AssociationEvent testObject = Substitute.For<AssociationEvent>();
                testObject.When(t => t.Validate(Arg.Any<ValidationHelper>())).Throw<EnterUpDataException>();
                List<AssociationEvent> iValidators = new List<AssociationEvent> { testObject, testObject, testObject };
                Assert.Throws<EnterUpDataException>(() => context.HandleIValidators(iValidators));
            }
        }
        [Fact]
        public void NotIValidatorNoError() {
            UserClaim testObject = new UserClaim {
                UserId = 1,
                ClaimType = 1,
                AssociationId = 0
            };
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.Null(testObject as IValidator);
            RodeoContext.UserClaims.Add(testObject);
            RodeoContext.SaveChanges("1");
            //No Exceptions is a pass.
        }
    }
}
