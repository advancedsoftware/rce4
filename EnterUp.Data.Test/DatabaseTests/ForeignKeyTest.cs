﻿using System.Data;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.DatabaseTests
{
    /// <summary>
    /// This class tests that database constaints are applied to the correct properties.
    /// </summary>
    public class ForeignKeyTest : BaseTestClass {

        #region RodeoAssociation Tests
        //This region contains the tests used to validate the FK relationships for a rodeo entry.
        [Fact]
        public void RodeoAssociation_RodeoId_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoAssociations", "Rodeos", "RodeoId"));
        }
        [Fact]
        public void RodeoAssociation_AssociationId_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoAssociations", "Associations", "AssociationId"));
        }
        #endregion RodeoAssociation Tests

        #region RodeoEntry Tests
        //This region contains the tests used to validate the FK relationships for a rodeo entry.
        [Fact]
        public void RodeoEntry_FirstPerformance_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntries", "Performances", "FirstPreferenceId"));
        }
        [Fact]
        public void RodeoEntry_SecondPerformance_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntries", "Performances", "SecondPreferenceId"));
        }
        [Fact]
        public void RodeoEntry_Rodeo_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntries", "Rodeos", "RodeoId"));
        }
        #endregion RodeoEntry Tests
        #region RodeoEvent FK Tests
        [Fact]
        public void RodeoEvent_Rodeo_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEvents", "Rodeos", "RodeoId"));
        }
        [Fact]
        public void RodeoEvent_AssociationEvent_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEvents", "AssociationEvents", "AssociationEventId"));
        }
        #endregion RodeoEvent FK Tests
        #region Rodeos
        [Fact]
        public void Rodeo_Association_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "Rodeos", "Associations", "AssociationId"));
        }
        #endregion Rodeos

        [Fact]
        public void Performance_Rodeo_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "Performances", "Rodeos", "RodeoId"));
        }

        [Fact]
        public void Constestant_AssociationId_ForeignKey_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "Contestants", "Associations", "AssociationId"));
        }

        [Fact]
        public void Contestant_DemographicId_ForeignKey_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "Contestants", "Demographics", "DemographicId"));
        }

        private static bool ForeignKeyExists(RodeoContext dbContext, string parentTableName, string childTableName, string indexColumnName) {
            string query =
                "Select Count(1) from sys.Tables t Join sys.Foreign_Keys fk on fk.parent_object_id = t.object_id"
                + $" Where fk.Name = 'FK_{parentTableName}_{childTableName}_{indexColumnName}'";

            bool hasKey;

            using (DbConnection connection = dbContext.Database.GetDbConnection()) {
                connection.Open();
                using (DbCommand command = connection.CreateCommand()) {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();
                    hasKey = reader.Read() && (int)reader[0] > 0;
                    reader.Dispose();
                }
                connection.Close();
            }
            return hasKey;
        }
    }
}
