﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.DatabaseTests
{
    /// <summary>
    /// This class tests that database constaints are applied to the correct properties.
    /// </summary>
    public class UniqueIndexTest {
        [Fact]
        private void Performances_RodeoId_PerformanceDate_IsSlack_Unique() {
            using (RodeoContext db = new RodeoContext()) {
                Assert.True(UniqueIndexExists(db,
                    "Performances",
                    new List<string> { "RodeoId", "PerformanceDate", "IsSlack" }));
            }
        }
        [Fact]
        private void CardNumber_and_AssociationId_must_be_unique() {
            using (RodeoContext db = new RodeoContext()) {
                Assert.True(UniqueIndexExists(db,
                    "Contestants",
                    new List<string> { "Association", "CardNumber" }));
            }
        }

        [Fact]
        private void Association_Abbreviation_Unique_Index() {
            using (RodeoContext db = new RodeoContext()) {
                Assert.True(UniqueIndexExists(db,
                    "Associations",
                    new List<string> { "Abbreviation" }));
            }
        }
        [Fact]
        private void Association_Name_Unique_Index() {
            using (RodeoContext db = new RodeoContext()) {
                Assert.True(UniqueIndexExists(db,
                    "Associations",
                    new List<string> { "Name" }));
            }
        }

        public static bool UniqueIndexExists(RodeoContext dbContext, string tableName, List<string> columnList) {
            string columnQuery = string.Empty;
            foreach (string columnName in columnList) {
                columnQuery += $"and ind.Name like '%{columnName}%'";
            }
            string query =
                "SELECT Count(*) FROM sys.indexes ind "
                + "INNER JOIN sys.index_columns ic ON ind.object_id = ic.object_id and ind.index_id = ic.index_id "
                + "INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id "
                + "INNER JOIN sys.tables t ON ind.object_id = t.object_id "
                + "WHERE "
                + "ind.is_primary_key = 0 "
                + "AND ind.is_unique = 1 "
                + "AND ind.is_unique_constraint = 0 "
                + "AND t.is_ms_shipped = 0 "
                + $"{columnQuery}"
                + $"AND t.Name = '{tableName}'";

            bool hasKey;

            using (DbConnection connection = dbContext.Database.GetDbConnection()) {
                connection.Open();
                using (DbCommand command = connection.CreateCommand()) {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();
                    hasKey = reader.Read() && (int)reader[0] > 0;
                    reader.Dispose();
                }
                connection.Close();
            }
            return hasKey;
        }
    }
}
