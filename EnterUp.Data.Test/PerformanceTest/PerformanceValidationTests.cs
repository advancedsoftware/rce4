﻿using System;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.PerformanceTest {
    public class PerformanceValidationTests : BaseTestClass {
        protected Performance Performance { get; set; }
        public PerformanceValidationTests() {
            Performance = new Performance {
                RodeoId = 20,
                PerformanceDate = DateTime.Today.AddHours(18),
            };
        }
        [Fact]
        public void Performance_Date_Must_Be_Between_Rodeo_Start_And_End_Dates() {
            var helper = Substitute.For<IValidationHelper>();
            helper.When(h => h.IsPerformanceDateBetweenRodeoDates(Arg.Any<int>(), Arg.Any<DateTime>())).DoNotCallBase();
            helper.IsPerformanceDateBetweenRodeoDates(Performance.RodeoId, Performance.PerformanceDate)
                .ReturnsForAnyArgs(false);
            TestHelpers.ErrorContains(Performance, 
                helper, 
                "Performance Date", "The performance date must be between the rodeo start and end dates.");

            helper.IsPerformanceDateBetweenRodeoDates(Performance.RodeoId, Performance.PerformanceDate)
                .ReturnsForAnyArgs(true);

            TestHelpers.NoError(Performance, helper);
        }

        [Fact]
        public void IsFollowingPerformance_Set_False_When_IsSlack_Is_False() {
            var helper = Substitute.For<IValidationHelper>();
            helper.When(h => h.IsPerformanceDateBetweenRodeoDates(Arg.Any<int>(), Arg.Any<DateTime>())).DoNotCallBase();
            helper.IsPerformanceDateBetweenRodeoDates(Performance.RodeoId, Performance.PerformanceDate)
                .ReturnsForAnyArgs(true);
            Performance.IsFollowingPerformance = true;
            Performance.IsSlack = false;
            Performance.Validate(helper);
            Assert.False(Performance.IsFollowingPerformance);
        }

        [Fact]
        public void IsFollowingPerformance_Keeps_Value_When_IsSlack_Is_True() {
            var helper = Substitute.For<IValidationHelper>();
            helper.When(h => h.IsPerformanceDateBetweenRodeoDates(Arg.Any<int>(), Arg.Any<DateTime>())).DoNotCallBase();
            helper.IsPerformanceDateBetweenRodeoDates(Performance.RodeoId, Performance.PerformanceDate)
                .ReturnsForAnyArgs(true);
            Performance.IsFollowingPerformance = true;
            Performance.IsSlack = true;
            Performance.Validate(helper);
            Assert.True(Performance.IsFollowingPerformance);
            Performance.IsFollowingPerformance = false;
            Performance.Validate(helper);
            Assert.False(Performance.IsFollowingPerformance);
        }

        
    }
}
