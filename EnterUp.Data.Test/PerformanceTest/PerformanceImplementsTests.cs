﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.PerformanceTest {
    public class PerformanceImplementsTests {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Performance() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Performance() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Performance() as IValidator);
        }
    }
}