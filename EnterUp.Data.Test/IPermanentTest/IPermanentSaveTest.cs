﻿using System.Linq;
using EnterUp.Test.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.IPermanentTest
{
    /// <summary>
    /// This class will verify the SaveChanges method handles IPermanent objects correctly.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class IPermanentSaveTest : BaseTestClass {
        [Fact]
        public void ObjectNotDeleted() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            RodeoContext.Associations.Add(AssociationGenerator.CreateAssociation(1));
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                Association association = db.Associations.First();
                db.Entry(association).State = EntityState.Deleted;
                db.SaveChanges("1");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Assert.NotNull(db.Associations.First());
            }
        }
        [Fact]
        public void ObjectDeletedBySet() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            RodeoContext.Associations.Add(AssociationGenerator.CreateAssociation(1));
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                Association association = db.Associations.First();
                db.Entry(association).State = EntityState.Deleted;
                db.SaveChanges("1");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Assert.Equal("1", db.Associations.First().DeletedById);
            }
        }
        [Fact]
        public void ObjectIsDeletedCorrect() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            RodeoContext.Associations.Add(AssociationGenerator.CreateAssociation(1));
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                Association association = db.Associations.First();
                db.Entry(association).State = EntityState.Deleted;
                db.SaveChanges("1");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Assert.True(db.Associations.First().IsDeleted);
            }
        }
    }
}
