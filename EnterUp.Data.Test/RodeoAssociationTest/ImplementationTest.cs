﻿using EnterUp.Data.Interfaces;
using Xunit;

// ReSharper disable RedundantCast

namespace EnterUp.Data.Test.RodeoAssociationTest
{
    public class ImplementationTest {
        public readonly RodeoAssociation TestObject = new RodeoAssociation(); 
        [Fact]
        public void ImplementsIUpdateTracker() {
            Assert.NotNull(TestObject as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            Assert.NotNull(TestObject as IPermanent);
        }
    }
}
