﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.RodeoEventTest
{
    public class RodeoEventAnnotationValidationTests : BaseTestClass {
        public RodeoEvent TestEvent = new RodeoEvent {
            RodeoId = 1,
            AssociationEventId = 1,
            DailyCount = 1,
            SlackCount = 1,
            Fee = 10,
            MaximumEntryCount = 2
        };

        [Fact]
        public void MaximumEntryCount_Must_Be_Greater_Than__Or_Equal_One() {
            TestEvent.MaximumEntryCount = -1;
            TestHelpers.ValidationErrorsContains(TestEvent,
                "Maximum Entry Count",
                "The value for the maximum entry count must be greater than zero.");
            TestEvent.MaximumEntryCount = 0;
            TestHelpers.ValidationErrorsContains(TestEvent,
                "Maximum Entry Count",
                "The value for the maximum entry count must be greater than zero.");
            TestEvent.MaximumEntryCount = 1;
            TestHelpers.NoAnnotationError(TestEvent);
        }

        [Fact]
        public void SlackCount_Must_Not_Be_Negative() {
            TestEvent.SlackCount = -1;
            TestHelpers.ValidationErrorsContains(TestEvent,
                "Slack Count",
                "The value for the slack count may not be negative.");
            TestEvent.SlackCount = 0;
            TestHelpers.NoAnnotationError(TestEvent);
            TestEvent.SlackCount = 1;
            TestHelpers.NoAnnotationError(TestEvent);
        }

        [Fact]
        public void Fee_Must_Be_Greater_Or_Equal_Zero() {
            TestEvent.Fee = -0.00001;
            TestHelpers.ValidationErrorsContains(TestEvent,
                "Fee",
                "The value for fee may not be negative.");
            TestEvent.Fee = 0;
            TestHelpers.NoAnnotationError(TestEvent);
            TestEvent.Fee = 0.000001;
            TestHelpers.NoAnnotationError(TestEvent);
        }

        [Fact]
        public void DailyCount_Must_Be_Greater_Than_One() {
            TestEvent.DailyCount = 0;
            TestHelpers.ValidationErrorsContains(TestEvent,
                "Daily Count", 
                "The daily count must be greater than 0.");
            TestEvent.DailyCount = 1;
            TestHelpers.NoAnnotationError(TestEvent);
        }
    }
}
