﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.RodeoEventTest
{
    public class RodeEventValidateTests {
        public IValidationHelper Helper { get; set; }
        public RodeoEvent Test { get; set; }

        public RodeEventValidateTests() {
            Test = new RodeoEvent {
                RodeoId = 1,
                AssociationEventId = 1,
                DailyCount = 1,
                SlackCount = 1,
                Fee = 10,
                MaximumEntryCount = 2
            };
            Helper = Substitute.For<IValidationHelper>();
            Helper.When(h => h.GetAnnotationErrors(Arg.Any<IValidator>())).DoNotCallBase();
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(new List<PropertyError>());
            Helper.When(h => h.IsSlackForAssociationEvent(Arg.Any<int>())).DoNotCallBase();
            Helper.IsSlackForAssociationEvent(Arg.Any<int>()).Returns(true);
            Helper.When(h => h.IsRodeoAndAssociationEventInSameAssociation(Arg.Any<int>(), Arg.Any<int>())).DoNotCallBase();
            Helper.IsRodeoAndAssociationEventInSameAssociation(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
        }

        [Fact]
        public void Rodeo_And_Association_Event_Must_Be_From_Same_Association() {
            TestHelpers.NoError(Test, Helper);
            Helper.IsRodeoAndAssociationEventInSameAssociation(Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            TestHelpers.ErrorContains(Test, Helper,
                "Invalid Association",
                "The rodeo and the association event are in different associations.");
        }

        [Fact]
        public void SlackCount_Unchanged_If_AssociationEvent_Is_Slack() {
            Test.SlackCount = -10;
            Helper.IsSlackForAssociationEvent(Arg.Any<int>()).Returns(true);
            Test.Validate(Helper);
            Assert.Equal(-10, Test.SlackCount);
        }
        [Fact]
        public void SlackCount_Set_To_Zero_If_AssociationEvent_Is_Not_Slack() {
            Test.SlackCount = 10;
            Helper.IsSlackForAssociationEvent(Arg.Any<int>()).Returns(false);
            Test.Validate(Helper);
            Assert.Equal(0, Test.SlackCount);
        }

        [Fact]
        public void Validate_Failure_Exception_Text() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => Test.Validate(Helper));
            Assert.Equal("The rodeo event is invalid.", ex.Message);
        }

        [Fact]
        public void Validate_PropertyErrors_Set_To_ValidationHelper_GetAnnotationError() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            TestHelpers.ErrorContains(Test, Helper, propertyName, errorText);
        }
    }
}
