﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.RodeoEventTest {
    public class RodeoEventImplementsTests {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new RodeoEvent() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new RodeoEvent() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new RodeoEvent() as IValidator);
        }
    }
}