using System;
using System.IO;
using System.Linq;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace EnterUp.Data.Test {
    public class BaseTestClass : IDisposable {
        internal RodeoContext RodeoContext { get; set; }

        public BaseTestClass() {
            RodeoContext = GetRodeoContext();
        }

        public RodeoContext GetRodeoContext() {
            RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options);
            context.Database.EnsureCreated();
            // ReSharper disable once InvertIf
            if (!context.Users.Any()) {
                string queryPath = Path.Combine(
                    TestHelpers.SolutionBasePath, 
                    "EnterUp.Test.Common", 
                    "Queries",
                    "InitialUsersQuery.sql");
                string queryText = File.ReadAllText(queryPath);
                context.Database.ExecuteSqlCommand(queryText);
            }
            return context;
        }

        public DbContextOptionsBuilder<RodeoContext> GetContextBuilderForTest() {
            //get the connection string
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder
                .SetBasePath(Path.Combine(TestHelpers.SolutionBasePath, "EnterUp.Data.Test"))
                .AddJsonFile(@"appsettings.json", optional: false, reloadOnChange: true);
            ConfigurationRoot configuration = (ConfigurationRoot)configurationBuilder.Build();
            DbContextOptionsBuilder<RodeoContext> optionsBuilder = new DbContextOptionsBuilder<RodeoContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("RodeoContext"));
            return optionsBuilder;
        }

        public void Dispose() {
            RodeoContext?.Dispose();
        }

        protected void ContainsError(EnterUpDataException enterUpDataException, string propertyName, string errorMessage) {
            TestHelpers.ErrorContains(
                enterUpDataException.PropertyErrors,
                propertyName,
                errorMessage);
        }

        protected void ValidateThrows(ValidationHelper helper, IValidator validator, string propertyName, string errorText) {
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => validator.Validate(helper));
            Assert.NotNull(ex.PropertyErrors);
            Assert.Single(ex.PropertyErrors);
        }
    }
}
