﻿using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace EnterUp.Data.Test.RodeoEntryTest {
    public class RodeEntryValidateTests {
        public IValidationHelper Helper { get; set; }
        public RodeoEntry Test { get; set; }

        public RodeEntryValidateTests() {
            Test = new RodeoEntry {
                RodeoId = 1,
                FirstPreferenceId = 1,
                SecondPreferenceId = 2
            };
            Helper = Substitute.For<IValidationHelper>();
            Helper.When(h => h.GetAnnotationErrors(Arg.Any<IValidator>())).DoNotCallBase();
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(new List<PropertyError>());
            Helper.IsPerformanceInRodeo(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
        }
        private void Error(string propertyName, string errorText) {
            TestHelpers.ErrorContains(Test, Helper, propertyName, errorText);
        }

        private void NoError() {
            TestHelpers.NoError(Test, Helper);
        }

        [Fact]
        public void SecondPreference_Not_Null_Must_Belong_To_Rodeo() {
            Test.SecondPreferenceId = 1;
            Helper.IsPerformanceInRodeo(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            TestHelpers.NoError(Test, Helper);
            Helper.IsPerformanceInRodeo(Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Error("Second Preference", "The second perference is not a performance in this rodeo.");
        }
        [Fact]
        public void Second_Preference_Null_No_Error() {
            Test.SecondPreferenceId = null;
            NoError();
        }
        [Fact]
        public void FirstPreference_Must_Belong_To_Rodeo() {
            Helper.IsPerformanceInRodeo(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            TestHelpers.NoError(Test, Helper);
            Helper.IsPerformanceInRodeo(Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Error("First Preference", "The first perference is not a performance in this rodeo.");
        }

        [Fact]
        public void Validation_Exception_Message_Set_Correctly() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => Test.Validate(Helper));
            Assert.Equal("The rodeo entry is invalid.", ex.Message);
        }
        [Fact]
        public void Validate_PropertyErrors_Set_To_ValidationHelper_GetAnnotationError() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            TestHelpers.ErrorContains(Test, Helper, propertyName, errorText);
        }
    }
}
