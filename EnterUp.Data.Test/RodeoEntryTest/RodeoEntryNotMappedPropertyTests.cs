﻿using Xunit;

namespace EnterUp.Data.Test.RodeoEntryTest
{
    public class RodeoEntryNotMappedPropertyTests {
        [Fact]
        public void ConfirmationNumber_Formatted_Version_Of_RodeoId_And_ConfirmationNumber() {
            RodeoEntry test = new RodeoEntry {
                RodeoId = 1,
                Id = 1
            };
            Assert.Equal("1-000001", test.ConfirmationNumber);
            test.RodeoId = 12345;
            test.Id = 123456789;
            Assert.Equal("12345-123456789", test.ConfirmationNumber);
        }
        [Fact]
        public void IsDeleted_Is_Controlled_By_DeletedById() {
            RodeoEntry test = new RodeoEntry {
                DeletedById = null
            };
            Assert.False(test.IsDeleted);
            test.DeletedById = "1";
            Assert.True(test.IsDeleted);
        }
    }
}
