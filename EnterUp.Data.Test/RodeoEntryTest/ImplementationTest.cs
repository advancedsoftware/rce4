﻿using EnterUp.Data.Interfaces;
using Xunit;
// ReSharper disable RedundantCast

namespace EnterUp.Data.Test.RodeoEntryTest
{
    public class ImplementationTest {
        public readonly RodeoEntry TestObject = new RodeoEntry(); 
        [Fact]
        public void ImplementsIUpdateTracker() {
            Assert.NotNull(TestObject as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            Assert.NotNull(TestObject as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            Assert.NotNull(TestObject as IValidator);
        }
    }
}
