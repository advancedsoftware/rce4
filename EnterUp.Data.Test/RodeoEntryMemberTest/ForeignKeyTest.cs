﻿using System.Data;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.RodeoEntryMemberTest
{
    /// <summary>
    /// This class tests that database FK constraints are applied to the correct properties.
    /// </summary>
    public class ForeignKeyTest : BaseTestClass {
        [Fact]
        public void NonMemberFeeWaivedById_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntryMembers", "Users", "NonMemberFeeWaivedById"));
        }
        [Fact]
        public void PaymentVerifiedById_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntryMembers", "Users", "PaymentVerifiedById"));
        }
        [Fact]
        public void ContestantId_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntryMembers", "Users", "ContestantId"));
        }
        [Fact]
        public void RodeoEntryId_FK_Exists() {
            Assert.True(ForeignKeyExists(RodeoContext, "RodeoEntryMembers", "RodeoEntries", "RodeoEntryId"));
        }

        public static bool ForeignKeyExists(RodeoContext dbContext, string parentTableName, string childTableName, string indexColumnName) {
            string query =
                "Select Count(1) from sys.Tables t Join sys.Foreign_Keys fk on fk.parent_object_id = t.object_id"
                + $" Where fk.Name = 'FK_{parentTableName}_{childTableName}_{indexColumnName}'";

            bool hasKey;

            using (DbConnection connection = dbContext.Database.GetDbConnection()) {
                connection.Open();
                using (DbCommand command = connection.CreateCommand()) {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();
                    hasKey = reader.Read() && (int)reader[0] > 0;
                    reader.Dispose();
                }
                connection.Close();
            }
            return hasKey;
        }
    }
}
