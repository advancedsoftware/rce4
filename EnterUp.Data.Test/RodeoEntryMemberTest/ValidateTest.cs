﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.RodeoEntryMemberTest
{
    public class ValidateTest {
        public RodeoEntryMember Entity { get; set; }
        public IValidationHelper Helper => Substitute.For<IValidationHelper>();

        public ValidateTest() {
            Entity = new RodeoEntryMember {
                
            };
            Helper.GetAnnotationErrors(Arg.Any<object>()).Returns(new List<PropertyError>());
        }
        [Fact]
        public void RVCount_Less_Than_Zero_Set_To_Zero() {
            Entity.RVCount = -1;
            Entity.Validate(Helper);
            Assert.Equal(0, Entity.RVCount);
        }
        [Fact]
        public void StallCount_Less_Than_0_Set_To_Zero() {
            Entity.StallCount = -1;
            Entity.Validate(Helper);
            Assert.Equal(0, Entity.StallCount);
        }
    }
}
