﻿using Xunit;

namespace EnterUp.Data.Test.RodeoEntryMemberTest {
    public class NotMappedTest {
        public RodeoEntryMember Entity = new RodeoEntryMember();
        [Fact]
        public void IsDeleted_Controlled_By_DeletedById() {
            Entity.DeletedById = null;
            Assert.False(Entity.IsDeleted);
            Entity.DeletedById = "-1";
            Assert.True(Entity.IsDeleted);
        }
        [Fact]
        public void IsPaymentVerified_Controlled_By_DeletedById() {
            Entity.PaymentVerifiedById = null;
            Assert.False(Entity.IsPaymentVerified);
            Entity.PaymentVerifiedById = -1;
            Assert.True(Entity.IsPaymentVerified);
        }
        [Fact]
        public void IsNonMemberFeeWaived_Controlled_By_DeletedById() {
            Entity.NonMemberFeeWaivedById = null;
            Assert.False(Entity.IsNonMemberFeeWaived);
            Entity.NonMemberFeeWaivedById = -1;
            Assert.True(Entity.IsNonMemberFeeWaived);
        }
    }
}
