﻿using EnterUp.Data.Interfaces;

namespace EnterUp.Data.Test.RodeoEntryMemberTest
{
    public class AnnotationValidationTests : BaseTestClass {
        public RodeoEntryMember Entity => new RodeoEntryMember {
            RodeoEntryId = 1
        };
        public IValidationHelper Helper => new ValidationHelper(RodeoContext);

    }
}
