﻿using System;
using System.Linq;
using System.Threading;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.UpdatedBaseTest {
    /// <summary>
    /// This class will verify the SaveChanges method handles IPermanent objects correctly.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class UpdatedBySaveTest : BaseTestClass {
        [Fact]
        public void Updated_SuppliedValueIgnored() {
            Association association = AssociationGenerator.CreateAssociation(9);
            association.Updated = new DateTime(2010, 1, 1);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.True(DateTime.Now.AddSeconds(-1) < association.Updated);
                Assert.True(DateTime.Now.AddSeconds(1) > association.Updated);
            }
        }
        [Fact]
        public void UpdatedBy_SuppliedValueIgnored() {
            Association association = AssociationGenerator.CreateAssociation(10);
            association.UpdatedBy = "2222";
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", association.UpdatedBy);
            }
        }
        [Fact]
        public void Created_SuppliedValueIgnored() {
            Association association = AssociationGenerator.CreateAssociation(11);
            RodeoContext.Associations.Add(association);
            association.Created = new DateTime(2010, 1, 1);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                Association updateAssociation = db.Associations.First(a => a.Id == association.Id);
                Assert.True(DateTime.Now.AddSeconds(-1) < updateAssociation.Created);
                Assert.True(DateTime.Now.AddSeconds(1) > updateAssociation.Created);
                updateAssociation.Name += "A";
                updateAssociation.Created = new DateTime(2010, 1, 1);
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Association currentAssociation = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal(association.Created, currentAssociation.Created);
            }
        }
        [Fact]
        public void CreatedBy_SuppliedValueIgnored() {
            Association association = AssociationGenerator.CreateAssociation(12);
            RodeoContext.Associations.Add(association);
            association.CreatedBy = "2222";
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", association.CreatedBy);
                association.Name += "A";
                association.CreatedBy = "2222";
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", association.CreatedBy);
            }
        }
        [Fact]
        public void UpdatedByPropertiesSet_Updated() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            Association association = AssociationGenerator.CreateAssociation(1);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                association.Name += "A";
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("2", association.UpdatedBy);
            }
        }
        [Fact]
        public void UpdatedPropertiesSet_Updated () {
            Association association = AssociationGenerator.CreateAssociation(2);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            Thread.Sleep(10);
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                association.Name += "A";
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.True(DateTime.Now.AddSeconds(-1) < association.Updated);
                Assert.True(DateTime.Now.AddSeconds(1) > association.Updated);
                Assert.True(association.Created < association.Updated);
            }
        }
        [Fact]
        public void CreatedByPropertiesNotSet_Updated() {
            Association association = AssociationGenerator.CreateAssociation(3);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            Thread.Sleep(10);
            using (RodeoContext db = GetRodeoContext()) {
                Association updateAssociation = db.Associations.First(a => a.Id == association.Id);
                updateAssociation.Name += "A";
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Association currentAssociation = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal(currentAssociation.Created, association.Created);
            }
        }
        [Fact]
        public void CreatedPropertiesNotSet_Updated() {
            Association association = AssociationGenerator.CreateAssociation(4);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            Thread.Sleep(10);
            using (RodeoContext db = GetRodeoContext()) {
                Association updateAssociation = db.Associations.First(a => a.Id == association.Id);
                updateAssociation.Name += "A";
                db.SaveChanges("2");
            }
            using (RodeoContext db = GetRodeoContext()) {
                Association currentAssociation = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", currentAssociation.CreatedBy);
            }
        }
        [Fact]
        public void UpdatedByPropertiesSet_New() {
            Association association = AssociationGenerator.CreateAssociation(5);
            RodeoContext.Associations.Add(association);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", association.UpdatedBy);
            }
        }
        [Fact]
        public void UpdatedPropertiesSet_New() {
            Association association = AssociationGenerator.CreateAssociation(6);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.True(DateTime.Now.AddSeconds(-1) < association.Updated);
                Assert.True(DateTime.Now.AddSeconds(1) > association.Updated);
            }
        }
        [Fact]
        public void CreatedByPropertiesSet_New() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
            }
            Association association = AssociationGenerator.CreateAssociation(7);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.Equal("1", association.CreatedBy);
            }
        }
        [Fact]
        public void CreatedPropertiesSet_New() {
            Association association = AssociationGenerator.CreateAssociation(8);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = GetRodeoContext()) {
                association = db.Associations.First(a => a.Id == association.Id);
                Assert.True(DateTime.Now.AddSeconds(-1) < association.Created);
                Assert.True(DateTime.Now.AddSeconds(1) > association.Created);
            }
        }
    }
}
