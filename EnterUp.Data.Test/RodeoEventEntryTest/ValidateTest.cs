﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.RodeoEventEntryTest
{
    public class ValidateTest {
        public IValidationHelper Helper { get; set; }
        public RodeoEventEntry Test { get; set; }

        public ValidateTest() {
            Test = new RodeoEventEntry {
                RodeoEventId = 1,
                ContestantMemberId = 1
            };
            Helper = Substitute.For<IValidationHelper>();
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(new List<PropertyError>());
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(false);
            Helper.IsContestantEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsTeammateEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsSwitchEndsRuleViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsDuplicationEntryViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsTeamEntryViolationOfSameGroupRule(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
        }
        private void Error(string propertyName, string errorText) {
            TestHelpers.ErrorContains(Test, Helper, propertyName, errorText);
        }

        private void NoError() {
            TestHelpers.NoError(Test, Helper);
        }

        [Fact]
        public void TeamEvent_Validations_Are_Only_Called_When_The_RodeoEvent_Is_A_Team_Event() {
            Test.RodeoEventId = -1;
            Test.ContestantMemberId = -2;
            Test.TeammateMemberId = -3;
            Test.Validate(Helper);
            Helper.DidNotReceive().IsTeammateEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
            Helper.DidNotReceive().IsSwitchEndsRuleViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
            Helper.DidNotReceive().IsDuplicationEntryViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
            Helper.DidNotReceive().IsTeamEntryViolationOfSameGroupRule(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
            Test.RodeoEventId = -1;
            Test.ContestantMemberId = -2;
            Test.TeammateMemberId = -3;
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Test.Validate(Helper);
            Helper.Received(1).IsTeammateEntryCountExcessive(-1, 0, -3);
            Helper.Received(1).IsSwitchEndsRuleViolation(-1, 0, -2, -3);
            Helper.Received(1).IsDuplicationEntryViolation(-1, Test.Id, -2, -3);
            Helper.Received(1).IsTeamEntryViolationOfSameGroupRule(-1, -2, -3);
        }

        [Fact]
        public void Teammate_In_Another_Buddy_Group_Violation_Exception() {
            Test.RodeoEventId = -1;
            Test.ContestantMemberId = -2;
            Test.TeammateMemberId = -3;
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Helper.IsSwitchEndsRuleViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsTeamEntryViolationOfSameGroupRule(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            NoError();
            Helper.IsTeamEntryViolationOfSameGroupRule(-1, -2, -3).Returns(true);
            Error("Same Buddy Group Violation", 
                "The contestant and teammate for this event must be in the same buddy group.");
            Helper.Received(2).IsTeamEntryViolationOfSameGroupRule(-1, -2, -3);
        }

        [Fact]
        public void Duplicate_Entry_Violation_Throws_Exception() {
            Test.RodeoEventId = -1;
            Test.ContestantMemberId = -2;
            Test.TeammateMemberId = -3;
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Helper.IsSwitchEndsRuleViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Helper.IsDuplicationEntryViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            NoError();
            Helper.IsDuplicationEntryViolation(Test.RodeoEventId, Test.Id, Test.ContestantMemberId, Test.TeammateMemberId.Value).Returns(true);
            Error("Duplicate Entry Violation", "The contestant and teammate are already entered in the same event in the same positions.");
            Helper.Received(2).IsDuplicationEntryViolation(Test.RodeoEventId, Test.Id, Test.ContestantMemberId, Test.TeammateMemberId.Value);
        }

        [Fact]
        public void SwitchEnds_Violation_Throws_An_Exception() {
            Test.RodeoEventId = -1;
            Test.ContestantMemberId = -2;
            Test.TeammateMemberId = -3;
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Helper.IsSwitchEndsRuleViolation(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            NoError();
            Helper.IsSwitchEndsRuleViolation(Test.RodeoEventId, 0, Test.ContestantMemberId, Test.TeammateMemberId.Value).Returns(true);
            Error("Switch Ends Violation", "The contestant and teammate have another entry where they are in opposite postitions.");
            Helper.Received(2).IsSwitchEndsRuleViolation(Test.RodeoEventId, Test.Id, Test.ContestantMemberId, Test.TeammateMemberId.Value);
        }

        [Fact]
        public void TeammateRodeoEntryMemberId_Required_For_Team_Event() {
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Test.TeammateMemberId = 2;
            NoError();
            Test.TeammateMemberId = null;
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Error("Teammate Required", "A teammate is required to enter this event.");
        }

        [Fact]
        public void Too_Many_Teammate_Entries_In_Event_Exception() {
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Helper.IsTeammateEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            Test.TeammateMemberId = 2;
            NoError();
            Helper.IsTeammateEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            Error("Teammate Entry Count", "The teammate already has the maximum entries allowed for this event.");
        }
        [Fact]
        public void Too_Many_Contestant_Entries_In_Event_Exception() {
            NoError();
            Helper.IsContestantEntryCountExcessive(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            Error("Contestant Entry Count", "The contestant already has the maximum entries allowed for this event.");
        }
        [Fact]
        public void Teammate_Set_Null_When_Not_Team_Event() {
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(true);
            Test.TeammateMemberId = -1;
            Test.Validate(Helper);
            Assert.Equal(-1, Test.TeammateMemberId);
            Helper.IsTeamEvent(Arg.Any<int>()).Returns(false);
            Test.TeammateMemberId = -1;
            Test.Validate(Helper);
            Assert.Null(Test.TeammateMemberId);
        }
        [Fact]
        public void Validation_Exception_Message_Set_Correctly() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => Test.Validate(Helper));
            Assert.Equal("The rodeo event entry is invalid.", ex.Message);
        }

        [Fact]
        public void Validate_PropertyErrors_Set_To_ValidationHelper_GetAnnotationError() {
            const string propertyName = "As Df";
            const string errorText = "Some stupid text used to test.";
            Helper.GetAnnotationErrors(Arg.Any<IValidator>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = errorText
                    }
                });
            TestHelpers.ErrorContains(Test, Helper, propertyName, errorText);
        }
    }
}
