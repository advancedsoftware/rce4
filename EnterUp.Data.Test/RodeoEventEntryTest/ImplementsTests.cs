﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.RodeoEventEntryTest {
    public class ImplementsTests {
        public RodeoEventEntry Test => new RodeoEventEntry();
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(Test as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(Test as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(Test as IValidator);
        }
    }
}