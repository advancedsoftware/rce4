﻿using System;
using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.ContestantTests {
    public class ContestantValidationTests : BaseTestClass {
        public Contestant TestContestant { get; set; }
        public ValidationHelper ValidationHelper { get; set; }

        public ContestantValidationTests() {
            TestHelpers.ClearTableData(RodeoContext, "");
            ValidationHelper = Substitute.For<ValidationHelper>(RodeoContext);
            ValidationHelper.When(v => v.IsCardNumberDigitsUnique(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<string>())).DoNotCallBase();
            ValidationHelper.IsCardNumberDigitsUnique(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<string>()).Returns(true);
            ValidationHelper.When(v => v.DateOfBirthHasValue(Arg.Any<int>())).DoNotCallBase();
            ValidationHelper.DateOfBirthHasValue(Arg.Any<int>()).Returns(true);
            ValidationHelper.When(v => v.GetAnnotationErrors(Arg.Any<object>())).DoNotCallBase();
            ValidationHelper.GetAnnotationErrors(Arg.Any<object>()).Returns(new List<PropertyError>());
            TestContestant = new Contestant {
                UserId = null,
                AssociationId = 1,
                CardNumber = "A1234B",
                DateOfBirthVerifiedAt = null,
            };
        }

        [Fact]
        public void ValidationHelper_GetAnnotationErrorsCalled() {
            TestContestant.Validate(ValidationHelper);
            ValidationHelper.Received().GetAnnotationErrors(Arg.Any<object>());
        }

        

        [Fact]
        public void UserId_allows_null() {
            TestContestant.UserId = null;
            TestContestant.Validate(ValidationHelper);
        }

        [Fact]
        public void CardNumber_Not_Unique_Throws() {
            ValidationHelper.IsCardNumberDigitsUnique(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<string>()).Returns(false);
            CheckError("Card Number", "The card number must contain a unique identifier.");
        }

        [Fact]
        public void CardNumber_Unique_Not_Throws() {
            ValidationHelper = Substitute.For<ValidationHelper>(RodeoContext);
            ValidationHelper.When(v => v.IsCardNumberDigitsUnique(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<string>())).DoNotCallBase();
            ValidationHelper.IsCardNumberDigitsUnique(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<string>()).Returns(true);
            NoError("Card Number", "The card number must contain a unique identifier.");
        }

        [Fact]
        public void DateOfBirthVerifiedBy_allows_null() {
            TestContestant.DateOfBirthVerifiedBy = null;
            TestContestant.Validate(ValidationHelper);
        }

        [Fact]
        public void DateOfBirthVerifiedAt_can_be_null() {
            TestContestant.DateOfBirthVerifiedAt = null;
            Assert.Null(TestContestant.DateOfBirthVerifiedBy);
        }

        [Fact]
        public void DateOfBirthVerifiedAt_Set_Now_If_VerifiedBy_Not_Null_And_VerifiedAt_Null() {
            TestContestant.DateOfBirthVerifiedBy = 10;
            TestContestant.DateOfBirthVerifiedAt = null;
            TestContestant.Validate(ValidationHelper);
            Assert.True(DateTime.Now.AddSeconds(-1) < TestContestant.DateOfBirthVerifiedAt);
            Assert.True(DateTime.Now.AddSeconds(1) > TestContestant.DateOfBirthVerifiedAt);
        }

        [Fact]
        public void DateOfBirthVerifiedAt_Set_Now_If_VerifiedAt_Not_Null_And_VerifiedBy_Changed_And_Not_Null() {
            ValidationHelper.When(v => v.HasPropertyChanged(Arg.Any<object>(), Arg.Any<string>())).DoNotCallBase();
            ValidationHelper.HasPropertyChanged(Arg.Any<object>(), Arg.Any<string>()).Returns(true);
            TestContestant.DateOfBirthVerifiedBy = 10;
            TestContestant.DateOfBirthVerifiedAt = DateTime.Now.AddDays(-1);
            TestContestant.Validate(ValidationHelper);
            Assert.True(DateTime.Now.AddSeconds(-1) < TestContestant.DateOfBirthVerifiedAt);
            Assert.True(DateTime.Now.AddSeconds(1) > TestContestant.DateOfBirthVerifiedAt);
        }

        [Fact]
        public void DateOfBirthVerifiedAt_Unchanged_If_VerifiedBy_Is_Unchanged_And_VerifiedAt_Not_Null() {
            ValidationHelper.When(v => v.HasPropertyChanged(Arg.Any<object>(), Arg.Any<string>())).DoNotCallBase();
            ValidationHelper.HasPropertyChanged(Arg.Any<object>(), Arg.Any<string>()).Returns(false);
            TestContestant.DateOfBirthVerifiedBy = 10;
            TestContestant.DateOfBirthVerifiedAt = DateTime.Today;
            TestContestant.Validate(ValidationHelper);
            Assert.Equal(DateTime.Today, TestContestant.DateOfBirthVerifiedAt);
        }

        [Fact]
        public void DateOfBirthVerifiedAt_set_to_null_if_DateOfBirthVerifiedBy_is_blank() {
            TestContestant.DateOfBirthVerifiedAt = DateTime.Now;
            TestContestant.DateOfBirthVerifiedBy = null;
            TestContestant.Validate(ValidationHelper);
            Assert.Null(TestContestant.DateOfBirthVerifiedAt);
        }

        [Fact]
        public void DateOfBirthVerifiedBy_And_VerfiedAt_Set_Null_If_DOB_Is_Null() {
            ValidationHelper.When(v => v.DateOfBirthHasValue(Arg.Any<int>())).DoNotCallBase();
            ValidationHelper.DateOfBirthHasValue(Arg.Any<int>()).Returns(false);
            TestContestant.DateOfBirthVerifiedAt = DateTime.Now;
            TestContestant.DateOfBirthVerifiedBy = 10;
            TestContestant.Validate(ValidationHelper);
            Assert.Null(TestContestant.DateOfBirthVerifiedAt);
            Assert.Null(TestContestant.DateOfBirthVerifiedBy);
        }

        [Fact]
        public void DateOfBirthVerifiedBy_And_VerifiedAt_Unchanged_If_DOB_Is_Not_Null() {
            ValidationHelper.When(v => v.DateOfBirthHasValue(Arg.Any<int>())).DoNotCallBase();
            ValidationHelper.DateOfBirthHasValue(Arg.Any<int>()).Returns(true);
            TestContestant.DateOfBirthVerifiedAt = DateTime.Today;
            TestContestant.DateOfBirthVerifiedBy = 10;
            TestContestant.Validate(ValidationHelper);
            Assert.Equal(DateTime.Today, TestContestant.DateOfBirthVerifiedAt);
            Assert.Equal(10, TestContestant.DateOfBirthVerifiedBy);
        }

        [Fact]
        public void Email_changed_from_null_to_blank() {
            TestContestant.Email = null;
            TestContestant.Validate(ValidationHelper);
            Assert.Equal(string.Empty, TestContestant.Email);
        }

        [Fact]
        public void Email_is_trimmed() {
            TestContestant.Email = "  Test  ";
            TestContestant.Validate(ValidationHelper);
            Assert.Equal("Test", TestContestant.Email);
        }

        [Fact]
        public void IsActive_false_if_IsDeleted() {
            TestContestant.IsActive = true;
            TestContestant.DeletedById = "1";
            TestContestant.Validate(ValidationHelper);
            Assert.False(TestContestant.IsActive);
        }

        private void CheckError(string expectedProperty, string expectedErrorText) {
            EnterUpDataException exception = Assert.Throws<EnterUpDataException>(() => TestContestant.Validate(ValidationHelper));
            Assert.Equal("The contestant is invalid.", exception.Message);
            TestHelpers.ErrorContains(
                exception.PropertyErrors,
                expectedProperty,
                expectedErrorText);
        }

        private void NoError(string propertyName, string errorText) {
            try {
                TestContestant.Validate(ValidationHelper);
            }
            catch (EnterUpDataException ex) {
                TestHelpers.ErrorNotContains(ex.PropertyErrors, propertyName, errorText);
            }
        }
    }
}