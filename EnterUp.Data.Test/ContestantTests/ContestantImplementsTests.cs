﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.ContestantTests {
    public class ContestantImplementsTests {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Contestant() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Contestant() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Contestant() as IValidator);
        }
    }
}