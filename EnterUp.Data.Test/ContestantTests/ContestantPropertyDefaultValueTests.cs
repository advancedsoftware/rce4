﻿using Xunit;

namespace EnterUp.Data.Test.ContestantTests
{
    /// <summary>
    /// This test class verifies the default constructor.
    /// </summary>
    public class ContestantPropertyDefaultValueTests {
        [Fact]
        public void UserId_default_is_Null() {
            Assert.Null(new Contestant().UserId);
        }

        [Fact]
        public void AssociationId_is_Zero() {
            Assert.Equal(0, new Contestant().AssociationId);
        }

        [Fact]
        public void CardNumber_default_is_null() {
            Assert.Null(new Contestant().CardNumber);
        }

        [Fact]
        public void DateOfBirthVerifiedBy_is_null() {
            Assert.Null(new Contestant().DateOfBirthVerifiedBy);
        }

        [Fact]
        public void DateOfBirthVerifiedOn_is_null() {
            Assert.Null(new Contestant().DateOfBirthVerifiedAt);
        }

        [Fact]
        public void Email_is_blank() {
            Assert.Equal(string.Empty, new Contestant().Email);
        }

        //[Fact]
        //public void HomePhone_is_blank() {
        //    Assert.Equal(string.Empty, new Contestant().HomePhone);
        //}

        //[Fact]
        //public void CellPhone_is_null() {
        //    Assert.Equal(string.Empty, new Contestant().CellPhone);
        //}

        [Fact]
        public void IsMember_is_false() {
            Assert.False(new Contestant().IsMember);
        }

        [Fact]
        public void IsPermit_is_false() {
            Assert.False(new Contestant().IsPermit);
        }

        [Fact]
        public void IsUserCreated_is_false() {
            Assert.False(new Contestant().IsUserCreated);
        }

        [Fact]
        public void IsActive_is_true() {
            Assert.True(new Contestant().IsActive);
        }

        [Fact]
        public void IsDeleted_is_false() {
            Assert.False(new Contestant().IsDeleted);
        }
    }
}
