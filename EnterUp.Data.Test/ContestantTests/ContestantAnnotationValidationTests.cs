﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ContestantTests
{
    public class ContestantAnnotationValidationTests : BaseTestClass {
        public Contestant TestContestant = new Contestant {
            
        };
        public ValidationHelper ValidationHelper => new ValidationHelper(RodeoContext);

        [Fact]
        public void Email_Not_Required() {
            TestContestant.Email = null;
            TestHelpers.ErrorNotContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Email",
                "The email address must be less than 256 characters.");
        }

        [Fact]
        public void Email_Allows_Blank() {
            TestContestant.Email = null;
            TestHelpers.ErrorNotContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Email",
                "The email address must be less than 256 characters.");
        }

        [Fact]
        public void Email_Max_Length_255() {
            for (int i = 0; i < 255; i++) {
                TestContestant.Email += "a";
            }
            TestHelpers.ErrorNotContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Email",
                "The email address must be less than 256 characters.");
            TestContestant.Email += "a";
            TestHelpers.ErrorContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Email",
                "The email address must be less than 256 characters.");
        }

        [Fact]
        public void CardNumber_MaxLength_Is_Ten() {
            TestContestant.CardNumber = "1234567890";
            TestHelpers.ErrorNotContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
            TestContestant.CardNumber += "A";
            TestHelpers.ErrorContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
        }

        [Fact]
        public void CardNumber_MinLength_Is_Two() {
            TestContestant.CardNumber = "A";
            TestHelpers.ErrorContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
            TestContestant.CardNumber = "Aa";
            TestHelpers.ErrorNotContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
        }

        [Fact]
        public void CardNumber_Blank_Throws() {
            TestContestant.CardNumber = string.Empty;
            TestHelpers.ErrorContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
        }

        [Fact]
        public void CardNumber_Null_Throws() {
            TestContestant.CardNumber = null;
            TestHelpers.ErrorContains(ValidationHelper.GetAnnotationErrors(TestContestant),
                "Card Number",
                "The Card Number is required and must be between 2 and 10 characters in length.");
        }
    }
}
