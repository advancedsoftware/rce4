﻿using Xunit;

namespace EnterUp.Data.Test.ContestantTests
{
    public class ContestantNotMappedPropertyTests {
        [Fact]
        public void IsDateOfBirthVerified_false_when_DateOfBirthVerifiedBy_null() {
            Contestant contestant = new Contestant {DateOfBirthVerifiedBy = null};
            Assert.False(contestant.IsDateOfBirthVerified);
        }
        
        [Fact]
        public void IsDateOfBirthVerified_true_when_DateOfBirthVerifiedBy_has_a_value() {
            Contestant contestant = new Contestant { DateOfBirthVerifiedBy = 1 };
            Assert.True(contestant.IsDateOfBirthVerified);
        }

        [Fact]
        public void IsDeleted_false_when_DeletedBy_is_null() {
            Contestant contestant = new Contestant { DeletedById = null };
            Assert.False(contestant.IsDeleted);
        }

        [Fact]
        public void IsDeleted_true_when_DeletedBy_has_a_value() {
            Contestant contestant = new Contestant { DeletedById = "1" };
            Assert.True(contestant.IsDeleted);
        }

        
    }
}
