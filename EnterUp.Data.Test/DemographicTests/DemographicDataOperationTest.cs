﻿using System.Linq;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.DemographicTests
{
    public class DemographicDataOperationTest : BaseTestClass {
        internal Demographic Demographic = new Demographic {
            State = "ID",
            Zipcode = "83642",
            FirstName = "Michael",
            LastName = "McDaniel",
            HomePhone = "2088469635",
            CellPhone = "2082841007",
            Address1 = "2863 E. Lucca Dr.",
            City = "Meridian",
            DateOfBirth = null
        };
        [Fact]
        public void Valid_Object_Is_Added() {
            using (RodeoContext db = new RodeoContext()) {
                TestHelpers.ClearTableData(db, "Demographics");
                int startingCount = db.Demographics.Count();
                db.Demographics.Add(Demographic);
                db.SaveChanges("1");
                Assert.Equal(startingCount + 1, db.Demographics.Count());
            }
        }

        [Fact]
        public void Valid_Object_Is_Updated() {
            using (RodeoContext db = new RodeoContext()) {
                TestHelpers.ClearTableData(db, "Demographics");
                int startingCount = db.Demographics.Count();
                db.Demographics.Add(Demographic);
                db.SaveChanges("1");
                Assert.Equal(startingCount + 1, db.Demographics.Count());
                Demographic.LastName += "a";
                db.SaveChanges("1");
                Assert.Equal(startingCount + 1, db.Demographics.Count());
            }
        }

        
    }
}
