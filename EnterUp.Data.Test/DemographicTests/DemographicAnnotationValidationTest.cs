﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.DemographicTests
{
    public class DemographicAnnotationValidationTest : BaseTestClass {
        internal ValidationHelper ValidationHelper = new ValidationHelper(null);
        internal Demographic Demographic = new Demographic {
            State = "ID",
            Zipcode = "83642",
            FirstName = "Michael",
            LastName = "McDaniel",
            HomePhone = "2088469635",
            CellPhone = "2082841007",
            Address1 = "2863 E. Lucca Dr.",
            City = "Meridian",
            DateOfBirth = null
        };

        private void CheckError(string expectedProperty, string expectedErrorText) {
            List<PropertyError> errors = ValidationHelper.GetAnnotationErrors(Demographic);
            TestHelpers.ErrorContains(
                errors,
                expectedProperty,
                expectedErrorText);
        }

        private void NoError(string propertyName, string errorText) {
            List<PropertyError> errors = ValidationHelper.GetAnnotationErrors(ValidationHelper);
            TestHelpers.ErrorNotContains(errors, propertyName, errorText);
        }

        [Fact]
        public void FirstName_Validation_Required() {
            const string errorText = "The first name is required and must be between 1 and 50 characters.";
            const string propertyName = "First Name";
            Demographic.FirstName = null;
            CheckError(propertyName, errorText);
            Demographic.FirstName = string.Empty;
            CheckError(propertyName, errorText);
            Demographic.FirstName = "a";
            NoError(propertyName, errorText);
        }

        [Fact]
        public void FirstName_Max_Length() {
            const string errorText = "The first name is required and must be between 1 and 50 characters.";
            const string propertyName = "First Name";
            Demographic.FirstName = string.Empty;
            for (int i = 0; i < 50; i++) {
                Demographic.FirstName += "a";
            }
            Assert.Equal(50, Demographic.FirstName.Length);
            NoError(propertyName, errorText);
            Demographic.FirstName += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void LastName_Required() {
            const string errorText = "The last name is required and must be between 1 and 50 characters.";
            const string propertyName = "Last Name";
            Demographic.LastName = null;
            CheckError(propertyName, errorText);
            Demographic.LastName = string.Empty;
            CheckError(propertyName, errorText);
            Demographic.LastName = "a";
            NoError(propertyName, errorText);
        }

        [Fact]
        public void LastName_Max_Length() {
            const string errorText = "The last name is required and must be between 1 and 50 characters.";
            const string propertyName = "Last Name";
            Demographic.LastName = string.Empty;
            for (int i = 0; i < 50; i++) {
                Demographic.LastName += "a";
            }
            Assert.Equal(50, Demographic.LastName.Length);
            NoError(propertyName, errorText);
            Demographic.LastName += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void Address1_Max_Length() {
            const string errorText = "The maximum length of the Address1 property is 100 characters.";
            const string propertyName = "Address 1";
            const int maxLength = 100;

            Demographic.Address1 = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.Address1 += "a";
            }
            Assert.Equal(maxLength, Demographic.Address1.Length);
            NoError(propertyName, errorText);
            Demographic.Address1 += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void Address2_Max_Length() {
            const string errorText = "The maximum length of the Address2 property is 100 characters.";
            const string propertyName = "Address 2";
            const int maxLength = 100;

            Demographic.Address2 = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.Address2 += "a";
            }
            Assert.Equal(maxLength, Demographic.Address2.Length);
            NoError(propertyName, errorText);
            Demographic.Address2 += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void City_Max_Length() {
            const string errorText = "The maximum length of the City property is 100 characters.";
            const string propertyName = "City";
            const int maxLength = 100;

            Demographic.City = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.City += "a";
            }
            Assert.Equal(maxLength, Demographic.City.Length);
            NoError(propertyName, errorText);
            Demographic.City += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void State_Max_Length() {
            const string errorText = "The maximum length of the State property is 5 characters.";
            const string propertyName = "State";
            const int maxLength = 5;

            Demographic.State = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.State += "a";
            }
            Assert.Equal(maxLength, Demographic.State.Length);
            NoError(propertyName, errorText);
            Demographic.State += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void Zipcode_Max_Length() {
            const string errorText = "The maximum length of the Zipcode property is 12 characters.";
            const string propertyName = "Zipcode";
            const int maxLength = 12;

            Demographic.Zipcode = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.Zipcode += "a";
            }
            Assert.Equal(maxLength, Demographic.Zipcode.Length);
            NoError(propertyName, errorText);
            Demographic.Zipcode += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void HomePhone_Max_Length() {
            const string errorText = "The maximum length of the HomePhone property is 25 characters.";
            const string propertyName = "Home Phone";
            const int maxLength = 25;

            Demographic.HomePhone = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.HomePhone += "a";
            }
            Assert.Equal(maxLength, Demographic.HomePhone.Length);
            NoError(propertyName, errorText);
            Demographic.HomePhone += "a";
            CheckError(propertyName, errorText);
        }

        [Fact]
        public void CellPhone_Max_Length() {
            const string errorText = "The maximum length of the CellPhone property is 25 characters.";
            const string propertyName = "Cell Phone";
            const int maxLength = 25;

            Demographic.CellPhone = string.Empty;
            for (int i = 0; i < maxLength; i++) {
                Demographic.CellPhone += "a";
            }
            Assert.Equal(maxLength, Demographic.CellPhone.Length);
            NoError(propertyName, errorText);
            Demographic.CellPhone += "a";
            CheckError(propertyName, errorText);
        }
    }
}
