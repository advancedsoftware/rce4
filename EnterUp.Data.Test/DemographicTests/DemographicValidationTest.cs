﻿using Xunit;

namespace EnterUp.Data.Test.DemographicTests
{
    public class DemographicValidationTest {
        private readonly Demographic demographic = new Demographic();
        [Fact]
        public void Address1_Null_To_Empty() {
            demographic.Address1 = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.Address1);
        }
        [Fact]
        public void Address2_Null_To_Empty() {
            demographic.Address2 = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.Address2);
        }
        [Fact]
        public void City_Null_To_Empty() {
            demographic.City = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.City);
        }
        [Fact]
        public void State_Null_To_Empty() {
            demographic.State = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.State);
        }
        [Fact]
        public void HomePhone_Null_To_Empty() {
            demographic.HomePhone = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.HomePhone);
        }
        [Fact]
        public void CellPhone_Null_To_Empty() {
            demographic.CellPhone = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.CellPhone);
        }
        [Fact]
        public void ZipCode_Null_To_Empty() {
            demographic.Zipcode = null;
            demographic.Validate(null);
            Assert.Equal(string.Empty, demographic.Zipcode);
        }
    }
}
