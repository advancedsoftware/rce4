﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.DemographicTests
{
    public class DemographicImplementationTest {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Demographic() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Demographic() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Demographic() as IValidator);
        }
    }
}
