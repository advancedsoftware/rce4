﻿using Xunit;

namespace EnterUp.Data.Test.DemographicTests
{
    public class DemographicConstructorTest {
        [Fact]
        public void Constructor_set_property_defaults() {
            Demographic testObject = new Demographic();
            Assert.Equal(string.Empty, testObject.Address1);
            Assert.Equal(string.Empty, testObject.Address2);
            Assert.Equal(string.Empty, testObject.CellPhone);
            Assert.Equal(string.Empty, testObject.City);
            Assert.Equal(string.Empty, testObject.FirstName);
            Assert.Equal(string.Empty, testObject.LastName);
            Assert.Equal(string.Empty, testObject.HomePhone);
            Assert.Equal(string.Empty, testObject.State);
            Assert.Equal(string.Empty, testObject.Zipcode);
        }

        
    }
}
