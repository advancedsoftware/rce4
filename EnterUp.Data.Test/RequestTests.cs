﻿using System.Collections.Generic;
using System.Linq;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test {
    /// <summary>
    /// This class holds all of the data tests for the request entity.
    /// </summary>
    public class RequestTests : BaseTestClass {
        public Request TestRequest { get; set; }

        public ValidationHelper ValidationHelper { get; set; }

        public RequestTests() {
            ValidationHelper = new ValidationHelper(RodeoContext);
            TestRequest = new Request {
                IpAddress = "255.255.255.255",
                RequestData = "Some Data",
                ResponseData = null,
                Url = "api/somecontroller/somemethod"
            };
        }

        #region Implements 
        [Fact]
        public void Request_Implements_IValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(TestRequest as IValidator); 
        }
        #endregion
        
        #region Validation Tests
        [Fact]
        public void GetAnnotationErrors_is_called() {
            ValidationHelper = Substitute.ForPartsOf<ValidationHelper>(RodeoContext);
            ValidationHelper.When(v => v.GetAnnotationErrors(Arg.Any<object>())).DoNotCallBase();
            ValidationHelper.GetAnnotationErrors(Arg.Any<object>()).Returns(new List<PropertyError>());
            TestRequest.Validate(ValidationHelper);
            ValidationHelper.Received(1).GetAnnotationErrors(Arg.Any<object>());
        }

        [Fact]
        public void Exception_Message() {
            TestRequest.IpAddress = null;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(new ValidationHelper(RodeoContext)));
            Assert.Equal("The request object is invalid.", ex.Message);
        }

        [Fact]
        public void IpAddress_is_required() {
            TestRequest.IpAddress = null;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(new ValidationHelper(RodeoContext)));
            ContainsError(ex, "Ip Address", "The IP address provided is invalid.");
        }

        [Fact]
        public void IpAddress_cannot_be_blank() {
            TestRequest.IpAddress = string.Empty;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(new ValidationHelper(RodeoContext)));
            ContainsError(ex, "Ip Address", "The IP address provided is invalid.");
        }

        [Fact]
        public void IpAddress_must_be_valid_IP_Address() {
            const string propertyName = "Ip Address";
            const string errorMessage = "The IP address provided is invalid.";
            ValidationHelper = new ValidationHelper(RodeoContext);
            TestRequest.Validate(ValidationHelper);
            TestRequest.IpAddress = "0.0.0.0";
            TestRequest.Validate(ValidationHelper);
            TestRequest.IpAddress = "256.255.255.255";
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(ValidationHelper));
            ContainsError(ex, propertyName, errorMessage);
        }

        [Fact]
        public void UserId_can_be_null() {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
            Assert.Equal(0, RodeoContext.Requests.Count());
            ValidationHelper = new ValidationHelper(RodeoContext);
            RodeoContext.Requests.Add(TestRequest);
            RodeoContext.SaveChanges("1");
            using (RodeoContext db = new RodeoContext()) {
                Assert.Equal(1, RodeoContext.Requests.Count());
                Assert.Equal(1, db.Requests.Count());
            }
        }

        [Fact]
        public void Url_Equals_Null_Throws_EnterUpDataException() {
            const string propertyName = "Url";
            const string errorMessage = "Url is required.";
            TestRequest.Url = null;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(ValidationHelper));
            ContainsError(ex, propertyName, errorMessage);
            
        }

        [Fact]
        public void Url_Equals_Blank_Throws_EnterUpDataException() {
            const string propertyName = "Url";
            const string errorMessage = "Url is required.";
            TestRequest.Url = string.Empty;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => TestRequest.Validate(ValidationHelper));
            ContainsError(ex, propertyName, errorMessage);
        }

        [Fact]
        public void RequestData_can_be_null() { 
            TestRequest.RequestData = null;
            TestRequest.Validate(ValidationHelper);
        }

        [Fact]
        public void RequestData_can_contain_string() {
            TestRequest.RequestData = string.Empty;
            TestRequest.Validate(ValidationHelper);
        }

        [Fact]
        public void ResponseData_can_be_null() {
            TestRequest.ResponseData = null;
            TestRequest.Validate(ValidationHelper);
        }

        [Fact]
        public void ResponseData_can_contain_a_string(){
            TestRequest.ResponseData = "Test String";
            TestRequest.Validate(ValidationHelper);
        }
        #endregion Validation Tests
    }
}
