﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.SuggestionTests
{
    public class SuggestionValidationTests : BaseTestClass
    {
        [Fact]
        public void UserSuggestion_Is_Required() {
            Suggestion suggestion = new Suggestion {
                UserSuggestion = null,
                Notes = TestHelpers.GetStringOfLength(512)
            };
            TestHelpers.ErrorContains(suggestion, "User Suggestion", "The user suggestion is required.");
            suggestion.UserSuggestion = string.Empty;
            TestHelpers.ErrorContains(suggestion, "User Suggestion", "The user suggestion is required.");
        }

        [Fact]
        public void UserSuggestion_Min_Length_5() {
            Suggestion suggestion = new Suggestion {
                UserSuggestion = "1234",
                Notes = TestHelpers.GetStringOfLength(100)
            };
            TestHelpers.ErrorContains(suggestion, "User Suggestion", "The user suggestion must be at least 5 characters.");
            suggestion.UserSuggestion += "A";
            TestHelpers.NoError(suggestion);
        }

        [Fact]
        public void UserSuggestion_Max_Length_1024() {
            Suggestion suggestion = new Suggestion {
                UserSuggestion = TestHelpers.GetStringOfLength(1024),
                Notes = TestHelpers.GetStringOfLength(512)
            };
            suggestion.Validate(new ValidationHelper(RodeoContext));
            suggestion.UserSuggestion += "a";
            TestHelpers.ErrorContains(suggestion, "User Suggestion", "The user suggestion cannot exceed 1024 characters.");
        }

        [Fact]
        public void Notes_Max_Length_512() {
            Suggestion suggestion = new Suggestion {
                UserSuggestion = "User Suggestion",
                Notes = TestHelpers.GetStringOfLength(512)
            };
            suggestion.Validate(new ValidationHelper(RodeoContext));
            suggestion.Notes += "a";
            TestHelpers.ErrorContains(suggestion, "Notes", "The notes cannot exceed 512 characters.");
        }

        [Fact]
        public void Notes_Changed_To_Empty_From_Null() {
            Suggestion suggestion = new Suggestion {
                UserSuggestion = "User Suggestion",
                Notes = null
            };
            suggestion.Validate(new ValidationHelper(RodeoContext));
            Assert.NotNull(suggestion.Notes);
        }
    }
}
