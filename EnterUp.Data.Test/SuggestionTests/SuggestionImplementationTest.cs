﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.SuggestionTests
{
    public class SuggestionImplementationTest {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Suggestion() as IUpdatedTracker);
        }

        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Suggestion() as IValidator);
        }
    }
}
