﻿using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.RodeoContextTests {
    public class ExceptionHandlingTest : BaseTestClass {
        public ExceptionHandlingTest() {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
        }

        [Fact]
        public void Multiple_Entities_Not_Event_Entry_Can_Be_Saved() {
            RodeoContext.Errors.Add(new Error {
                ErrorMessage = "test",
                ValidationErrors = "",
                StackTrace = "Test Stack Trace"
            });
            RodeoContext.Errors.Add(new Error {
                ErrorMessage = "test",
                ValidationErrors = "",
                StackTrace = "Test Stack Trace"
            });
            RodeoContext.SaveChanges("1");
        }
        [Fact]
        public void Multiple_Entries_On_Save_Throws_Exception() {
            RodeoContext.RodeoEventEntries.Add(new RodeoEventEntry());
            RodeoContext.RodeoEventEntries.Add(new RodeoEventEntry());
            EnterUpDataException ex =
                Assert.Throws<EnterUpDataException>(() => RodeoContext.SaveChanges("1"));
            Assert.Equal("Only one event entry can be saved at a time.", ex.Message);
            Assert.Empty(ex.PropertyErrors);
        }
        [Fact]
        public void Unique_Constraint_Violation_Throws_EnterUpException() {
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                context.Associations.Add(AssociationGenerator.CreateAssociation(1));
                context.SaveChanges("1");
            }
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                Association association = AssociationGenerator.CreateAssociation(1);
                association.Abbreviation += "A";
                context.Associations.Add(association);
                EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => context.SaveChanges("1"));
                Assert.Equal("Saving the Association failed.", ex.Message);
                TestHelpers.ErrorContains(ex.PropertyErrors,
                    "Name",
                    "The value (Association 01) for Name is already in use, the value for Name must be unique.");
                Assert.NotNull(ex.InnerException as DbUpdateException);
            }
        }
    }
}
