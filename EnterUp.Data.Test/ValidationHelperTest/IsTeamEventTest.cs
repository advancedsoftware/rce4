﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class IsTeamEventTest : MockDbTestBase {
        [Fact]
        public void RodeoEvent_Is_TeamEvent_Returns_False() {
            AssociationEvent associationEvent = new AssociationEvent {
                Id = 1,
                IsTeamEvent = true
            };
            RodeoEvent rodeoEvent = new RodeoEvent {
                Id = 1,
                AssociationEventId = 1
            };
            using (RodeoContext db = MockContext()) {
                db.AssociationEvents.Add(associationEvent);
                db.RodeoEvents.Add(rodeoEvent);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeamEvent(1));
            }
        }

        [Fact]
        public void RodeoEvent_Not_IsTeamEvent_Returns_False() {
            AssociationEvent associationEvent = new AssociationEvent {
                Id = 1,
                IsTeamEvent = false
            };
            RodeoEvent rodeoEvent = new RodeoEvent {
                Id = 1,
                AssociationEventId = 1
            };
            using (RodeoContext db = MockContext()) {
                db.AssociationEvents.Add(associationEvent);
                db.RodeoEvents.Add(rodeoEvent);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsTeamEvent(1));
            }
        }
        [Fact]
        public void RodeoEventId_Invalid_Returns_False() {
            AssociationEvent associationEvent = new AssociationEvent {
                Id = 1,
                IsTeamEvent = true
            };
            RodeoEvent rodeoEvent = new RodeoEvent {
                Id = 1,
                AssociationEventId = 1
            };
            using (RodeoContext db = MockContext()) {
                db.AssociationEvents.Add(associationEvent);
                db.RodeoEvents.Add(rodeoEvent);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsTeamEvent(100));
            }
        }
    }
}