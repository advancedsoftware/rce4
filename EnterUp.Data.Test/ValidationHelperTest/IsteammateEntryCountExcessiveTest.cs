﻿using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsTeammateEntryCountExcessiveTest : MockDbTestBase {
        [Fact]
        public void Entry_Count_Less_Than_Max_Returns_False() {
            using (RodeoContext db = new RodeoContext(Options)) {
                db.IsSkipValidation = true;
                ValidationHelper helper = Substitute.ForPartsOf<ValidationHelper>(db);
                helper.When(h => h.GetEntryCountByRodeoEntryMemberId(1, 2, 1)).DoNotCallBase();
                helper.GetEntryCountByRodeoEntryMemberId(1, 2, 1).Returns(1);
                db.ValidationHelper = helper;
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    MaximumEntryCount = 2
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsTeammateEntryCountExcessive(1, 2, 1));
            }
        }

        [Fact]
        public void Entry_Count_Equal_Max_Returns_True() {
            using (RodeoContext db = new RodeoContext(Options)) {
                db.IsSkipValidation = true;
                ValidationHelper helper = Substitute.ForPartsOf<ValidationHelper>(db);
                helper.When(h => h.GetEntryCountByRodeoEntryMemberId(1, 2, 1)).DoNotCallBase();
                helper.GetEntryCountByRodeoEntryMemberId(1, 2, 1).Returns(2);
                db.ValidationHelper = helper;
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    MaximumEntryCount = 2
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeammateEntryCountExcessive(1, 2, 1));
            }
        }

        [Fact]
        public void Entry_Count_Greater_Than_Max_Returns_True() {
            using (RodeoContext db = new RodeoContext(Options)) {
                db.IsSkipValidation = true;
                ValidationHelper helper = Substitute.ForPartsOf<ValidationHelper>(db);
                helper.When(h => h.GetEntryCountByRodeoEntryMemberId(1, 2, 1)).DoNotCallBase();
                helper.GetEntryCountByRodeoEntryMemberId(1, 2, 1).Returns(3);
                db.ValidationHelper = helper;
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    MaximumEntryCount = 2
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeammateEntryCountExcessive(1, 2, 1));
            }
        }

        [Fact]
        public void GetEntryCountByRodeoEntryMemberCalled_With_Correct_Parameters() {
            using (RodeoContext db = new RodeoContext(Options)) {
                db.IsSkipValidation = true;
                ValidationHelper helper = Substitute.ForPartsOf<ValidationHelper>(db);
                helper.When(h => h.GetEntryCountByRodeoEntryMemberId(1, 2, 3)).DoNotCallBase();
                helper.GetEntryCountByRodeoEntryMemberId(1, 2, 3).Returns(-1);
                db.ValidationHelper = helper;
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    MaximumEntryCount = 2
                });
                db.SaveChanges("1");
                db.ValidationHelper.IsTeammateEntryCountExcessive(1, 2, 3);
                helper.Received(1).GetEntryCountByRodeoEntryMemberId(1, 2, 3);
            }
        }

        [Fact]
        public void Invalid_RodoeEventId_Returns_True() {
            using (RodeoContext db = MockContext()) {
                Assert.True(db.ValidationHelper.IsTeammateEntryCountExcessive(10, 0, 1));
            }
        }
    }
}
