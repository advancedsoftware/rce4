﻿using System;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class ValidationHelperMethodTests
    {
        #region IsNullableDateRangeValid Tests
        //This section contains all the unit tests for IsNullableDateRangeValid.
        [Fact]
        public void Both_Values_False_Returns_True() {
            Assert.True(new ValidationHelper(null).IsNullableDateRangeValid(null, null));
        }

        [Fact]
        public void Start_Null_And_End_Value_Returns_False() {
            Assert.False(new ValidationHelper(null).IsNullableDateRangeValid(null, DateTime.Now));
        }

        [Fact]
        public void End_Null_And_Start_Value_Returns_False() {
            Assert.False(new ValidationHelper(null).IsNullableDateRangeValid(DateTime.Now, null));
        }

        [Fact]
        public void Start_Less_Than_End_Returns_True() {
            DateTime testValue = DateTime.Now;
            Assert.True(new ValidationHelper(null).IsNullableDateRangeValid(testValue, testValue.AddTicks(1)));
        }

        [Fact]
        public void Start_Equal_End_Returns_False() {
            DateTime testValue = DateTime.Now;
            Assert.False(new ValidationHelper(null).IsNullableDateRangeValid(testValue, testValue));
        }

        [Fact]
        public void Start_Greater_Than_End_Returns_False() {
            DateTime testValue = DateTime.Now;
            Assert.False(new ValidationHelper(null).IsNullableDateRangeValid(testValue.AddTicks(1), testValue));
        }
        #endregion IsNullableDateRangeValid Tests
    }
}
