﻿using System;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class IsNullableDateRangeValidTest {
        private ValidationHelper Helper => new ValidationHelper(new RodeoContext());

        [Fact]
        public void Both_Values_Null_Returns_True() {
            Assert.True(Helper.IsNullableDateRangeValid(null, null));
        }
        [Fact]
        public void End_Greater_Than_Start_Returns_True() {
            Assert.True(Helper.IsNullableDateRangeValid(DateTime.Now, DateTime.Now.AddMilliseconds(2)));
        }
        [Fact]
        public void End_Less_Than_Start_Returns_False() {
            Assert.False(Helper.IsNullableDateRangeValid(DateTime.Now, DateTime.Now.AddMilliseconds(-2)));
        }
        [Fact]
        public void End_Null_And_Start_HasValue_Returns_False() {
            Assert.False(Helper.IsNullableDateRangeValid(DateTime.Now, null));
        }
        [Fact]
        public void Start_Null_And_End_HasValue_Returns_False() {
            Assert.False(Helper.IsNullableDateRangeValid(null, DateTime.Now));
        }
    }
}
