﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsSlackForAssociationEventTest : MockDbTestBase {
        public AssociationEvent AssociationEvent = new AssociationEvent {
            Id = 1,
            IsSlack = true
        };

        [Fact]
        public void AssociationEvent_Not_IsSlack_Returns_False() {
            using (RodeoContext db = MockContext()) {
                AssociationEvent.IsSlack = false;
                db.AssociationEvents.Add(AssociationEvent);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsSlackForAssociationEvent(AssociationEvent.Id));
            }
        }

        [Fact]
        public void AssociationEvent_IsSlack_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.AssociationEvents.Add(AssociationEvent);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsSlackForAssociationEvent(AssociationEvent.Id));
            }
        }

    }
}
