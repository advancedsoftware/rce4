﻿using System;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsPerformanceDateBetweenRodeoDatesTest : MockDbTestBase {
        public Rodeo Rodeo = new Rodeo {
            Id = 1,
            StartDate = new DateTime(2017, 1, 1),
            EndDate = new DateTime(2017, 1, 3).AddMilliseconds(-3)
        };
        public Performance Performance = new Performance {
            Id = 1,
            PerformanceDate = new DateTime(2017, 1, 1, 18, 0, 0)
        };

        [Fact]
        public void PerformanceDateTime_Between_Start_And_End_Dates_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(Rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsPerformanceDateBetweenRodeoDates(Rodeo.Id, Rodeo.StartDate.AddMilliseconds(1000)));
            }
        }

        [Fact]
        public void PerfomanceDateTime_Less_Than_StartDate_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(Rodeo);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsPerformanceDateBetweenRodeoDates(Rodeo.Id, Rodeo.StartDate.AddMilliseconds(-3)));
            }
        }

        [Fact] public void PerformanceDate_Greater_Than_EndDate_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(Rodeo);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsPerformanceDateBetweenRodeoDates(Rodeo.Id, Rodeo.EndDate.AddMilliseconds(3)));
            }
        }

        [Fact]
        public void PerfomanceDateTime_Equals_EndDate_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(Rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsPerformanceDateBetweenRodeoDates(Rodeo.Id, Rodeo.EndDate));
            }
        }

        [Fact]
        public void PerformanceDateTime_Equals_StartDate_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(Rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsPerformanceDateBetweenRodeoDates(Rodeo.Id, Rodeo.StartDate));
            }
        }
    }
}
