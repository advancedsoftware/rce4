﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EnterUp.Common.Exceptions;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class GetAnnotationErrorsTest :BaseTestClass {
        [Fact]
        public void EmptyListOfPropertyErrorsReturned() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.NotNull(helper.GetAnnotationErrors(new object()));
            }
        }
        [Fact]
        public void NoAnnotations_NoException() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.NotNull(helper.GetAnnotationErrors(new NoAnnotations()));
            }
        }
        [Fact]
        public void WithAnnotations_NoException() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.NotNull(helper.GetAnnotationErrors(new WithAnnotations()));
            }
        }
        [Fact]
        public void NoAnnotations_No_Errors() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.Empty(helper.GetAnnotationErrors(new NoAnnotations()));
            }
        }
        [Fact]
        public void WithAnnotations_No_Errors_Valid() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.Empty(helper.GetAnnotationErrors(new WithAnnotations {
                    FirstName = "first name",
                    Name = "ASDFAS",
                    Id = 5
                }));
            }
        }
        [Fact]
        public void WithAnnotations_Single_Error() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.Single(helper.GetAnnotationErrors(new WithAnnotations {
                    FirstName = "first name",
                    Name = "",
                    Id = 5
                }));
            }
        }
        [Fact]
        public void WithAnnotations_Multiple_Error() {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                Assert.Equal(3, helper.GetAnnotationErrors(new WithAnnotations {
                    Name = "",
                    Id = 0
                }).Count);
            }
        }
        [Fact]
        public void PropertyError_Items_Populated () {
            using (RodeoContext db = GetRodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                List<PropertyError> propertyErrors = helper.GetAnnotationErrors(new WithAnnotations {
                    Name = "a",
                    Id = 0
                });
                //This one proves that the string splitting is happening.
                Assert.Equal("First Name", propertyErrors[0].PropertyName);
                Assert.Equal("First Name is required.", propertyErrors[0].ErrorString);
                Assert.Equal("Id", propertyErrors[1].PropertyName);
                Assert.Equal("Not in the right range.", propertyErrors[1].ErrorString);
                Assert.Equal("Name", propertyErrors[2].PropertyName);
                Assert.Equal("Wrong Length.", propertyErrors[2].ErrorString);
            }
        }
    }

    public class NoAnnotations {
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class WithAnnotations {
        [Required(ErrorMessage = " Name is required. ")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = " Wrong Length. ")]
        public string Name { get; set; }
        [Required(ErrorMessage = " First Name is required. ")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = " Wrong Length. ")]
        public string FirstName { get; set; }
        [Range(1, 10, ErrorMessage = " Not in the right range. ")]
        public int Id { get; set; }
    }
}
