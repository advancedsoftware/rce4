﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class IsPerformanceInRodeoTest : MockDbTestBase {
        private readonly Performance performance = new Performance() {
            Id = 1,
            RodeoId = 1
        };

        private readonly Performance performance2 = new Performance() {
            Id = 2,
            RodeoId = 2
        };

        [Fact]
        public void RodeoId_Equals_Performance_RodeoId_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Performances.Add(performance);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsPerformanceInRodeo(performance.RodeoId, performance.Id));
            }
        }

        [Fact]
        public void RodeoId_Not_Equals_Performance_RodeoId_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.Performances.Add(performance2);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsPerformanceInRodeo(performance.RodeoId, performance.Id));
            }
        }
    }
}