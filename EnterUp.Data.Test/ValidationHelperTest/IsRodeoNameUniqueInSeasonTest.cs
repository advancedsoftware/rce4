﻿using System;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsRodeoNameUniqueInSeasonTest : MockDbTestBase {
        private readonly Rodeo rodeo = new Rodeo {
            Id = 1,
            Name = "Test",
            StartDate = new DateTime(2017, 1, 1),
            EndDate = new DateTime(2017, 1, 3).AddMilliseconds(-3)
        };

        [Fact]
        public void Matches_On_Deleted_Rodeos_Return_True() {
            using (RodeoContext db = MockContext()) {
                rodeo.DeletedById = "1";
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test", 2017));
            }
        }
        [Fact]
        public void Name_Match_For_Supplied_Id_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(1, "Test", 2017));
            }
        }

        [Fact]
        public void Name_Partial_Match_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test1", 2017));
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test 1", 2017));
            }
        }

        [Fact]
        public void Name_Match_Case_Insensitive() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "tEST", 2017));
            }
        }

        [Fact]
        public void Names_No_Match_Same_Season_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test1", 2017));
            }
        }

        [Fact]
        public void Names_No_Match_Different_Season_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test1", 2016));
            }
        }

        [Fact]
        public void Rodeos_Match_Name_Different_Years_Return_True() {
            using (RodeoContext db = MockContext()) {
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test", 2016));
            }
        }

        [Fact]
        public void Rodeos_Match_Name_On_First_Of_Year_Returns_False() {
            using (RodeoContext db = MockContext()) {
                rodeo.EndDate = rodeo.StartDate.AddDays(1).AddMilliseconds(-3);
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test", 2017));
            }
        }

        [Fact]
        public void Rodeos_Match_Name_On_Last_Of_Year_Returns_False() {
            using (RodeoContext db = MockContext()) {
                rodeo.EndDate = rodeo.StartDate.AddYears(1).AddMilliseconds(-3);
                db.Rodeos.Add(rodeo);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsRodeoNameUniqueInSeason(2, "Test", 2017));
            }
        }
    }
}
