﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class IsSwitchEndsRuleViolationTest : MockDbTestBase {
        [Fact]
        public void Entry_With_Different_Id_And_Matching_Positions_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowSwitchEnds = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1,
                    TeammateMemberId = 2
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsSwitchEndsRuleViolation(1, 0, 1, 2));
            }
        }
        [Fact]
        public void Entry_With_Same_Id_And_Switched_Ends_Return_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowSwitchEnds = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1,
                    TeammateMemberId = 2
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsSwitchEndsRuleViolation(1, 1, 2, 1));
            }
        }
        [Fact]
        public void Entry_With_Different_Id_And_Switched_Ends_Return_True() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowSwitchEnds = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1,
                    TeammateMemberId = 2,
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsSwitchEndsRuleViolation(1, 0, 2, 1));
            }
        }
        [Fact]
        public void RodeoEvent_Allows_SwitchEnds_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowSwitchEnds = true
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsSwitchEndsRuleViolation(1, 1, 0, 1));
            }
        }
        [Fact]
        public void RodeoEvent_Not_Exists_Returns_True() {
            using (RodeoContext db = MockContext()) {
                Assert.True(db.ValidationHelper.IsSwitchEndsRuleViolation(10, 1, 0, 1));
            }
        }
    }
}