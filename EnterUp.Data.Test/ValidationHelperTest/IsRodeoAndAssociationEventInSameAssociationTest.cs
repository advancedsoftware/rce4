﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsRodeoAndAssociationEventInSameAssociationTest : MockDbTestBase {
        private readonly Rodeo rodeo = new Rodeo {
            Id = 1,
            AssociationId = 1,
            Name = "Test"
        };

        private readonly AssociationEvent associationEvent = new AssociationEvent {
            Id = 1,
            AssociationId = 1,
            Name = "Test"
        };

        [Fact]
        public void AssociationEvent_AssociationId_Matches_Rodeo_AssociationId(){
            using (RodeoContext db = MockContext())
            {
                db.Rodeos.Add(rodeo);
                db.AssociationEvents.Add(associationEvent);
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper
                    .IsRodeoAndAssociationEventInSameAssociation(rodeo.AssociationId, associationEvent.AssociationId));
            }
        }

        [Fact]
        public void AssociationEvent_AssociationId_Not_Matches_Rodeo_AssociationId(){
            using (RodeoContext db = MockContext())
            {
                associationEvent.AssociationId = 2;
                db.Rodeos.Add(rodeo);
                db.AssociationEvents.Add(associationEvent);
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper
                    .IsRodeoAndAssociationEventInSameAssociation(rodeo.AssociationId, associationEvent.AssociationId));
            }
        }
    }
}