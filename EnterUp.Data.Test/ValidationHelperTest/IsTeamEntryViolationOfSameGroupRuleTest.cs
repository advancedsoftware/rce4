﻿using System;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsTeamEntryViolationOfSameGroupRuleTest : MockDbTestBase {
        [Fact]
        public void Different_Entries_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowPartnersOutsideBuddyGroup = false
                });
                db.RodeoEntryMembers.Add(new RodeoEntryMember {
                    Id = 2,
                    RodeoEntryId = 4,
                });
                db.RodeoEntryMembers.Add(new RodeoEntryMember {
                    Id = 3,
                    RodeoEntryId = 5,
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(1, 2, 3));
            }
        }
        [Fact]
        public void Teammates_In_Same_Entry_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowPartnersOutsideBuddyGroup = false
                });
                db.RodeoEntryMembers.Add(new RodeoEntryMember {
                    Id = 2,
                    RodeoEntryId = 4,
                });
                db.RodeoEntryMembers.Add(new RodeoEntryMember {
                    Id = 3,
                    RodeoEntryId = 4,
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(1, 2, 3));
            }
        }

        [Fact]
        public void TeammateMemberId_Invalid_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowPartnersOutsideBuddyGroup = false
                });
                db.RodeoEntryMembers.Add(new RodeoEntryMember {
                    Id = 2,
                    RodeoEntryId = 4,
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(1, 2, 3));
            }
        }
        [Fact]
        public void ContestantMemberId_Invalid_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowPartnersOutsideBuddyGroup = false
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(1, 2, 3));
            }
        }

        [Fact]
        public void RodeoEvent_Allows_Teammates_In_Separate_Entries_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowPartnersOutsideBuddyGroup = true
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(1, 2, 3));
            }
        }
        [Fact]
        public void RodeoEvent_Not_Exists_Returns_True() {
            using (RodeoContext db = MockContext()) {
                Assert.True(db.ValidationHelper.IsTeamEntryViolationOfSameGroupRule(10, 0, 1));
            }
        }
    }
}