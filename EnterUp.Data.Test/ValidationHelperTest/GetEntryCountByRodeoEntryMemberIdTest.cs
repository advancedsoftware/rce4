﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest {
    public class GetEntryCountByRodeoEntryMemberIdTest : MockDbTestBase {
        [Fact]
        public void Entries_Without_The_MemberId_Are_Not_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 2,
                    RodeoEventId = 1,
                    TeammateMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(0, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 0, 2));
            }
        }
        [Fact]
        public void Both_Teammate_And_Contestant_Are_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 2,
                    RodeoEventId = 1,
                    TeammateMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(2, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 0, 1));
            }
        }
        [Fact]
        public void Teammate_Entries_Are_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    TeammateMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(1, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 0, 1));
            }
        }
        [Fact]
        public void Contestant_Entries_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(1, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 0, 1));
            }
        }

        [Fact]
        public void ExcludedEntryId_Is_Not_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 2,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(1, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 2, 1));
            }
        }
        [Fact]
        public void Only_Entries_With_Matching_RodeoEvent_Are_Counted() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 1,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 2,
                    RodeoEventId = 2,
                    ContestantMemberId = 1
                });
                db.SaveChanges("1");
                Assert.Equal(1, db.ValidationHelper.GetEntryCountByRodeoEntryMemberId(1, 0, 1));
            }
        }
    }
}
