﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ValidationHelperTest
{
    public class IsDuplicationEntryViolationTest : MockDbTestBase {
        [Fact]
        public void Duplicate_Returns_True() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 2,
                    AllowDuplicateTeams = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 2,
                    ContestantMemberId = 3,
                    TeammateMemberId = 4
                });
                db.SaveChanges("1");
                Assert.True(db.ValidationHelper.IsDuplicationEntryViolation(2, 0, 3, 4));
            }
        }

        [Fact]
        public void Duplicate_With_Same_Id_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 2,
                    AllowDuplicateTeams = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 2,
                    ContestantMemberId = 3,
                    TeammateMemberId = 4
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsDuplicationEntryViolation(2, 1, 3, 4));
            }
        }

        [Fact]
        public void Duplicate_In_Different_Event_Return_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 2,
                    AllowDuplicateTeams = false
                });
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 20,
                    AllowDuplicateTeams = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 2,
                    ContestantMemberId = 3,
                    TeammateMemberId = 4
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsDuplicationEntryViolation(20, 1, 3, 4));
            }
        }

        [Fact]
        public void Different_Contestants_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 2,
                    AllowDuplicateTeams = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 2,
                    ContestantMemberId = 3,
                    TeammateMemberId = 4
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsDuplicationEntryViolation(2, 1, 5, 6));
            }
        }

        [Fact]
        public void SwitchEnds_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 2,
                    AllowDuplicateTeams = false
                });
                db.RodeoEventEntries.Add(new RodeoEventEntry {
                    Id = 1,
                    RodeoEventId = 2,
                    ContestantMemberId = 3,
                    TeammateMemberId = 4
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsDuplicationEntryViolation(2, 1, 4, 3));
            }
        }
        [Fact]
        public void RodeoEvent_Is_AllowDuplicateTeams_Returns_False() {
            using (RodeoContext db = MockContext()) {
                db.RodeoEvents.Add(new RodeoEvent {
                    Id = 1,
                    AllowDuplicateTeams = true
                });
                db.SaveChanges("1");
                Assert.False(db.ValidationHelper.IsDuplicationEntryViolation(1, 0, 0, 1));
            }
        }
        [Fact]
        public void RodeoEvent_Not_Exists_Returns_True() {
            using (RodeoContext db = MockContext()) {
                Assert.True(db.ValidationHelper.IsDuplicationEntryViolation(10, 0, 0, 1));
            }
        }
    }
}