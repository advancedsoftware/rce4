﻿using System.Linq;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    public class AssociationEventDataOperationTest : BaseTestClass {
        [Fact]
        public void Valid_Saved_Successfully() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
                RodeoContext.Associations.Add(AssociationGenerator.CreateAssociation(1));
                RodeoContext.SaveChanges("1");
                RodeoContext.AssociationEvents.AddRange(AssociationEventGenerator.GetList(1));
                RodeoContext.SaveChanges("1");
            }
            using (RodeoContext db = new RodeoContext(GetContextBuilderForTest().Options)) {
                Assert.Equal(5, db.AssociationEvents.Count());
            }
        }

        [Fact]
        public void AssociationEvents_Added_To_Association_Saved_Successfully() {
            using (RodeoContext dbContext = new RodeoContext(GetContextBuilderForTest().Options)) {
                TestHelpers.ClearTableData(dbContext, "Associations");
                Association association = AssociationGenerator.CreateAssociation(1);
                association.AssociationEvents.AddRange(AssociationEventGenerator.GetList(1));
                dbContext.Associations.Add(association);
                dbContext.SaveChanges("1");
            }
            using (RodeoContext db = new RodeoContext(GetContextBuilderForTest().Options)) {
                Assert.Equal(5, db.AssociationEvents.Count());
            }
        }
    }
}
