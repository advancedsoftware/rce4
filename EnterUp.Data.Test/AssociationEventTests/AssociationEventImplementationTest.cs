﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    public class AssociationEventImplementationTest {
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new AssociationEvent() as IValidator);
        }
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new AssociationEvent() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new AssociationEvent() as IPermanent);
        }
    }
}
