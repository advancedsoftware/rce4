﻿using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests
{
    /// <summary>
    /// This class is used to test any property marked as NotMapped.
    /// </summary>
    public class AssociationEventPropertyTest {
        [Fact]
        public void IsDeletedFalse_Null() {
            AssociationEvent testObject = new AssociationEvent { DeletedById = null };
            Assert.False(testObject.IsDeleted);
        }

        [Fact]
        public void IsDeletedTrue() {
            AssociationEvent testObject = new AssociationEvent { DeletedById = "2" };
            Assert.True(testObject.IsDeleted);

        }
    }
}
