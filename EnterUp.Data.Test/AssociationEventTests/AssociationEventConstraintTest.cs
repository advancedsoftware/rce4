﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    public class AssociationEventConstraintTest : BaseTestClass {
        public AssociationEventConstraintTest() {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
        }

        [Fact]
        public void Name_Must_Be_Unique_WithIn_Association () {
            AssociationGenerator.Generate(RodeoContext, 2);
            AssociationEventGenerator.Generate(RodeoContext, 1);
            using (RodeoContext context = new RodeoContext(GetContextBuilderForTest().Options)) {
                List<AssociationEvent> events = AssociationEventGenerator.GetList(1);
                context.AssociationEvents.Add(events[0]);
                EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => context.SaveChanges("1"));
                Assert.Equal("Saving the Association Event failed.", ex.Message);
                TestHelpers.ErrorContains(ex.PropertyErrors,
                    "Name",
                    "The value (Event 00) for Name is already in use, the value for Name must be unique.");
                Assert.NotNull(ex.InnerException as DbUpdateException);
            }
        }
        [Fact]
        public void Name_Repeated_In_Another_Association_No_Exception () {
            AssociationGenerator.Generate(RodeoContext, 2);
            AssociationEventGenerator.Generate(RodeoContext, 1);
            AssociationEventGenerator.Generate(RodeoContext, 2);
        }
    }
}
