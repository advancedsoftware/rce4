﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    /// <summary>
    /// This class checks all of the annotation validation for the Association class. 
    /// </summary>
    public class AssociationEventAnnotationValidationTest {

        [Fact]
        public void TeammateLabel_Null_No_Error() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                TeammateLabel = null
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label");
        }

        [Fact]
        public void TeammateLabel_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                TeammateLabel = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label",
                "The Teammate Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void TeammateLabel_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                TeammateLabel = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label",
                "The Teammate Label must be between 5 and 100 characters.");

            testObject.TeammateLabel += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label",
                "The Teammate Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void TeammateLabel_Max_Length() {
            const int length = 100;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                TeammateLabel = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label",
                "The Teammate Label must be between 5 and 100 characters.");

            testObject.TeammateLabel += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Teammate Label",
                "The Teammate Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void ContestantLabel_Null_No_Error() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                ContestantLabel = null
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label");
        }

        [Fact]
        public void ContestantLabel_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                ContestantLabel = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label",
                "The Contestant Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void ContestantLabel_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                ContestantLabel = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label",
                "The Contestant Label must be between 5 and 100 characters.");

            testObject.ContestantLabel += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label",
                "The Contestant Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void ContestantLabel_Max_Length() {
            const int length = 100;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                ContestantLabel = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label",
                "The Contestant Label must be between 5 and 100 characters.");

            testObject.ContestantLabel += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Contestant Label",
                "The Contestant Label must be between 5 and 100 characters.");
        }

        [Fact]
        public void Name_Null() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                Name = null
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");
        }

        [Fact]
        public void Name_Empty() {
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                Name = string.Empty
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");
        }

        [Fact]
        public void Name_Min_Length() {
            const int length = 5;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                Name = TestHelpers.GetStringOfLength(length - 1)
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");

            testObject.Name += "A";
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");
        }

        [Fact]
        public void Name_Max_Length() {
            const int length = 100;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                Name = TestHelpers.GetStringOfLength(length)
            };
            TestHelpers.ErrorNotContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");

            testObject.Name += "A";
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Name",
                "The Name field is require and must be between 5 and 100 characters.");
        }
        [Fact]
        public void OrderIndex_Min() {
            const int value = 1;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                OrderIndex = value - 1
            };
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Order Index",
                "The value for OrderIndex must be between 1 and 99.");
            testObject.OrderIndex = value;
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Order Index");
        }

        [Fact]
        public void OrderIndex_Max() {
            const int value = 99;
            ValidationHelper validationHelper = new ValidationHelper(null);
            AssociationEvent testObject = new AssociationEvent {
                OrderIndex = value
            };
            TestHelpers.ErrorNotContainsPropertyName(validationHelper.GetAnnotationErrors(testObject),
                "Order Index");

            testObject.OrderIndex = value + 1;
            TestHelpers.ErrorContains(validationHelper.GetAnnotationErrors(testObject),
                "Order Index",
                "The value for OrderIndex must be between 1 and 99.");
        }
    }
}