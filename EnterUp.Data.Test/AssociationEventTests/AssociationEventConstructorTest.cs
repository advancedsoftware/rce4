﻿using EnterUp.Common.Enums;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    public class AssociationEventConstructorTest {
        [Fact]
        public void OrderIndex_1() {
            Assert.Equal(1, new AssociationEvent().OrderIndex);
        }

        [Fact]
        public void Name_Blank() {
            Assert.Equal(string.Empty, new AssociationEvent().Name);
        }

        [Fact]
        public void ScoreSheet_None() {
            Assert.Equal(ScoreSheetType.None, new AssociationEvent().ScoreSheet);
        }

        [Fact]
        public void ContestantLabel_Blank() {
            Assert.Equal(string.Empty, new AssociationEvent().ContestantLabel);
        }

        [Fact]
        public void TeammateLabel_Blank() {
            Assert.Equal(string.Empty, new AssociationEvent().TeammateLabel);
        }
    }
}
