﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.AssociationEventTests {
    /// <summary>
    /// This class checks all of the validation for the Association Event class. 
    /// </summary>
    public class AssociationEventValidationTest : BaseTestClass {
        [Fact]
        public void Age_Range_Error_If_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "Associations");
            Association association = AssociationGenerator.CreateAssociation(1);
            RodeoContext.Associations.Add(association);
            RodeoContext.SaveChanges("1");
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.IsAgeSpecific = true;
            associationEvent.MinimumAge = 10;
            associationEvent.MaximumAge = 9;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => associationEvent.Validate(RodeoContext.ValidationHelper));
            Assert.Equal("The association event is invalid.", ex.Message);
            TestHelpers.ErrorContains(ex.PropertyErrors, "Ages", "The minimum and maximum ages must form a valid range for age specific events.");
        }

        [Fact]
        public void Age_Range_No_Error_If_Not_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.MinimumAge = 50;
            associationEvent.MaximumAge = 49;
            associationEvent.IsAgeSpecific = false;
            associationEvent.Validate(RodeoContext.ValidationHelper);
        }

        [Fact]
        public void MaximumAge_Error_If_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.MinimumAge = 0;
            associationEvent.MaximumAge = 0;
            associationEvent.IsAgeSpecific = true;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => associationEvent.Validate(RodeoContext.ValidationHelper));
            Assert.Equal("The association event is invalid.", ex.Message);
            TestHelpers.ErrorContains(ex.PropertyErrors, "Ages", "The minimum and maximum ages must form a valid range for age specific events.");
        }

        [Fact]
        public void MaximumAge_No_Error_If_Not_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.MinimumAge = 50;
            associationEvent.MaximumAge = 0;
            associationEvent.Validate(RodeoContext.ValidationHelper);
        }

        [Fact]
        public void MinimumAge_Error_If_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.MinimumAge = 0;
            associationEvent.MaximumAge = 50;
            associationEvent.IsAgeSpecific = true;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => associationEvent.Validate(RodeoContext.ValidationHelper));
            Assert.Equal("The association event is invalid.", ex.Message);
            TestHelpers.ErrorContains(ex.PropertyErrors, "Ages", "The minimum and maximum ages must form a valid range for age specific events.");
        }

        [Fact]
        public void MinimumAge_No_Error_If_Not_IsAgeSpecific() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.MinimumAge = 0;
            associationEvent.MaximumAge = 50;
            associationEvent.Validate(RodeoContext.ValidationHelper);
        }

        [Fact]
        public void TeammateLabel_Required_If_IsTeamEvent() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.ContestantLabel = "Contestant Label";
            associationEvent.TeammateLabel = "";
            associationEvent.IsTeamEvent = true;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => associationEvent.Validate(RodeoContext.ValidationHelper));
            Assert.Equal("The association event is invalid.", ex.Message);
            TestHelpers.ErrorContains(ex.PropertyErrors, "Teammate Label", "The teammate label is required for team events.");
        }

        [Fact]
        public void ContestantLabel_Required_If_IsTeamEvent() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.ContestantLabel = "";
            associationEvent.TeammateLabel = "Teammate Label";
            associationEvent.IsTeamEvent = true;
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => associationEvent.Validate(RodeoContext.ValidationHelper));
            Assert.Equal("The association event is invalid.", ex.Message);
            TestHelpers.ErrorContains(ex.PropertyErrors, "Contestant Label", "The contestant label is required for team events.");
        }

        [Fact]
        public void Labels_Not_Required_If_Not_Team_Event() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.ContestantLabel = "";
            associationEvent.TeammateLabel = "";
            associationEvent.IsTeamEvent = false;
            associationEvent.Validate(RodeoContext.ValidationHelper);
        }

        [Fact]
        public void Labels_Changed_From_Empty_To_Blank_On_Validate() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.ContestantLabel = string.Empty;
            associationEvent.TeammateLabel = string.Empty;
            associationEvent.IsTeamEvent = false;
            associationEvent.Validate(RodeoContext.ValidationHelper);
            Assert.Null(associationEvent.TeammateLabel);
            Assert.Null(associationEvent.ContestantLabel);
        }

        [Fact]
        public void Labels_Trimmed_On_Validate() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            AssociationEvent associationEvent = AssociationEventGenerator.GetList(1)[0];
            associationEvent.ContestantLabel = " Contestant Label ";
            associationEvent.TeammateLabel = " Teammate Label ";
            associationEvent.IsTeamEvent = false;
            associationEvent.Validate(RodeoContext.ValidationHelper);
            Assert.Equal("Teammate Label", associationEvent.TeammateLabel);
            Assert.Equal("Contestant Label", associationEvent.ContestantLabel);
        }

        [Fact]
        public void GetAnnotationErrorsCalled() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            var validationHelper = Substitute.For<ValidationHelper>(RodeoContext);
            validationHelper.When(v => v.GetAnnotationErrors(Arg.Any<object>())).DoNotCallBase();
            validationHelper.GetAnnotationErrors(Arg.Any<object>())
                .Returns(new List<PropertyError> ());
            RodeoContext.ValidationHelper = validationHelper;
            RodeoContext.AssociationEvents.Add(AssociationEventGenerator.GetList(1)[0]);
            RodeoContext.SaveChanges("1");
            validationHelper.Received(1).GetAnnotationErrors(Arg.Any<object>());
        }

        [Fact]
        public void ExceptionThrowIfInvalid() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            var validationHelper = Substitute.For<ValidationHelper>(RodeoContext);
            validationHelper.When(v => v.GetAnnotationErrors(Arg.Any<object>())).DoNotCallBase();
            validationHelper.GetAnnotationErrors(Arg.Any<object>()).Returns(
                new List<PropertyError> {
                    new PropertyError {
                        PropertyName = "Test",
                        ErrorString = "Test error string."
                    }
                });
            RodeoContext.ValidationHelper = validationHelper;
            RodeoContext.AssociationEvents.Add(AssociationEventGenerator.GetList(1)[0]);
            Assert.Throws<EnterUpDataException>(
                () => RodeoContext.SaveChanges("1"));
        }

        [Fact]
        public void NoExceptionIfValid() {
            TestHelpers.ClearTableData(RodeoContext, "AssociationEvents");
            AssociationGenerator.Generate(RodeoContext);
            RodeoContext.AssociationEvents.Add(AssociationEventGenerator.GetList(1)[0]);
            RodeoContext.SaveChanges("1");
        }
    }
}