﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using EnterUp.Common.Exceptions;
using Newtonsoft.Json;
using Xunit;

namespace EnterUp.Data.Test.ErrorTests
{
    public class LogExceptionTest : BaseTestClass {
        [Fact]
        public void Error_Object_Saved_When_Valid() {
            int startingCount = RodeoContext.Errors.Count();
            try {
                new TestExGenerator().GetException();
            }
            catch (Exception ex) {
                Error.LogException(ex, "1");
            }
            Assert.Equal(startingCount + 1, RodeoContext.Errors.Count());
        }

        [Fact]
        public void Property_Errors_Serialized_And_Added_To_Error_Object() {
            int startingCount = RodeoContext.Errors.Count(e => !string.IsNullOrEmpty(e.ValidationErrors));
            try {
                new TestExGenerator().GetException2();
            }
            catch (Exception ex) {
                Error.LogException(ex, "1");
            }
            Assert.Equal(startingCount + 1, RodeoContext.Errors.Count(e => !string.IsNullOrEmpty(e.ValidationErrors)));
        }

        [Fact]
        public void Property_Errors_Can_Be_Deserialized() {
            List<PropertyError> inputValidationErrors = null;
            try {
                new TestExGenerator().GetException2();
            }
            catch (Exception ex) {
                inputValidationErrors = ((EnterUpDataException) ex).PropertyErrors;
                Error.LogException(ex, "1");
            }
            Error savedError = RodeoContext.Errors
                .Take(1)
                .OrderByDescending(e => e.Id)
                .First();
            List<PropertyError> errorList = 
                JsonConvert.DeserializeObject<List<PropertyError>>(savedError.ValidationErrors);
            Assert.Single(errorList);
            Debug.Assert(inputValidationErrors != null, nameof(inputValidationErrors) + " != null");
            Assert.Equal(inputValidationErrors[0].PropertyName, errorList[0].PropertyName);
            Assert.Equal(inputValidationErrors[0].ErrorString, errorList[0].ErrorString);
        }

        [Fact]
        public void Only_Last_1000_Errors_Retained() {
            try {
                new TestExGenerator().GetException();
            }
            catch (Exception ex) {
                for (int index = 0; index < 1100; index++) {
                    Error.LogException(ex, "1");
                    Assert.True(RodeoContext.Errors.Count() <= 1000);
                }
            }
        }

        
    }

    public class TestExGenerator {
        public void GetException() {
            throw new Exception("Test Exception");
        }

        public void GetException2() {
            new TestExGenerator2().GetException();
        }
    }

    public class TestExGenerator2 {
        public void GetException() {
            EnterUpDataException ex = new EnterUpDataException("EU Data Exception");
            ex.PropertyErrors.Add(new PropertyError{PropertyName = "Property Name", ErrorString = "Error String"});
            throw ex;
        }
    }
}
