﻿using System.Linq;
using Xunit;

namespace EnterUp.Data.Test.ErrorTests
{
    public class ErrorDataOperationTest : BaseTestClass {
        internal Error Error = new Error() {
            ErrorMessage = "Error Message",
            StackTrace = "Stack Trace"
        };

        [Fact]
        public void Valid_Object_Is_Added() {
            using (RodeoContext db = new RodeoContext()) {
                int initialCount = db.Errors.Count();
                db.Errors.Add(Error);
                db.SaveChanges("1");
                Assert.Equal(initialCount + 1, db.Errors.Count());
            }
        }

        [Fact]
        public void Valid_Object_Is_Updated() {
            using (RodeoContext db = new RodeoContext()) {
                int startingCount = db.Errors.Count();
                db.Errors.Add(Error);
                db.SaveChanges("1");
                Assert.Equal(startingCount + 1, db.Errors.Count());
                Error.ErrorMessage += "a";
                db.SaveChanges("1");
                Assert.Equal(startingCount + 1, db.Errors.Count());
            }
        }

        
    }
}
