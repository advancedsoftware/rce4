﻿using Xunit;

namespace EnterUp.Data.Test.ErrorTests {
    public class ErrorValidationTest : BaseTestClass {
        public Error TestError { get; set; }

        public ValidationHelper ValidationHelper { get; set; }

        public ErrorValidationTest() {
            ValidationHelper = new ValidationHelper(RodeoContext);
            TestError = new Error {
                ErrorMessage = "Test Message",
                StackTrace = "Test StackTrace"
            };
        }

        [Fact]
        public void ValidationErrors_Null_Changed_To_Empty() {
            TestError.Validate(ValidationHelper);
            Assert.Equal(string.Empty, TestError.ValidationErrors);
        }

        [Fact]
        public void ValidationErrors_Not_Empty_Not_Changed() {
            const string testString = "Test Value";
            TestError.ValidationErrors = testString;
            TestError.Validate(ValidationHelper);
            Assert.Equal(testString, TestError.ValidationErrors);
        }

        
    }
}
