﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.ErrorTests
{
    public class ErrorImplementationTest {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Error() as IUpdatedTracker);
        }

        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Error() as IValidator);
        }
    }
}
