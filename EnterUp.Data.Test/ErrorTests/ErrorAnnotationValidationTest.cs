﻿using System.Collections.Generic;
using EnterUp.Common.Exceptions;
using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.ErrorTests
{
    public class ErrorAnnotationValidationTest : BaseTestClass {
        internal ValidationHelper ValidationHelper = new ValidationHelper(null);
        internal Error Error = new Error() {
            ErrorMessage = "Error Message",
            StackTrace = "Stack Trace"
        };

        private void CheckError(string expectedProperty, string expectedErrorText) {
            List<PropertyError> errors = ValidationHelper.GetAnnotationErrors(Error);
            TestHelpers.ErrorContains(
                errors,
                expectedProperty,
                expectedErrorText);
        }

        private void NoError(string propertyName, string errorText) {
            List<PropertyError> errors = ValidationHelper.GetAnnotationErrors(ValidationHelper);
            TestHelpers.ErrorNotContains(errors, propertyName, errorText);
        }

        [Fact]
        public void ErrorMessage_Equals_Blank_Throws_EnterUpDataException() {
            const string propertyName = "Error Message";
            const string errorMessage = "Error messages must not be blank.";
            Error.ErrorMessage = string.Empty;
            CheckError(propertyName, errorMessage);
        }

        [Fact]
        public void ErrorMessage_Equals_Null_Throws_EnterUpDataException() {
            const string propertyName = "Error Message";
            const string errorMessage = "Error messages must not be blank.";
            Error.ErrorMessage = null;
            CheckError(propertyName, errorMessage);
        }


        [Fact]
        public void StackTrace_Equals_Blank_Throws_EnterUpDataException() {
            const string propertyName = "Stack Trace";
            const string errorMessage = "Stack trace must not be blank.";
            Error.StackTrace = string.Empty;
            CheckError(propertyName, errorMessage);
        }


        [Fact]
        public void StackTrace_Equals_Null_Throws_EnterUpDataException() {
            const string propertyName = "Stack Trace";
            const string errorMessage = "Stack trace must not be blank.";
            Error.StackTrace = string.Empty;
            CheckError(propertyName, errorMessage);
        }

        [Fact]
        public void No_Error_When_StackTrace_And_Message_Are_Present() {
            const string propertyName = "Stack Trace";
            const string errorMessage = "Stack trace must not be blank.";
            NoError(propertyName, errorMessage);
        }

        [Fact]
        public void ErrorMessage_Max_Length_512() {
            const string propertyName = "Error Message";
            const string errorMessage = "The error message has a maximum length of 512 characters.";
            string testMessage = string.Empty;
            for (int i = 0; i < 512; i++) {
                testMessage += "A";
            }
            Error.ErrorMessage = testMessage;
            NoError(propertyName, errorMessage);
            Error.ErrorMessage += "a";
            CheckError(propertyName, errorMessage);
        }
    }
}
