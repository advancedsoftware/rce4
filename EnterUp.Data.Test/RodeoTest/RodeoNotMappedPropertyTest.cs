﻿using Xunit;

namespace EnterUp.Data.Test.RodeoTest {
    public class RodeoNotMappedPropertyTest {
        [Fact]
        public void IsClosed_Controlled_By_ClosedBy() {
            Rodeo rodeo = new Rodeo();
            Assert.False(rodeo.IsClosed);
            rodeo.ClosedById = "-1";
            Assert.True(rodeo.IsClosed);
        }

        [Fact]
        public void IsScheduled_Is_Controlled_By_SchedulingBy() {
            Rodeo rodeo = new Rodeo();
            Assert.False(rodeo.IsScheduled);
            rodeo.ScheduledById = "-1";
            Assert.True(rodeo.IsScheduled);
        }

        [Fact]
        public void IsSchedulingRun_Is_Controlled_By_SchedulingRunBy() {
            Rodeo rodeo = new Rodeo();
            Assert.False(rodeo.IsSchedulingRun);
            rodeo.SchedulingRunById = "-1";
            Assert.True(rodeo.IsSchedulingRun);
        }

        [Fact]
        public void IsScheduled_Is_Controlled_By_ScheduledBy() {
            Rodeo rodeo = new Rodeo();
            Assert.False(rodeo.IsScheduled);
            rodeo.ScheduledById = "-1";
            Assert.True(rodeo.IsScheduled);
        }

        [Fact]
        public void IsDeleted_Is_Controlled_By_DeletedBy() {
            Rodeo rodeo = new Rodeo();
            Assert.False(rodeo.IsDeleted);
            rodeo.DeletedById = "-1";
            Assert.True(rodeo.IsDeleted);
        }
    }
}
