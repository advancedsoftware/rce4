﻿using EnterUp.Test.Common;
using Xunit;

namespace EnterUp.Data.Test.RodeoTest
{
    public class RodeoAnnotationValidationTests : BaseTestClass {
        public Rodeo Test = new Rodeo {
            AssociationId = 1,
            Name = "Rodeo Name",
        };

        [Fact]
        public void Name_Min_Length_5() {
            Test.Name = TestHelpers.GetStringOfLength(4);
            TestHelpers.ValidationErrorsContains(Test, "Name", "The name must be between 5 and 100 characters.");
            Test.Name = TestHelpers.GetStringOfLength(5);
            TestHelpers.NoAnnotationError(Test);
        }

        [Fact]
        public void Name_Max_Lenth_100() {
            Test.Name = TestHelpers.GetStringOfLength(100);
            TestHelpers.NoAnnotationError(Test);
            Test.Name = TestHelpers.GetStringOfLength(101);
            TestHelpers.ValidationErrorsContains(Test, "Name", "The name must be between 5 and 100 characters.");
        }
    }
}
