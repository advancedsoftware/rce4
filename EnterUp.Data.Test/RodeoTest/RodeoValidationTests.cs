﻿using System;
using System.Collections.Generic;
using EnterUp.Common;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using EnterUp.Test.Common;
using NSubstitute;
using Xunit;

namespace EnterUp.Data.Test.RodeoTest {
    public class RodeoValidationTests : BaseTestClass {
        public Rodeo Test = new Rodeo {
            TimeZoneId = TimeZoneItem.GetApplicationItems()[0].ZoneId,
            StartDate = DateTime.Today.AddDays(-1),
            EndDate = DateTime.Today.AddDays(1).AddMilliseconds(-3)
        };

        public IValidationHelper ValidationHelper { get; set; }

        public RodeoValidationTests() {
            ConfigureValidationHelper();
        }
        protected void ConfigureValidationHelper() {
            ValidationHelper = Substitute.For<IValidationHelper>();
            ValidationHelper
                .When(v => v.GetAnnotationErrors(Arg.Any<IValidator>()))
                .DoNotCallBase();
            ValidationHelper
                .GetAnnotationErrors(Arg.Any<IValidator>())
                .Returns(new List<PropertyError>());
            ValidationHelper
                .When(v => v.IsRodeoNameUniqueInSeason(Arg.Any<int>(), Arg.Any<string>(), Arg.Any<int>()))
                .DoNotCallBase();
            ValidationHelper
                .IsRodeoNameUniqueInSeason(Arg.Any<int>(), Arg.Any<string>(), Arg.Any<int>())
                .Returns(true);
            ValidationHelper
                .When(v => v.IsNullableDateRangeValid(Arg.Any<DateTime?>(), Arg.Any<DateTime?>()))
                .DoNotCallBase();
            ValidationHelper
                .IsNullableDateRangeValid(Arg.Any<DateTime?>(), Arg.Any<DateTime?>())
                .Returns(true);
        }

        #region Results Posted Value Tests
        [Fact]
        public void ReultsPostedBy_Values_Must_Both_Be_Set() {
            Test.ResultsPostedById = null;
            Test.ResultsPostedAt = null;
            TestHelpers.NoError(Test, ValidationHelper);
            Test.ResultsPostedById = "-1";
            Test.ResultsPostedAt = null;
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Results Posted",
                "Posting results for a rodeo requires a value for both the results posted by and results posted at properties."
            );
            ConfigureValidationHelper();
            Test.ResultsPostedById = null;
            Test.ResultsPostedAt = DateTime.Today;
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Results Posted",
                "Posting results for a rodeo requires a value for both the results posted by and results posted at properties."
            );
        }
        #endregion Results Posted Value Tests

        #region Scheduled Value Tests
        [Fact]
        public void ScheduledAt_Must_Be_Less_Than_Equal_StartDate() {
            Test.ScheduledById = "-1";
            Test.ScheduledAt = Test.EndDate.AddTicks(1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Scheduled Date",
                "The date a rodeo is scheduled must be less than the rodeo start date.");

        }
        [Fact]
        public void ScheduledBy_Null_ScheduledAt_Value_Exception() {
            Test.ScheduledById = null;
            Test.ScheduledAt = Test.EndDate;
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Scheduled",
                "Scheduling a rodeo requires a value for both the scheduled by and scheduled at properties.");
        }
        [Fact]
        public void ScheduledBy_ScheduledAt_Both_Have_Values_No_Error() {
            Test.ScheduledById = "-1";
            Test.ScheduledAt = Test.StartDate;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        [Fact]
        public void ScheduledBy_And_Scheduled_At_Both_Null_No_Error() {
            Test.ScheduledById = null;
            Test.ScheduledAt = null;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        #endregion Scheduled Value Tests

        #region Closed Value Tests
        [Fact]
        public void ClosedAt_Must_Be_Less_Than_Equal_EndDate() {
            Test.ClosedById = "-1";
            Test.ClosedAt = Test.EndDate.AddTicks(1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Closed Date",
                "The date a rodeo is closed must be less than the rodeo end date.");

        }
        [Fact]
        public void ClosedBy_Null_ClosedAt_Value_Exception() {
            Test.ClosedById = null;
            Test.ClosedAt = Test.EndDate;
            TestHelpers.ErrorContains(Test, ValidationHelper, "Closed", "Closing a rodeo requires a value for both the closed by and closed at properties.");
        }
        [Fact]
        public void ClosedBy_ClosedAt_Both_Have_Values_No_Error() {
            Test.ClosedById = "-1";
            Test.ClosedAt = Test.EndDate;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        [Fact]
        public void ClosedBy_And_Closed_At_Both_Null_No_Error() {
            Test.ClosedById = null;
            Test.ClosedAt = null;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        #endregion Closed Value Tests

        #region Message Text
        [Fact]
        public void Message_Blank_When_Null() {
            Test.Message = null;
            Test.Validate(ValidationHelper);
            Assert.Empty(Test.Message);
        }
        #endregion Message Text

        #region Late Entry Date Tests
        //The rules for late entry dates are exactly the same as the rules for call back dates.
        [Fact]
        public void LateEntry_Dates_Start_Before_Call_In_Ends() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.LateEntryStart = Test.CallInEnd.Value.AddMilliseconds(-1);
            Test.LateEntryEnd = Test.LateEntryStart.Value.AddTicks(1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Late Entry Dates",
                "The late entry period must start after the call in period ends.");
        }

        [Fact]
        public void LateEntry_Dates_Start_When_CallIn_Ends_No_Error() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.LateEntryStart = Test.CallInEnd.Value;
            Test.LateEntryEnd = Test.LateEntryStart.Value.AddTicks(1);
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void LateEntry_Dates_Start_After_CallIn_Ends_No_Error() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.LateEntryStart = Test.CallInEnd.Value.AddTicks(1);
            Test.LateEntryEnd = Test.LateEntryStart.Value;
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void LateEntry_Dates_End_After_Rodeo_Starts() {
            Test.LateEntryEnd = Test.StartDate.AddTicks(1);
            Test.LateEntryStart = Test.LateEntryEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Late Entry Dates",
                "The late entry period must end before the rodeo starts.");
        }

        [Fact]
        public void LateEntryDates_End_When_Rodeo_Starts_Error() {
            Test.LateEntryEnd = Test.StartDate;
            Test.LateEntryStart = Test.LateEntryEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Late Entry Dates",
                "The late entry period must end before the rodeo starts.");
        }

        [Fact]
        public void LateEntry_Dates_End_Before_Rodeo_Start_No_Error() {
            Test.LateEntryEnd = Test.StartDate.AddTicks(-1);
            Test.LateEntryStart = Test.LateEntryEnd.Value.AddTicks(-1);
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void LateEntry_Dates_Must_Form_Valid_Range() {
            ValidationHelper
                .IsNullableDateRangeValid(Arg.Any<DateTime?>(), Arg.Any<DateTime?>())
                .Returns(false);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Late Entry Dates",
                "The late entry dates don't form a valid range.");
        }

        [Fact]
        public void LateEntry_Dates_Null_No_Error() {
            Test.LateEntryEnd = null;
            Test.LateEntryStart = null;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        #endregion Late Entry Date Tests

        #region Call Back Date Tests
        [Fact]
        public void CallBack_Dates_Start_Before_Call_In_Ends() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.CallBackStart = Test.CallInEnd.Value.AddMilliseconds(-1);
            Test.CallBackEnd = Test.CallBackStart.Value.AddTicks(1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call Back Dates",
                "The call back period must start after the call in period ends.");
        }

        [Fact]
        public void CallBack_Dates_Start_When_CallIn_Ends_No_Error() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.CallBackStart = Test.CallInEnd.Value;
            Test.CallBackEnd = Test.CallBackStart.Value.AddTicks(1);
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void CallBack_Dates_Start_After_CallIn_Ends_No_Error() {
            Test.CallInEnd = Test.StartDate.AddDays(-1).AddHours(18);
            Test.CallInStart = Test.CallInEnd.Value.AddHours(-10);
            Test.CallBackStart = Test.CallInEnd.Value.AddTicks(1);
            Test.CallBackEnd = Test.CallBackStart.Value;
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void CallBack_Dates_End_After_Rodeo_Starts() {
            Test.CallBackEnd = Test.StartDate.AddTicks(1);
            Test.CallBackStart = Test.CallBackEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call Back Dates",
                "The call back period must end before the rodeo starts.");
        }

        [Fact]
        public void CallBackDates_End_When_Rodeo_Starts_Error() {
            Test.CallBackEnd = Test.StartDate;
            Test.CallBackStart = Test.CallBackEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call Back Dates",
                "The call back period must end before the rodeo starts.");
        }

        [Fact]
        public void CallBack_Dates_End_Before_Rodeo_Start_No_Error() {
            Test.CallBackEnd = Test.StartDate.AddTicks(-1);
            Test.CallBackStart = Test.CallBackEnd.Value.AddTicks(-1);
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void CallBack_Dates_Must_Form_Valid_Range() {
            ValidationHelper
                .IsNullableDateRangeValid(Arg.Any<DateTime?>(), Arg.Any<DateTime?>())
                .Returns(false);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call Back Dates",
                "The call back dates don't form a valid range.");
        }

        [Fact]
        public void CallBack_Dates_Null_No_Error() {
            Test.CallBackEnd = null;
            Test.CallBackStart = null;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        #endregion Call Back Date Tests

        #region Call In Date Tests
        [Fact]
        public void CallIn_Dates_End_After_Rodeo_Starts() {
            Test.CallInEnd = Test.StartDate.AddTicks(1);
            Test.CallInStart = Test.CallInEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call In Dates",
                "The call in period must end before the rodeo starts.");
        }

        [Fact]
        public void CallInDates_End_When_Rodeo_Starts_Error() {
            Test.CallInEnd = Test.StartDate;
            Test.CallInStart = Test.CallInEnd.Value.AddTicks(-1);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call In Dates",
                "The call in period must end before the rodeo starts.");
        }

        [Fact]
        public void CallIn_Dates_End_Before_Rodeo_Start_No_Error() {
            Test.CallInEnd = Test.StartDate.AddTicks(-1);
            Test.CallInStart = Test.CallInEnd.Value.AddTicks(-1);
            TestHelpers.NoError(Test, ValidationHelper);
        }

        [Fact]
        public void CallIn_Dates_Must_Form_Valid_Range() {
            ValidationHelper
                .IsNullableDateRangeValid(Arg.Any<DateTime?>(), Arg.Any<DateTime?>())
                .Returns(false);
            TestHelpers.ErrorContains(Test,
                ValidationHelper,
                "Call In Dates",
                "The call in dates don't form a valid range.");
        }

        [Fact]
        public void CallIn_Dates_Null_No_Error() {
            Test.CallInEnd = null;
            Test.CallInStart = null;
            TestHelpers.NoError(Test, ValidationHelper);
        }
        #endregion Call In Date Tests

        [Fact]
        public void Rodeo_Cannot_Be_Edited_After_EndDate() {
            Test.EndDate = DateTime.Now.AddMilliseconds(-3);
            Test.StartDate = Test.EndDate.AddMilliseconds(-3);
            TestHelpers.ErrorContains(Test, ValidationHelper, "End Date", "A rodeo cannot be edited after the rodeo is complete.");
        }

        [Fact]
        public void Validation_Exception_Message_Text() {
            ValidationHelper
                .IsRodeoNameUniqueInSeason(Arg.Any<int>(), Arg.Any<string>(), Arg.Any<int>())
                .Returns(false);
            EnterUpDataException ex = Assert.Throws<EnterUpDataException>(() => Test.Validate(ValidationHelper));
            Assert.Equal("The rodeo is invalid and cannot be saved.", ex.Message);
        }

        [Fact]
        public void Rodeo_Name_Must_Be_Unique_In_A_Season() {
            TestHelpers.NoError(Test, ValidationHelper);

            ValidationHelper
                .IsRodeoNameUniqueInSeason(Arg.Any<int>(), Arg.Any<string>(), Arg.Any<int>())
                .Returns(false);
            TestHelpers.ErrorContains(Test, ValidationHelper, "Name", "The rodeo name must be unique within a season.");
        }

        [Fact]
        public void Rodeo_TimeZone_Must_Be_In_TimeZone_List() {
            Test.TimeZoneId = TimeZoneItem.GetApplicationItems()[0].ZoneId;
            TestHelpers.NoError(Test, ValidationHelper);
            Test.TimeZoneId += "A";
            TestHelpers.ErrorContains(Test, ValidationHelper, "Time Zone", "The time zone identifier is not valid.");
        }

        [Fact]
        public void StartDate_Must_Be_Less_Than_EndDate() {
            Test.StartDate = Test.EndDate;
            TestHelpers.ErrorContains(Test, ValidationHelper, "Rodeo Dates",
                "The start and end dates don't form a valid range.");
        }
    }
}