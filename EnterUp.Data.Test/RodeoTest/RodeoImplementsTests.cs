﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.RodeoTest {
    public class RodeoImplementsTests {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Rodeo() as IUpdatedTracker);
        }
        [Fact]
        public void ImplementsIPermanent() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Rodeo() as IPermanent);
        }
        [Fact]
        public void ImplementsIValidator() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new Rodeo() as IValidator);
        }
    }
}