﻿using EnterUp.Data.Interfaces;
using Xunit;

namespace EnterUp.Data.Test.UserClaimTests
{
    public class UserClaimImplementationTest {
        [Fact]
        public void ImplementsIUpdateTracker() {
            // ReSharper disable once RedundantCast
            Assert.NotNull(new UserClaim() as IUpdatedTracker);
        }
    }
}
