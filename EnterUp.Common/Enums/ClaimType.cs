﻿namespace EnterUp.Common.Enums
{
    public enum ClaimType {
        User = 0,
        SiteAdmin = 1,
        AssociationAdmin = 2,
        AssociationStaff = 3,
        AssociaitonScheduler = 4,
        AssociationContestant = 5,
        EntryContestant = 6
    }
}
