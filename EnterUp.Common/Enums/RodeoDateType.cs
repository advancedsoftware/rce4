﻿namespace EnterUp.Common.Enums {
    public enum RodeoDateType {
        CallInStart = 0,
        CallInEnd = 1,
        CallBackStart = 2,
        CallBackEnd = 3,
        LateEntryStart = 4,
        LateEntryEnd = 5
    }
}