﻿namespace EnterUp.Common.Enums {
    public enum ScoreSheetType {
        None,
        BarrelRacingTimerSheet,
        JudgeSheet,
        TeamRopingTimerSheet,
        TimerSheet
    }
}