﻿using System;

namespace EnterUp.Common.Exceptions {
    public class EnterUpException : Exception {
        public EnterUpException(string message) : base(message) { }
        public EnterUpException(string message, Exception ex) : base(message, ex) { }
    }
}