﻿using System;
using System.Collections.Generic;

namespace EnterUp.Common.Exceptions {
    /// <summary>
    /// This class is used to containerize and serialize a list of user friendly error messages.
    /// </summary>
    public class EnterUpDataException : Exception {
        public List<PropertyError> PropertyErrors { get; set; }
        public bool HasErrors => PropertyErrors != null && PropertyErrors.Count > 0;
        public EnterUpDataException(string message) : base(message) {
            PropertyErrors = new List<PropertyError>();
        }
        public EnterUpDataException(string message, Exception innerException) : base(message, innerException) {
            PropertyErrors = new List<PropertyError>();
        }
#if DEBUG
        public EnterUpDataException() {
            PropertyErrors = new List<PropertyError>();
        }

#endif
    }
}