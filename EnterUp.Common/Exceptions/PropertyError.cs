namespace EnterUp.Common.Exceptions {
    public class PropertyError {
        public string PropertyName { get; set; }
        public string ErrorString { get; set; }
    }
}