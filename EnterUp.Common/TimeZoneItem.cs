﻿
using System.Collections.Generic;

namespace EnterUp.Common {
    public class TimeZoneItem {
        public string Name { get; set; }
        public string ZoneId { get; set; }

        private static List<TimeZoneItem> applicationItems;

        public static List<TimeZoneItem> GetApplicationItems() {
            if (applicationItems == null) {
                applicationItems = new List<TimeZoneItem> {
                    new TimeZoneItem {
                        Name = "Alaska",
                        ZoneId = "Alaskan Standard Time"
                    },
                    new TimeZoneItem {
                        Name = "Pacific Time",
                        ZoneId = "Pacific Standard Time"
                    },
                    new TimeZoneItem {
                        Name = "Mountain Time - Arizona",
                        ZoneId = "US Mountain Standard Time"
                    },
                    new TimeZoneItem {
                        Name = "Mountain Time",
                        ZoneId = "Mountain Standard Time"
                    },
                    new TimeZoneItem {
                        Name = "Central Time",
                        ZoneId = "Central Standard Time"
                    },
                    new TimeZoneItem {
                        Name = "Eastern Time",
                        ZoneId = "Eastern Standard Time"
                    }
                };
            }
            return applicationItems;
        }
    }
}