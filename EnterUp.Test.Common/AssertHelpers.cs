﻿using System;
using Xunit;

namespace EnterUp.Test.Common {
    public static class AssertHelpers {
        public static void IsDateCurrent(DateTime inputDate) {
            DateTime minDateTime = DateTime.Now.AddMilliseconds(-10);
            DateTime maxDateTime = DateTime.Now.AddMilliseconds(10);
            Assert.True(minDateTime <= inputDate && maxDateTime >= inputDate);
        }
    }
}
