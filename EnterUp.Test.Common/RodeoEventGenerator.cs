﻿using System.Collections.Generic;
using EnterUp.Data;

namespace EnterUp.Test.Common {
    public static class RodeoEventGenerator {
        public static List<RodeoEvent> Generate(int rodeoId, List<AssociationEvent> events) {
            using (RodeoContext db = new RodeoContext()) {
                foreach (AssociationEvent associationEvent in events) {
                    if (db.RodeoEvents.Any(re => re.RodeoId == rodeoId &&
                                                 re.AssociationEventId == associationEvent.Id)) {
                        continue;
                    }
                    db.RodeoEvents.Add(new RodeoEvent {
                        AllowSwitchEnds = associationEvent.IsTeamEvent,
                        AssociationEventId = associationEvent.Id,
                        DailyCount = associationEvent.DefaultDailyCount,
                        EventCountsForStandings = true,
                        Fee = associationEvent.DefaultFee,
                        MaxEntries = associationEvent.IsTeamEvent ? 2 : 1,
                        RodeoId = rodeoId,
                        SlackCount = associationEvent.UseSlack ? associationEvent.DefaultSlackCount : -1
                    });
                }
                db.SaveChanges("Test");
                return db.RodeoEvents
                    .Where(re => re.RodeoId == rodeoId)
                    .ToList();
            }
        }
    }
}
