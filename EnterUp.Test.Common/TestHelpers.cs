﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EnterUp.Common.Exceptions;
using EnterUp.Data;
using EnterUp.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EnterUp.Test.Common {
    public static class TestHelpers {
        private static string _solutionBasePath;

        public static string SolutionBasePath {
            get {
                if (!string.IsNullOrEmpty(_solutionBasePath)) return _solutionBasePath;
                _solutionBasePath = AppContext.BaseDirectory;
                while (Directory.GetFiles(_solutionBasePath)
                           .FirstOrDefault(f => f.EndsWith("EnterUp4.sln",
                               StringComparison.CurrentCultureIgnoreCase)) == null) {
                    _solutionBasePath = new DirectoryInfo(_solutionBasePath).Parent.FullName;
                }
                return _solutionBasePath;
            }
        }

        public static void ErrorContains(List<PropertyError> propertyErrors,
            string propertyName,
            string errorText) {
            List<PropertyError> errors =
                propertyErrors.FindAll(p => p.PropertyName == propertyName && p.ErrorString == errorText);
            Assert.Single(errors);
            //$"There is no property error for {propertyName} with message \"{errorText}\".");
        }

        public static void ErrorContains(IValidator validator,
            string propertyName,
            string errorText) {
            using (RodeoContext db = new RodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                EnterUpDataException ex =
                    Assert.Throws<EnterUpDataException>(() => validator.Validate(helper));
                List<PropertyError> errors =
                    ex.PropertyErrors.FindAll(p => p.PropertyName == propertyName && p.ErrorString == errorText);
                Assert.Single(errors);
            }
            //$"There is no property error for {propertyName} with message \"{errorText}\".");
        }

        public static void ErrorContains(IValidator validator,
            IValidationHelper helper,
            string propertyName,
            string errorText) {
            using (RodeoContext db = new RodeoContext()) {
                EnterUpDataException ex =
                    Assert.Throws<EnterUpDataException>(() => validator.Validate(helper));
                List<PropertyError> errors =
                    ex.PropertyErrors.FindAll(p => p.PropertyName == propertyName && p.ErrorString == errorText);
                Assert.Single(errors);
            }
            //$"There is no property error for {propertyName} with message \"{errorText}\".");
        }

        public static void NoError(IValidator validator, IValidationHelper helper) {
            using (RodeoContext db = new RodeoContext()) {
                validator.Validate(helper);
            }
        }

        public static void NoError(IValidator validator) {
            using (RodeoContext db = new RodeoContext()) {
                ValidationHelper helper = new ValidationHelper(db);
                validator.Validate(helper);
            }
            //$"There is no property error for {propertyName} with message \"{errorText}\".");
        }

        public static void NoAnnotationError(IValidator validator) {
            ValidationHelper helper = new ValidationHelper(null);
            List<PropertyError> errors = helper.GetAnnotationErrors(validator);
            Assert.Empty(errors);
        }

        public static void ErrorNotContains(List<PropertyError> propertyErrors,
            string propertyName,
            string errorText) {
            PropertyError error =
                propertyErrors.Find(p => p.PropertyName == propertyName && p.ErrorString == errorText);
            Assert.True(error == null,
                $"There is a property error for {propertyName} with message \"{errorText}\".");
        }

        public static void ErrorNotContainsPropertyName(List<PropertyError> propertyErrors, string propertyName) {
            PropertyError error = propertyErrors.Find(p => p.PropertyName == propertyName);
            Assert.True(error == null, $"There is a property error for {propertyName}.");
        }

        public static string GetStringOfLength(int length) {
            string returnString = string.Empty;
            for (int i = 0; i < length; i++) {
                returnString += "A";
            }
            return returnString;
        }

        /// <summary>
        /// This method will clear all data from the database upto the supplied table name.
        /// </summary>
        /// <param name="rodeoContext">The current context being used.</param>
        /// <param name="tableName">The last table to be cleared out.</param>
        public static void ClearTableData(RodeoContext rodeoContext, string tableName) {
            foreach (string table in DeleteOrderedTableList) {
                string queryText = $"delete from dbo.{table}; DBCC CHECKIDENT ('{table}', RESEED, 0);";
                rodeoContext.Database.ExecuteSqlCommand(queryText);
                if (!string.IsNullOrEmpty(tableName) &&
                    tableName.Equals(table, StringComparison.CurrentCultureIgnoreCase)) {
                    break;
                }
            }
        }

        public static List<string> DeleteOrderedTableList =>
            new List<string> {
                "Suggestions", "Errors", "Requests",
                "AssociationEvents", "Associations",
                "UserClaims"
            };

        public static void ValidationErrorsContains(IValidator test, string propertyName, string errorText) {
            ValidationHelper helper = new ValidationHelper(null);
            List<PropertyError> errors = helper.GetAnnotationErrors(test)
                .Where(e => e.PropertyName == propertyName && e.ErrorString == errorText)
                .ToList();
            Assert.Single(errors);
        }
    }
}
