﻿using System.Collections.Generic;
using System.Linq;
using EnterUp.Data;

namespace EnterUp.Test.Common {
    public static class ContestantGenerator {
        public static List<Contestant> Generate(RodeoContext context, int associationId) {
            List<Contestant> contestants = GetList(associationId);
            foreach (Contestant contestant in contestants) {
                if (context.Contestants
                    .Any(c => c.AssociationId == associationId && c.CardNumber == contestant.CardNumber)) {
                    continue;
                }
                context.Contestants.Add(contestant);
            }
            context.SaveChanges("1");
            return context.Contestants.ToList();
        }

        public static List<Contestant> GetList(int associationId) {
            List<Demographic> demographics = DemographicGenerator.GetList();
            List<Contestant> contestants = new List<Contestant>();
            for (int i = 1; i < 101; i++) {
                contestants.Add(new Contestant {
                    AssociationId = associationId,
                    DemographicId = i + 3,
                    IsMember = i % 5 != 0,
                    IsPermit = i % 5 == 0,
                    IsUserCreated = false,
                    CardNumber = i % 5 != 0
                    ? $"C{i:000}"
                    : $"P{i:000}",
                    Email = $"{demographics[i -1].FirstName}.{demographics[i - 1].LastName}@notReal.com"
                    
                });
            }

            return contestants;
        }
    }
}