﻿using System.Linq;
using EnterUp.Data;

namespace EnterUp.Test.Common {
    public static class AssociationGenerator {
        public static void Generate(RodeoContext dbContext, int? count = 1) {
            for (int i = 1; i < count + 1; i++) {
                string index = $"{i:00}";
                string associationName = $"Association {index}";
                if (dbContext.Associations.Any(a => a.Name == associationName)) {
                    continue;
                }
                dbContext.Associations.Add(CreateAssociation(i));
            }
            dbContext.SaveChanges("1");
        }

        public static Association CreateAssociation(int idIndex) {
            string index = $"{idIndex:00}";
            string associationName = $"Association {index}";
            return new Association {
                Name = associationName,
                Abbreviation = $"TA_{index}",
                IsActive = true,
                ContactEmail = "michael@advsoftware.biz",
                ContactName = "Michael McDaniel",
                ContactPhone = "2082841007",
                ContactOrganization = "EnterUp Test",
                EntryPhone = "180099998765",
                IsFirstPreferenceSlackAllowed = true,
                IsPartnerAllowedOutsideBuddyGroup = true,
                MaximumBuddies = 4,
                MemberDrawPriority = 1,
                MemberSchedulingPriority = 1,
                NonMemberDrawPriority = 4,
                NonMemberSchedulingPriority = 4,
                PermitSchedulingPriority = 2,
                PermitDrawPriority = 2
            };
        }
    }
}