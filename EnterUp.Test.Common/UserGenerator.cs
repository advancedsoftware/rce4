﻿using System.Collections.Generic;
using System.Linq;
using EnterUp.Data;

namespace EnterUp.Test.Common
{
    public static class UserGenerator {
        public static List<EnterUpUser> Generate(RodeoContext context) {
            List<EnterUpUser> users = GetList();
            foreach (EnterUpUser user in users) {
                if (context.Users.Any(u => u.Email == user.Email)) {
                    continue;
                }
                context.Users.Add(user);
            }
            context.SaveChanges("1");
            return context.Users.ToList();
        }

        public static List<EnterUpUser> GetList() {
            List<EnterUpUser> users = new List<EnterUpUser> {
                new EnterUpUser {
                    Email = "michael@advsoftware.biz",
                },
                new EnterUpUser {
                    Email = "bobbyjeancolyer@yahoo.com"
                },
                new EnterUpUser {
                    Email = "cowqueen19987@gmail.com"
                }};

            return users;
        }
    }
}
