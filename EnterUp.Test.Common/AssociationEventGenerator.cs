using System.Collections.Generic;
using System.Linq;
using EnterUp.Common.Enums;
using EnterUp.Data;

namespace EnterUp.Test.Common {
    public static class AssociationEventGenerator {
        public static void Generate(RodeoContext dbContext, int associationId) {
            List<AssociationEvent> events = GetList(associationId);
            foreach (AssociationEvent associationEvent in events) {
                if (dbContext.AssociationEvents
                        .Any(ae => ae.Name == associationEvent.Name && ae.AssociationId == associationId)) {
                    continue;
                }
                dbContext.AssociationEvents.Add(associationEvent);
            }
            dbContext.SaveChanges("1");
        }


        /// <summary>
        /// Returns a list of association events that have not been added to the database.
        /// </summary>
        /// <param name="associationId">The id of the association to which these events are associated.</param>
        /// <returns></returns>
        public static List<AssociationEvent> GetList(int associationId) {
            List<AssociationEvent> events = new List<AssociationEvent>();
            for (int i = 0; i < 5; i++) {
                string eventName = $"Event {i:00}";
                events.Add(new AssociationEvent {
                    AssociationId = associationId,
                    ContestantLabel = i > 2 ? "Contestant Label" : null,
                    TeammateLabel = i > 2 ? "Teammate Label" : null,
                    IsLowFirstOut = i == 0,
                    IsTeamEvent = i > 2,
                    MaximumAge = 0,
                    MinimumAge = 0,
                    Name = eventName,
                    OrderIndex = 5 - i,
                    ScoreSheet = i == 0
                        ? ScoreSheetType.BarrelRacingTimerSheet
                        : i == 1
                            ? ScoreSheetType.JudgeSheet
                            : i == 2
                                ? ScoreSheetType.TimerSheet
                                : ScoreSheetType.TeamRopingTimerSheet,
                    IsSlack = i != 1
                });
            }
            return events;
        }
    }
}