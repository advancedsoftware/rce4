﻿using System;
using EnterUp.Data;
using Microsoft.EntityFrameworkCore;

namespace EnterUp.Test.Common {
    public class MockDbTestBase {
        public MockDbTestBase() {
            Options = new DbContextOptionsBuilder<RodeoContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
        }

        public DbContextOptions<RodeoContext> Options { get; set; }

        public RodeoContext MockContext() {
            RodeoContext context = new RodeoContext(Options) {
                IsSkipValidation = true
            };
            return context;
        }
    }
}