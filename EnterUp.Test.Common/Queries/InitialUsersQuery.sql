﻿Set Identity_Insert dbo.Users On; 
Insert Into dbo.Users
(Id, Created, CreatedBy, DemographicId, Email, EmailConfirmationId, EmailConfirmed, FailedLogonCount, IsActive, IsLocked, LastFailedLogon, LastPasswordChange, NotificationsEnabled, PasswordHash, Updated, UpdatedBy) 
Values (1, GetDate(), 1, null, 'michael@advsoftware.biz', null, 1, 0, 1, 0, '2001-01-01', null, 1, '', GetDate(), 1)
Set Identity_Insert dbo.Users Off;

Set Identity_Insert dbo.Demographics On; 
Insert Into dbo.Demographics
(Id, Address1, Address2, CellPhone, City, Created, CreatedBy, DateOfBirth, DeletedById, FirstName, HomePhone, IsActive, LastName, State, Updated, UpdatedBy, Zipcode)
Values
(1, '2863 E. Lucca Dr.', '', '2082841007', 'Meridian', GetDate(), 1, null, null, 'Michael', '',  1, 'McDaniel', 'ID', GetDate(), 1, '83642');
Set Identity_Insert dbo.Demographics Off; 

Set Identity_Insert dbo.Users On; 
Insert Into dbo.Users
(Id, Created, CreatedBy, DemographicId, Email, EmailConfirmationId, EmailConfirmed, FailedLogonCount, IsActive, IsLocked, LastFailedLogon, LastPasswordChange, NotificationsEnabled, PasswordHash, Updated, UpdatedBy) 
Values (2, GetDate(), 1, null, '', null, 1, 0, 1, 0, '2001-01-01', null, 1, '', GetDate(), 1)
Set Identity_Insert dbo.Users Off;

Set Identity_Insert dbo.Demographics On; 
Insert Into dbo.Demographics
(Id, Address1, Address2, CellPhone, City, Created, CreatedBy, DateOfBirth, DeletedById, FirstName, HomePhone, IsActive, LastName, State, Updated, UpdatedBy, Zipcode)
Values
(2, '', '', '2082841007', 'Bruneau', GetDate(), 1, null, null, 'Bobby Jean', '',  1, 'Colyer', 'ID', GetDate(), 1, '88666');
Set Identity_Insert dbo.Demographics Off; 