﻿using System.Collections.Generic;
using System.Linq;
using EnterUp.Data;

namespace EnterUp.Test.Common {
    public static class DemographicGenerator {
        public static List<Contestant> Generate(RodeoContext context) {
            List<Demographic> demographics = GetList();
            foreach (Demographic demographic in demographics) {
                if (context.Demographics
                    .Any(d => d.FirstName == demographic.FirstName && d.LastName == demographic.LastName)) {
                    continue;
                }
                context.Demographics.Add(demographic);
            }
            context.SaveChanges("1");
            return context.Contestants.ToList();
        }

        public static List<Demographic> GetList() {
            #region contestant List
            List<string> contestantNames = new List<string> {
                "James Smith",
                "John Johnson",
                "Robert Williams",
                "Michael Jones",
                "William Brown",
                "David Davis",
                "Richard Miller",
                "Joseph Wilson",
                "Thomas Moore",
                "Charles Taylor",
                "Christopher Anderson",
                "Daniel Thomas",
                "Matthew Jackson",
                "Anthony White",
                "Donald Harris",
                "Mark Martin",
                "Paul Thompson",
                "Steven Garcia",
                "Andrew Martinez",
                "Kenneth Robinson",
                "George Clark",
                "Joshua Rodriguez",
                "Kevin Lewis",
                "Brian Lee",
                "Edward Walker",
                "Ronald Hall",
                "Timothy Allen",
                "Jason Young",
                "Jeffrey Hernandez",
                "Ryan King",
                "Gary Wright",
                "Jacob Lopez",
                "Nicholas Hill",
                "Eric Scott",
                "Stephen Green",
                "Jonathan Adams",
                "Larry Baker",
                "Justin Gonzalez",
                "Scott Nelson",
                "Frank Carter",
                "Brandon Mitchell",
                "Raymond Perez",
                "Gregory Roberts",
                "Benjamin Turner",
                "Samuel Phillips",
                "Patrick Campbell",
                "Alexander Parker",
                "Jack Evans",
                "Dennis Edwards",
                "Jerry Collins",
                "Tyler Stewart",
                "Aaron Sanchez",
                "Henry Morris",
                "Douglas Rogers",
                "Jose Reed",
                "Peter Cook",
                "Adam Morgan",
                "Zachary Bell",
                "Nathan Murphy",
                "Walter Bailey",
                "Harold Rivera",
                "Kyle Cooper",
                "Carl Richardson",
                "Arthur Cox",
                "Gerald Howard",
                "Roger Ward",
                "Keith Torres",
                "Jeremy Peterson",
                "Terry Gray",
                "Lawrence Ramirez",
                "Sean James",
                "Christian Watson",
                "Albert Brooks",
                "Joe Kelly",
                "Ethan Sanders",
                "Austin Price",
                "Jesse Bennett",
                "Willie Wood",
                "Billy Barnes",
                "Bryan Ross",
                "Bruce Henderson",
                "Jordan Coleman",
                "Ralph Jenkins",
                "Roy Perry",
                "Noah Powell",
                "Dylan Long",
                "Eugene Patterson",
                "Wayne Hughes",
                "Alan Flores",
                "Juan Washington",
                "Louis Butler",
                "Russell Simmons",
                "Gabriel Foster",
                "Randy Gonzales",
                "Philip Bryant",
                "Harry Alexander",
                "Vincent Russell",
                "Bobby Griffin",
                "Johnny Diaz",
                "Logan Hayes"
            };
            #endregion

            List<Demographic> demographics = new List<Demographic> {
                new Demographic {
                    FirstName = "Michael",
                    LastName = "McDaniel",
                    City = "Meridian",
                    Address1 = "2863 E. Lucca Dr.",
                    HomePhone = "2088469635",
                    State = "ID",
                    Zipcode = "83642-3311"
                },
                new Demographic {
                    FirstName = "BobbyJean",
                    LastName = "Colyer",
                    City = "Bruneau",
                    Address1 = "",
                    HomePhone = "ID",
                    Zipcode = "83642-3311"
                },
                new Demographic {
                    FirstName = "Erin",
                    LastName = "Eldridge",
                    City = "Bruneau",
                    Address1 = "",
                    HomePhone = "",
                    State = "ID",
                    Zipcode = "83642-3311"
                }
            };
            for (int i = 1; i < contestantNames.Count + 1; i++) {
                string[] names = contestantNames[i].Split(" ".ToCharArray());
                Demographic demographic = new Demographic {
                    FirstName = names[0],
                    LastName = names[1],
                    CellPhone = $"20834567{i:00}",
                    City = "Sometown",
                    Address1 = $"{i:0}{i:00} {i} St.",
                    HomePhone = $"20898765{i:00}",
                    State = "MT",
                    Zipcode = "76543-2109"
                };
                demographics.Add(demographic);
            }

            return demographics;
        }
    }
}