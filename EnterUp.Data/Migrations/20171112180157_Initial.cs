﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EnterUp.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Associations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Abbreviation = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    AllowNonMembersToEnterOnline = table.Column<bool>(type: "bit", nullable: false),
                    AllowPermitsToEnterOnline = table.Column<bool>(type: "bit", nullable: false),
                    ContactEmail = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ContactName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ContactOrganization = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ContactPhone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EntryPhone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsAgeVerified = table.Column<bool>(type: "bit", nullable: false),
                    IsContestantUserCreated = table.Column<bool>(type: "bit", nullable: false),
                    IsEntryFormDownLoaded = table.Column<bool>(type: "bit", nullable: false),
                    IsFirstPreferenceSlackAllowed = table.Column<bool>(type: "bit", nullable: false),
                    IsPartnerAllowedOutsideBuddyGroup = table.Column<bool>(type: "bit", nullable: false),
                    IsPaymentVerified = table.Column<bool>(type: "bit", nullable: false),
                    IsPoints = table.Column<bool>(type: "bit", nullable: false),
                    IsStandingEntryShown = table.Column<bool>(type: "bit", nullable: false),
                    MaximumBuddies = table.Column<int>(type: "int", nullable: false),
                    MemberDrawPriority = table.Column<int>(type: "int", nullable: false),
                    MemberSchedulingPriority = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    NonMemberDrawPriority = table.Column<int>(type: "int", nullable: false),
                    NonMemberSchedulingPriority = table.Column<int>(type: "int", nullable: false),
                    PermitDrawPriority = table.Column<int>(type: "int", nullable: false),
                    PermitSchedulingPriority = table.Column<int>(type: "int", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Associations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Demographics",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address1 = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Address2 = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CellPhone = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    HomePhone = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    State = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Zipcode = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Demographics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Errors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ErrorMessage = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    StackTrace = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ValidationErrors = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Errors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Elapsed = table.Column<int>(type: "int", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    RequestData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponseData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Suggestions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Notes = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserSuggestion = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suggestions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssociationEvents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationId = table.Column<int>(type: "int", nullable: false),
                    ContestantLabel = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsAgeSpecific = table.Column<bool>(type: "bit", nullable: false),
                    IsLowFirstOut = table.Column<bool>(type: "bit", nullable: false),
                    IsSlack = table.Column<bool>(type: "bit", nullable: false),
                    IsTeamEvent = table.Column<bool>(type: "bit", nullable: false),
                    MaximumAge = table.Column<int>(type: "int", nullable: false),
                    MinimumAge = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderIndex = table.Column<int>(type: "int", nullable: false),
                    ScoreSheet = table.Column<int>(type: "int", nullable: false),
                    TeammateLabel = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociationEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssociationEvents_Associations_AssociationId",
                        column: x => x.AssociationId,
                        principalTable: "Associations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rodeos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationId = table.Column<int>(type: "int", nullable: false),
                    CallBackEnd = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CallBackStart = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CallInEnd = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CallInStart = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ClosedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ClosedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CopiedToId = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsExternal = table.Column<bool>(type: "bit", nullable: false),
                    LateEntryEnd = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LateEntryStart = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OfficeFee = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    RVHookUpFee = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    ResultsPostedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ResultsPostedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScheduledAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ScheduledById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SchedulingRunAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SchedulingRunById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StallFee = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TimeZoneId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TotalPayout = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rodeos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rodeos_Associations_AssociationId",
                        column: x => x.AssociationId,
                        principalTable: "Associations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DemographicId = table.Column<int>(type: "int", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailConfirmationId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    FailedLogonCount = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsLocked = table.Column<bool>(type: "bit", nullable: false),
                    LastFailedLogon = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastPasswordChange = table.Column<DateTime>(type: "datetime2", nullable: true),
                    NotificationsEnabled = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<bool>(type: "bit", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Demographics_DemographicId",
                        column: x => x.DemographicId,
                        principalTable: "Demographics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Performances",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsFollowingPerformance = table.Column<bool>(type: "bit", nullable: false),
                    IsSlack = table.Column<bool>(type: "bit", nullable: false),
                    PerformanceDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RodeoId = table.Column<int>(type: "int", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Performances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Performances_Rodeos_RodeoId",
                        column: x => x.RodeoId,
                        principalTable: "Rodeos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RodeoAssociations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationId = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    RodeoId = table.Column<int>(type: "int", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodeoAssociations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RodeoAssociations_Associations_AssociationId",
                        column: x => x.AssociationId,
                        principalTable: "Associations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoAssociations_Rodeos_RodeoId",
                        column: x => x.RodeoId,
                        principalTable: "Rodeos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RodeoEvents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowDuplicateTeams = table.Column<bool>(type: "bit", nullable: false),
                    AllowPartnersOutsideBuddyGroup = table.Column<bool>(type: "bit", nullable: false),
                    AllowSwitchEnds = table.Column<bool>(type: "bit", nullable: false),
                    AssociationEventId = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DailyCount = table.Column<int>(type: "int", nullable: false),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EventCountsForStandings = table.Column<bool>(type: "bit", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    Fee = table.Column<double>(type: "float", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    MaximumEntryCount = table.Column<int>(type: "int", nullable: false),
                    RodeoId = table.Column<int>(type: "int", nullable: false),
                    SlackCount = table.Column<int>(type: "int", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodeoEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RodeoEvents_AssociationEvents_AssociationEventId",
                        column: x => x.AssociationEventId,
                        principalTable: "AssociationEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEvents_AssociationEvents_EventId",
                        column: x => x.EventId,
                        principalTable: "AssociationEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEvents_Rodeos_RodeoId",
                        column: x => x.RodeoId,
                        principalTable: "Rodeos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contestants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationId = table.Column<int>(type: "int", nullable: false),
                    CardNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfBirthVerifiedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DemographicId = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsMember = table.Column<bool>(type: "bit", nullable: false),
                    IsPermit = table.Column<bool>(type: "bit", nullable: false),
                    IsUserCreated = table.Column<bool>(type: "bit", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contestants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contestants_Associations_AssociationId",
                        column: x => x.AssociationId,
                        principalTable: "Associations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contestants_Users_DateOfBirthVerifiedBy",
                        column: x => x.DateOfBirthVerifiedBy,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contestants_Demographics_DemographicId",
                        column: x => x.DemographicId,
                        principalTable: "Demographics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contestants_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RodeoEntries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstPreferenceId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsScheduled = table.Column<bool>(type: "bit", nullable: false),
                    RodeoId = table.Column<int>(type: "int", nullable: false),
                    RodeoId1 = table.Column<int>(type: "int", nullable: true),
                    SchedulingNumber = table.Column<int>(type: "int", nullable: true),
                    SecondPreferenceId = table.Column<int>(type: "int", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodeoEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RodeoEntries_Performances_FirstPreferenceId",
                        column: x => x.FirstPreferenceId,
                        principalTable: "Performances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntries_Rodeos_RodeoId",
                        column: x => x.RodeoId,
                        principalTable: "Rodeos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RodeoEntries_Rodeos_RodeoId1",
                        column: x => x.RodeoId1,
                        principalTable: "Rodeos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntries_Performances_SecondPreferenceId",
                        column: x => x.SecondPreferenceId,
                        principalTable: "Performances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RodeoEntryMembers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContestantId = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EntryId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    NonMemberFeeWaivedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    NonMemberFeeWaivedById = table.Column<int>(type: "int", nullable: true),
                    PaymentVerifiedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PaymentVerifiedById = table.Column<int>(type: "int", nullable: true),
                    RVCount = table.Column<int>(type: "int", nullable: false),
                    RodeoEntryId = table.Column<int>(type: "int", nullable: false),
                    StallCount = table.Column<int>(type: "int", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WeeklyRodeoCount = table.Column<int>(type: "int", nullable: true),
                    WeeklyScheduleNumber = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodeoEntryMembers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RodeoEntryMembers_Users_ContestantId",
                        column: x => x.ContestantId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntryMembers_RodeoEntries_EntryId",
                        column: x => x.EntryId,
                        principalTable: "RodeoEntries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntryMembers_Users_NonMemberFeeWaivedById",
                        column: x => x.NonMemberFeeWaivedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntryMembers_Users_PaymentVerifiedById",
                        column: x => x.PaymentVerifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEntryMembers_RodeoEntries_RodeoEntryId",
                        column: x => x.RodeoEntryId,
                        principalTable: "RodeoEntries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RodeoEventEntries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContestantId = table.Column<int>(type: "int", nullable: true),
                    ContestantMemberId = table.Column<int>(type: "int", nullable: false),
                    ContestantResult = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DrawNumber = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsContestantEntered = table.Column<bool>(type: "bit", nullable: false),
                    IsContestantStandingEntry = table.Column<bool>(type: "bit", nullable: false),
                    IsTeammateStandingEntry = table.Column<bool>(type: "bit", nullable: false),
                    RodeoEventId = table.Column<int>(type: "int", nullable: false),
                    TeammateId = table.Column<int>(type: "int", nullable: true),
                    TeammateMemberId = table.Column<int>(type: "int", nullable: true),
                    TeammateResult = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodeoEventEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RodeoEventEntries_RodeoEntryMembers_ContestantId",
                        column: x => x.ContestantId,
                        principalTable: "RodeoEntryMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEventEntries_RodeoEntryMembers_ContestantMemberId",
                        column: x => x.ContestantMemberId,
                        principalTable: "RodeoEntryMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEventEntries_RodeoEvents_RodeoEventId",
                        column: x => x.RodeoEventId,
                        principalTable: "RodeoEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEventEntries_RodeoEntryMembers_TeammateId",
                        column: x => x.TeammateId,
                        principalTable: "RodeoEntryMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RodeoEventEntries_RodeoEntryMembers_TeammateMemberId",
                        column: x => x.TeammateMemberId,
                        principalTable: "RodeoEntryMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "UX_AssociationEvent_Name",
                table: "AssociationEvents",
                columns: new[] { "AssociationId", "Name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UX_Association_Abbreviation",
                table: "Associations",
                column: "Abbreviation",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UX_Association_Name",
                table: "Associations",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contestants_DateOfBirthVerifiedBy",
                table: "Contestants",
                column: "DateOfBirthVerifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Contestants_DemographicId",
                table: "Contestants",
                column: "DemographicId");

            migrationBuilder.CreateIndex(
                name: "IX_Contestants_UserId",
                table: "Contestants",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "UX_Contestant_Association_CardNumber",
                table: "Contestants",
                columns: new[] { "AssociationId", "CardNumber" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UX_Performances_RodeoId_PerformanceDate_IsSlack",
                table: "Performances",
                columns: new[] { "RodeoId", "PerformanceDate", "IsSlack" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RodeoAssociations_AssociationId",
                table: "RodeoAssociations",
                column: "AssociationId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoAssociations_RodeoId",
                table: "RodeoAssociations",
                column: "RodeoId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntries_FirstPreferenceId",
                table: "RodeoEntries",
                column: "FirstPreferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntries_RodeoId",
                table: "RodeoEntries",
                column: "RodeoId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntries_RodeoId1",
                table: "RodeoEntries",
                column: "RodeoId1");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntries_SecondPreferenceId",
                table: "RodeoEntries",
                column: "SecondPreferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntryMembers_ContestantId",
                table: "RodeoEntryMembers",
                column: "ContestantId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntryMembers_EntryId",
                table: "RodeoEntryMembers",
                column: "EntryId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntryMembers_NonMemberFeeWaivedById",
                table: "RodeoEntryMembers",
                column: "NonMemberFeeWaivedById");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntryMembers_PaymentVerifiedById",
                table: "RodeoEntryMembers",
                column: "PaymentVerifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEntryMembers_RodeoEntryId",
                table: "RodeoEntryMembers",
                column: "RodeoEntryId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEventEntries_ContestantId",
                table: "RodeoEventEntries",
                column: "ContestantId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEventEntries_ContestantMemberId",
                table: "RodeoEventEntries",
                column: "ContestantMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEventEntries_RodeoEventId",
                table: "RodeoEventEntries",
                column: "RodeoEventId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEventEntries_TeammateId",
                table: "RodeoEventEntries",
                column: "TeammateId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEventEntries_TeammateMemberId",
                table: "RodeoEventEntries",
                column: "TeammateMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEvents_AssociationEventId",
                table: "RodeoEvents",
                column: "AssociationEventId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEvents_EventId",
                table: "RodeoEvents",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_RodeoEvents_RodeoId",
                table: "RodeoEvents",
                column: "RodeoId");

            migrationBuilder.CreateIndex(
                name: "IX_Rodeos_AssociationId",
                table: "Rodeos",
                column: "AssociationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DemographicId",
                table: "Users",
                column: "DemographicId",
                unique: true,
                filter: "[DemographicId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contestants");

            migrationBuilder.DropTable(
                name: "Errors");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropTable(
                name: "RodeoAssociations");

            migrationBuilder.DropTable(
                name: "RodeoEventEntries");

            migrationBuilder.DropTable(
                name: "Suggestions");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "RodeoEntryMembers");

            migrationBuilder.DropTable(
                name: "RodeoEvents");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "RodeoEntries");

            migrationBuilder.DropTable(
                name: "AssociationEvents");

            migrationBuilder.DropTable(
                name: "Demographics");

            migrationBuilder.DropTable(
                name: "Performances");

            migrationBuilder.DropTable(
                name: "Rodeos");

            migrationBuilder.DropTable(
                name: "Associations");
        }
    }
}
