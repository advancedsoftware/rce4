﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class RodeoEventEntry : IValidator, IPermanent, IUpdatedTracker {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int RodeoEventId { get; set; }
        public int ContestantMemberId { get; set; }
        public int? TeammateMemberId { get; set; }
        public int? DrawNumber { get; set; }
        public bool IsContestantStandingEntry { get; set; }
        public bool IsTeammateStandingEntry { get; set; }
        public bool IsContestantEntered { get; set; }
        public decimal? ContestantResult { get; set; }
        public decimal? TeammateResult { get; set; }
        public string DeletedById { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }

        public RodeoEvent RodeoEvent{ get; set; }
        public RodeoEntryMember Contestant { get; set; }
        public RodeoEntryMember Teammate { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public virtual void Validate(IValidationHelper helper) {
            List<PropertyError> errors = helper.GetAnnotationErrors(this);

            if (helper.IsContestantEntryCountExcessive(RodeoEventId, Id, ContestantMemberId)) {
                errors.Add(new PropertyError {
                    PropertyName = "Contestant Entry Count",
                    ErrorString = "The contestant already has the maximum entries allowed for this event."
                });
            }

            if (helper.IsTeamEvent(RodeoEventId)) {
                if (!TeammateMemberId.HasValue) {
                    {
                        errors.Add(new PropertyError {
                            PropertyName = "Teammate Required",
                            ErrorString = "A teammate is required to enter this event."
                        });
                    }
                }
                else {
                    if (helper.IsTeammateEntryCountExcessive(RodeoEventId, Id, TeammateMemberId.Value)) {
                        errors.Add(new PropertyError {
                            PropertyName = "Teammate Entry Count",
                            ErrorString = "The teammate already has the maximum entries allowed for this event."
                        });
                    }
                    if (helper.IsSwitchEndsRuleViolation(RodeoEventId,
                        Id,
                        ContestantMemberId,
                        TeammateMemberId.Value)) {
                        errors.Add(new PropertyError {
                            PropertyName = "Switch Ends Violation",
                            ErrorString = "The contestant and teammate have another entry where they are in opposite postitions."
                        });
                    }
                    if (helper.IsDuplicationEntryViolation(RodeoEventId,
                        Id,
                        ContestantMemberId,
                        TeammateMemberId.Value)) {
                        errors.Add(new PropertyError {
                            PropertyName = "Duplicate Entry Violation",
                            ErrorString = "The contestant and teammate are already entered in the same event in the same positions."
                        });
                    }
                    if (helper.IsTeamEntryViolationOfSameGroupRule(RodeoEventId,
                        ContestantMemberId,
                        TeammateMemberId.Value)) {
                        errors.Add(new PropertyError {
                            PropertyName = "Same Buddy Group Violation",
                            ErrorString = "The contestant and teammate for this event must be in the same buddy group."
                        });
                    }
                }
            }
            else {
                TeammateMemberId = null;
            }

            if (errors.Count > 0) {
                throw new EnterUpDataException("The rodeo event entry is invalid.") {
                    PropertyErrors = errors
                };
            }
        }
    }
}
