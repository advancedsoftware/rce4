﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class RodeoEntryMember : IValidator, IPermanent, IUpdatedTracker {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int RodeoEntryId { get; set; }
        [Required]
        public int ContestantId { get; set; }
        public int? NonMemberFeeWaivedById { get; set; }
        public DateTime? NonMemberFeeWaivedAt { get; internal set; }
        public int? PaymentVerifiedById { get; set; }
        public DateTime? PaymentVerifiedAt { get; internal set; }
        public int? WeeklyScheduleNumber { get; internal set; }
        public int? WeeklyRodeoCount { get; internal set; }
        public int StallCount { get; set; }
        public int RVCount { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public RodeoEntry Entry { get; set; }
        public EnterUpUser Contestant { get; set; }
        public EnterUpUser NonMemberFeeWaivedBy { get; set; }
        public EnterUpUser PaymentVerifiedBy { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }
        #region Model Properties
        //These properties are only used for modeling displayed to the UI.
        public IList<RodeoEventEntry> ContestantEntries { get; set; }
        public IList<RodeoEventEntry> TeammateEntries { get; set; }
        #endregion

        private List<RodeoEventEntry> entries;
        [NotMapped]
        public IList<RodeoEventEntry> Entries {
            get {
                // ReSharper disable once InvertIf
                if (entries == null) {
                    entries = new List<RodeoEventEntry>();
                    entries.AddRange(ContestantEntries ?? new List<RodeoEventEntry>());
                    entries.AddRange(TeammateEntries ?? new List<RodeoEventEntry>());
                }
                return entries;
            } 
        }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        [NotMapped]
        public bool IsPaymentVerified => PaymentVerifiedById.HasValue;

        [NotMapped]
        public bool IsNonMemberFeeWaived => NonMemberFeeWaivedById.HasValue;

        public void Validate(IValidationHelper helper) {
            StallCount = StallCount < 0 ? 0 : StallCount;
            RVCount = RVCount < 0 ? 0 : RVCount;
        }
    }
}