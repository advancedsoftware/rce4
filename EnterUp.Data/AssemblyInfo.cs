﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("EnterUp.Data.Test")]
[assembly: InternalsVisibleTo("EnterUp.Test.Common")]
[assembly: InternalsVisibleTo("EnterUp.API.Test")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("31ae7038-ab95-4d40-860f-cd8d155a0d5b")]