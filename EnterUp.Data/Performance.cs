﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Data.Interfaces;
using EnterUp.Common.Exceptions;

namespace EnterUp.Data {
    public class Performance : IUpdatedTracker, IValidator, IPermanent {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int RodeoId { get; set; }
        public DateTime PerformanceDate { get; set; }
        public bool IsSlack { get; set; }
        public bool IsFollowingPerformance { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public Rodeo Rodeo { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }
        [NotMapped]
        public List<RodeoEntry> FirstPreferenceEntries { get; set; }
        [NotMapped]
        public List<RodeoEntry> SecondPreferenceEntries { get; set; }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public void Validate(IValidationHelper helper) {
            IsFollowingPerformance = IsSlack && IsFollowingPerformance;
            EnterUpDataException ex = new EnterUpDataException("The performance object is invalid.") {
                PropertyErrors = helper.GetAnnotationErrors(this) ?? new List<PropertyError>() 
            };

            if (!helper.IsPerformanceDateBetweenRodeoDates(RodeoId, PerformanceDate)) {
                ex.PropertyErrors.Add(new PropertyError {
                    PropertyName = "Performance Date",
                    ErrorString = "The performance date must be between the rodeo start and end dates."
                });    
            }
            if (ex.PropertyErrors.Count > 0) {
                throw ex;
            }
        }
    }
}
