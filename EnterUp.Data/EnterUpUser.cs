﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data
{
    public class EnterUpUser : IUpdatedTracker {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? DemographicId { get; set; }
        public string Email { get; set; }
        public string EmailConfirmationId { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool NotificationsEnabled { get; set; }
        public bool PasswordHash { get; set; }
        public DateTime? LastPasswordChange { get; set; }
        public int FailedLogonCount { get; set; }
        public DateTime LastFailedLogon { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsLocked { get; set; }
        public bool IsActive { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public Demographic UserDemographic { get; set; }

        public IList<UserClaim> Claims { get; set; }
    }
}