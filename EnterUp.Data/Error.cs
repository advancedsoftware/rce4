﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EnterUp.Data
{
    public class Error : IValidator, IUpdatedTracker {
        #region Data Properties

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Error messages must not be blank.")]
        [MaxLength(512, ErrorMessage = "The error message has a maximum length of 512 characters.")]
        public string ErrorMessage { get; set; }
        [Required(ErrorMessage = "Stack trace must not be blank.")]
        public string StackTrace { get; set; }
        public string ValidationErrors { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }
        #endregion Data Properties



        public static void LogException(Exception ex, string userId) {
            //ToDo : Add logic to strip out validation text.
            Error error = new Error {
                ErrorMessage = ex.Message,
                StackTrace = ex.StackTrace
            };
            if (ex is EnterUpDataException dataEx) {
                error.ValidationErrors = JsonConvert.SerializeObject(dataEx.PropertyErrors);
            }

            using (RodeoContext dbContext = new RodeoContext()) {
                dbContext.Errors.Add(error);
                dbContext.SaveChanges(userId);
                const string queryText = 
                    "delete from dbo.Errors where Id < (select max(id) from dbo.Errors) - 999 ";
                dbContext.Database.ExecuteSqlCommand(queryText);
            }
        }

        public void Validate(IValidationHelper helper) {
            ValidationErrors = string.IsNullOrEmpty(ValidationErrors)
                ? string.Empty
                : ValidationErrors;
            EnterUpDataException ex = new EnterUpDataException("The request object is invalid.") {
                PropertyErrors = helper.GetAnnotationErrors(this)
            };
            if (ex.PropertyErrors.Count > 0)
            {
                throw ex;
            }

        }

    }
}
