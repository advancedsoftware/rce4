﻿using System.Data.SqlClient;
using System.Text.RegularExpressions;
using EnterUp.Common.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace EnterUp.Data.Exceptions {
    public static class ExceptionWrapper {
        public static Regex UniqueConstraintTableNameRegex => 
            new Regex(@"(?<=\'UX_).*?(?=_+)", RegexOptions.Compiled);
        public static Regex UniqueConstraintPropertyNameRegex => 
            new Regex(@"(?<=\'UX_.*?_).*(?=\')", RegexOptions.Compiled);
        public static Regex UniqueConstraintPropertyValueRegex2 =>
            new Regex(@"(?<=duplicate key value is \(.*,\s).*(?=\)\.)", RegexOptions.Compiled);
        public static Regex UniqueConstraintPropertyValueRegex1 =>
            new Regex(@"(?<=duplicate key value is \().*(?=\)\.)", RegexOptions.Compiled);
        public static Regex CamelCaseSplitRegex => new Regex(
            @"(?<=[A-Z])(?=[A-Z][a-z])|(?<=[^A-Z])(?=[A-Z])|(?<=[A-Za-z])(?=[^A-Za-z])",
            RegexOptions.IgnorePatternWhitespace);

        /// <summary>
        /// This method wraps the Sql Exception and adds user friendly
        /// </summary>
        /// <param name="ex">The exception being converted.</param>
        /// <returns>An exception wrapped into an EnterUpException.</returns>
        public static EnterUpDataException EnterUpDataExceptionFromSql(DbUpdateException ex) {
            SqlException sqlException = ex.InnerException as SqlException;
            if (sqlException == null) {
                throw ex;
            }

            string className, propertyName, propertyValue, exMessage;
            EnterUpDataException returnException = null;
            switch (sqlException.Number) {
                case 2601:
                    className = UniqueConstraintTableNameRegex.Match(sqlException.Message).Value;
                    propertyName = UniqueConstraintPropertyNameRegex.Match(sqlException.Message).Value;
                    propertyValue = UniqueConstraintPropertyValueRegex2.IsMatch(sqlException.Message)
                        ? UniqueConstraintPropertyValueRegex2.Match(sqlException.Message).Value
                        : UniqueConstraintPropertyValueRegex1.Match(sqlException.Message).Value;
                    propertyName = propertyName.Replace("_", "");
                    propertyValue = propertyValue.Replace("_", ",");
                    className = ValidationHelper.SplitCamelCase(className);
                    propertyName = ValidationHelper.SplitCamelCase(propertyName);
                    exMessage = $"Saving the {className} failed.";
                    returnException = new EnterUpDataException(exMessage, ex);
                    returnException.PropertyErrors.Add(new PropertyError {
                        PropertyName = propertyName,
                        ErrorString = $"The value ({propertyValue}) for {propertyName} is already in use, the value for {propertyName} must be unique."
                    });
                    break;
                case 547:
                    className = UniqueConstraintTableNameRegex.Match(sqlException.Message).Value;
                    exMessage = $"Saving the {className} failed due to a foreign key violation.";
                    returnException = new EnterUpDataException(exMessage, ex);
                    break;
            }
            return returnException;
        }
    }
}
