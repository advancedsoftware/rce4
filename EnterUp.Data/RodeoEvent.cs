﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class RodeoEvent : IUpdatedTracker, IValidator, IPermanent {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int RodeoId { get; set; }
        public int AssociationEventId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The daily count must be greater than 0.")]
        public int DailyCount { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "The value for the slack count may not be negative.")]
        public int SlackCount { get; set; }
        [Range(0, double.MaxValue, ErrorMessage = "The value for fee may not be negative.")]
        public double Fee { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The value for the maximum entry count must be greater than zero.")]
        public int MaximumEntryCount { get; set; }
        public bool AllowSwitchEnds { get; set; }
        public bool AllowDuplicateTeams { get; set; }
        public bool AllowPartnersOutsideBuddyGroup { get; set; }
        public bool EventCountsForStandings { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }
        public Rodeo Rodeo { get; set; }
        public AssociationEvent AssociationEvent { get; set; }
        public IList<RodeoEventEntry> Entries { get; set; }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public AssociationEvent Event { get; set; }

        public void Validate(IValidationHelper helper) {
            List<PropertyError> errors = helper.GetAnnotationErrors(this);
            if (!helper.IsSlackForAssociationEvent(AssociationEventId)) {
                SlackCount = 0;
            }

            if (!helper.IsRodeoAndAssociationEventInSameAssociation(RodeoId, AssociationEventId)) {
                errors.Add(new PropertyError {
                    PropertyName = "Invalid Association",
                    ErrorString = "The rodeo and the association event are in different associations."
                });
            }

            if (errors.Count > 0) {
                throw new EnterUpDataException("The rodeo event is invalid.") {
                    PropertyErrors = errors
                };
            }
        }
    }
}
