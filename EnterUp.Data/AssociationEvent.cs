﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Enums;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class AssociationEvent : IPermanent, IUpdatedTracker, IValidator {
        public AssociationEvent() {
            OrderIndex = 1;
            Name = string.Empty;
            ScoreSheet = ScoreSheetType.None;
            ContestantLabel = string.Empty;
            TeammateLabel = string.Empty;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AssociationId { get; set; }
        [Range(1, 99, ErrorMessage = "The value for OrderIndex must be between 1 and 99.")]
        public int OrderIndex { get; set; }

        private const string NameErrorMessage = "The Name field is require and must be between 5 and 100 characters.";
        [Required(ErrorMessage = NameErrorMessage)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = NameErrorMessage)]
        public string Name { get; set; }
        public ScoreSheetType ScoreSheet { get; set; }

        private const string ContestantLabelErrorMessage = "The Contestant Label must be between 5 and 100 characters.";
        [StringLength(100, MinimumLength = 5, ErrorMessage = ContestantLabelErrorMessage)]
        public string ContestantLabel { get; set; }

        private const string TeammateLabelErrorMessage = "The Teammate Label must be between 5 and 100 characters.";
        [StringLength(100, MinimumLength = 5, ErrorMessage = TeammateLabelErrorMessage)]
        public string TeammateLabel { get; set; }
        public string DeletedById { get; set; }
        public int MinimumAge { get; set; }
        public int MaximumAge { get; set; }
        public bool IsLowFirstOut { get; set; }
        public bool IsTeamEvent { get; set; }
        public bool IsSlack { get; set; }
        public bool IsAgeSpecific { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        
        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public List<RodeoEvent> RodeoEvents { get; set; }

        public virtual void Validate(IValidationHelper helper) {
            ContestantLabel = string.IsNullOrWhiteSpace(ContestantLabel) ? null : ContestantLabel.Trim();
            TeammateLabel = string.IsNullOrWhiteSpace(TeammateLabel) ? null : TeammateLabel.Trim();
            List<PropertyError> propertyErrors = helper.GetAnnotationErrors(this);
            if (IsTeamEvent) {
                if (string.IsNullOrEmpty(ContestantLabel)) {
                    propertyErrors.Add(new PropertyError {
                        PropertyName = "Contestant Label",
                        ErrorString = "The contestant label is required for team events."
                    });
                }
                if (string.IsNullOrEmpty(TeammateLabel)) {
                    propertyErrors.Add(new PropertyError {
                        PropertyName = "Teammate Label",
                        ErrorString = "The teammate label is required for team events."
                    });
                }
            }

            if (IsAgeSpecific) {
                if (MinimumAge < 1 || MaximumAge < 1 || MinimumAge > MaximumAge) {
                    propertyErrors.Add(new PropertyError {
                        PropertyName = "Ages",
                        ErrorString = "The minimum and maximum ages must form a valid range for age specific events."
                    });
                }
            }
            if (propertyErrors == null || propertyErrors.Count <= 0) return;
            throw new EnterUpDataException("The association event is invalid.") {PropertyErrors = propertyErrors};
        }
    }
}