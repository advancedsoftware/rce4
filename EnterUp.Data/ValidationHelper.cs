﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EnterUp.Data {
    public class ValidationHelper : IValidationHelper {
        public static Regex CamelCaseSplitRegex => new Regex(
            @"(?<=[A-Z])(?=[A-Z][a-z])|(?<=[^A-Z])(?=[A-Z])|(?<=[A-Za-z])(?=[^A-Za-z])",
            RegexOptions.IgnorePatternWhitespace);
        public RodeoContext Context { get; }

        public ValidationHelper(RodeoContext dbContext) {
            Context = dbContext;
        }

        public virtual List<PropertyError> GetAnnotationErrors(object entity) {
            ValidationContext validationContext = new ValidationContext(entity, null, null);
            List<ValidationResult> validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(entity, validationContext, validationResults, true);

            List<PropertyError> propertyErrors = validationResults.Select(result => new PropertyError {
                    PropertyName = SplitCamelCase(result.MemberNames.First()),
                    ErrorString = result.ErrorMessage.Trim()
                })
                .ToList();
            return propertyErrors.OrderBy(p => p.PropertyName).ToList();
        }

        public virtual bool IsCardNumberDigitsUnique(int associationId, int userId, string cardNumber) {
            cardNumber = Regex.Replace(cardNumber, @"\D", string.Empty);
            return !Context.Contestants
                .Where(u => u.AssociationId == associationId)
                .Where(u => u.Id != userId)
                .Any(u => u.CardNumber.Contains(cardNumber));

        }

        public virtual bool DateOfBirthHasValue(int demographicId) {
            return Context.Demographics.Any(d => d.Id == demographicId && d.DateOfBirth.HasValue);
        }

        public virtual bool HasPropertyChanged(object entity, string propertyName) {
            EntityEntry entityEntry = Context.Entry(entity);
            return entityEntry.OriginalValues[propertyName] != entityEntry.CurrentValues[propertyName];
        }

        public virtual bool IsUserNameValidOrNull(int? userId) {
            return !userId.HasValue;
        }

        public bool IsPerformanceDateBetweenRodeoDates(int rodeoId, DateTime performanceDate) {
            return Context.Rodeos
                .Any(r =>
                    r.Id == rodeoId
                    && r.EndDate >= performanceDate
                    && r.StartDate <= performanceDate);
        }

        public bool IsRodeoNameUniqueInSeason(int rodeoId, string rodeoName, int season) {
            DateTime firstOfYear = new DateTime(season, 1, 1);
            DateTime endOfYear = firstOfYear.AddYears(1).AddMilliseconds(-3);
            return !Context.Rodeos.Any(r =>
                string.Equals(r.Name, rodeoName, StringComparison.CurrentCultureIgnoreCase)
                && r.StartDate >= firstOfYear
                && r.EndDate <= endOfYear
                && r.Id != rodeoId
                && (r.DeletedById == null || r.DeletedById == string.Empty)
            );
        }

        public bool IsNullableDateRangeValid(DateTime? start, DateTime? end) {
            if (start.HasValue != end.HasValue) {
                return false;
            }
            if (start.HasValue) {
                return start.Value < end.Value;
            }
            return true;
        }

        public bool IsSlackForAssociationEvent(int associationEventId) {
            return Context.AssociationEvents.Any(ae => ae.Id == associationEventId && ae.IsSlack);
        }

        public bool IsRodeoAndAssociationEventInSameAssociation(int rodeoId, int associationEventId) {
            AssociationEvent associationEvent =
                Context.AssociationEvents.FirstOrDefault(ae => ae.Id == associationEventId);

            return
                associationEvent != null
                && Context.Rodeos.Any(r => r.Id == rodeoId && r.AssociationId == associationEvent.Id);
        }

        public bool IsPerformanceInRodeo(int rodeoId, int performanceId) {
            return Context.Performances.Any(p => p.RodeoId == rodeoId && p.Id == performanceId);
        }

        public bool IsTeamEvent(int rodeoEventId) {
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);
            return rodeoEvent != null
                   && Context.AssociationEvents.Any(ae => ae.Id == rodeoEvent.AssociationEventId && ae.IsTeamEvent);
        }

        public bool IsContestantEntryCountExcessive(int rodeoEventId, int entryId, int memberId) {
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);

            if (rodeoEvent == null) {
                return true;
            }

            int entryCount = GetEntryCountByRodeoEntryMemberId(rodeoEventId, entryId, memberId);
            return rodeoEvent.MaximumEntryCount <= entryCount;
        }

        public bool IsTeammateEntryCountExcessive(int rodeoEventId, int entryId, int memberId) {
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);

            if (rodeoEvent == null) {
                return true;
            }
            int entryCount = GetEntryCountByRodeoEntryMemberId(rodeoEventId, entryId, memberId);
            return rodeoEvent.MaximumEntryCount <= entryCount;
        }

        public bool IsSwitchEndsRuleViolation(int rodeoEventId, int entryId, int contestantMemberId, int teammateMemberId) {
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);

            if (rodeoEvent == null) {
                return true;
            }
            if (rodeoEvent.AllowSwitchEnds) {
                return false;
            }
            return Context.RodeoEventEntries.Any(ree => ree.Id != entryId
                                                        && ree.RodeoEventId == rodeoEventId
                                                        && ree.ContestantMemberId == teammateMemberId 
                                                        && ree.TeammateMemberId == contestantMemberId);
        }

        public bool IsDuplicationEntryViolation(int rodeoEventId, int entryId, int contestantMemberId, int teammateMemberId) {
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);

            if (rodeoEvent == null) {
                return true;
            }
            if (rodeoEvent.AllowDuplicateTeams) {
                return false;
            }
            return Context.RodeoEventEntries.Any(ree => ree.Id != entryId
                                                        && ree.RodeoEventId == rodeoEventId
                                                        && ree.ContestantMemberId == contestantMemberId
                                                        && ree.TeammateMemberId == teammateMemberId);
        }

        public bool IsTeamEntryViolationOfSameGroupRule(int rodeoEventId, int contestantMemberId, int teammateMemberId) {
            //The Same Group rule: some team events require that teammate be in the same rodeo entry.
            RodeoEvent rodeoEvent = Context.RodeoEvents.FirstOrDefault(re => re.Id == rodeoEventId);
            if (rodeoEvent == null) {
                return true;
            }
            if (rodeoEvent.AllowPartnersOutsideBuddyGroup) {
                return false;
            }
            RodeoEntryMember contestantMember =
                Context.RodeoEntryMembers.FirstOrDefault(rem => rem.Id == contestantMemberId);
            if (contestantMember == null) {
                return true;
            }
            RodeoEntryMember teammateMember =
                Context.RodeoEntryMembers.FirstOrDefault(rem => rem.Id == teammateMemberId);
            return contestantMember.RodeoEntryId != teammateMember?.RodeoEntryId;
        }

        public static string SplitCamelCase(string input) {
            return CamelCaseSplitRegex.Replace(input, " ").Trim();
        }

        /// <summary>
        /// This returns the count of entries a rodeo entry member has in a given event.
        /// The supplied entry Id is not counted for resuls.
        /// If you want all entries supply 0 for the supplied entry Id.
        /// </summary>
        /// <param name="rodeoEventId"></param>
        /// <param name="excludedEntryId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public virtual int GetEntryCountByRodeoEntryMemberId(int rodeoEventId, int excludedEntryId, int memberId) {
            return Context.RodeoEventEntries
                .Where(ree => ree.RodeoEventId == rodeoEventId)
                .Where(ree => ree.Id != excludedEntryId)
                .Count(ree => ree.ContestantMemberId == memberId || ree.TeammateMemberId == memberId);
        }
    }
}