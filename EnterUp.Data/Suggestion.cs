﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class Suggestion : IUpdatedTracker, IValidator {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "The user suggestion is required.")]
        [MaxLength(1024, ErrorMessage = "The user suggestion cannot exceed 1024 characters.")]
        [MinLength(5, ErrorMessage = "The user suggestion must be at least 5 characters.")]
        public string UserSuggestion { get; set; }
        [MaxLength(512, ErrorMessage = "The notes cannot exceed 512 characters.")]
        public string Notes { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public void Validate(IValidationHelper helper) {
            Notes = string.IsNullOrEmpty(Notes) ? string.Empty : Notes;
            EnterUpDataException ex = new EnterUpDataException("The suggestion object is invalid.") {
                PropertyErrors = helper.GetAnnotationErrors(this)
            };
            if (ex.PropertyErrors.Count > 0) {
                throw ex;
            }
        }
    }
}