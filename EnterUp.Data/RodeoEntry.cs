﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class RodeoEntry : IUpdatedTracker, IValidator, IPermanent {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int RodeoId { get; set; }
        public int FirstPreferenceId { get; set; }
        public int? SecondPreferenceId { get; set; }
        public int? SchedulingNumber { get; set; }
        public bool IsScheduled { get; set; }

        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }
        public Rodeo Rodeo { get; set; }
        public Performance FirstPreference { get; set; }
        public Performance SecondPreference { get; set; }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        [NotMapped]
        public string ConfirmationNumber => $"{RodeoId}-{Id:000000}";

        public virtual List<RodeoEntryMember> RodeoEntryMembers { get; set; }

        public void Validate(IValidationHelper helper) {
            List<PropertyError> errors = helper.GetAnnotationErrors(this);
            if (!helper.IsPerformanceInRodeo(RodeoId, FirstPreferenceId)) {
                errors.Add(new PropertyError {
                    PropertyName = "First Preference",
                    ErrorString = "The first perference is not a performance in this rodeo."
                });
            }
            if (SecondPreferenceId.HasValue && !helper.IsPerformanceInRodeo(RodeoId, FirstPreferenceId)) {
                errors.Add(new PropertyError {
                    PropertyName = "Second Preference",
                    ErrorString = "The second perference is not a performance in this rodeo."
                });
            }
            if (errors.Count > 0) {
                throw new EnterUpDataException("The rodeo entry is invalid.") {
                    PropertyErrors = errors
                };
            }
        }
    }
}
