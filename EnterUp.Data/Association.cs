﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class Association : IUpdatedTracker, IPermanent, IValidator {
        #region Constructor
        public Association() {
            Name = string.Empty;
            Abbreviation = string.Empty;
            ContactEmail = string.Empty;
            ContactName = string.Empty;
            ContactOrganization = string.Empty;
            ContactPhone = string.Empty;
            EntryPhone = string.Empty;
            IsActive = true;
            ContactEmail = string.Empty;
            MaximumBuddies = 4;
            MemberDrawPriority = 1;
            MemberSchedulingPriority = 1;
            NonMemberDrawPriority = 4;
            NonMemberSchedulingPriority = 4;
            PermitDrawPriority = 2;
            PermitSchedulingPriority = 2;

            // ReSharper disable once VirtualMemberCallInConstructor
            AssociationEvents = new List<AssociationEvent>();
        }
        #endregion Constructor

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "The Name field is require and must be between 5 and 255 characters.")]
        [StringLength(255, MinimumLength = 5,
            ErrorMessage = "The Name field is require and must be between 5 and 255 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The Abbreviation field is require and must be between 2 and 50 characters.")]
        [StringLength(50, MinimumLength = 2,
            ErrorMessage = "The Abbreviation field is require and must be between 2 and 50 characters.")]
        public string Abbreviation { get; set; }

        [Required(ErrorMessage = "The Contact Name field is require and must be between 5 and 255 characters.")]
        [StringLength(255, MinimumLength = 5,
            ErrorMessage = "The Contact Name field is require and must be between 5 and 255 characters.")]
        public string ContactName { get; set; }
        
        [Required(ErrorMessage = "The Contact Organization field is require and must be between 5 and 255 characters.")]
        [StringLength(255, MinimumLength = 5,
            ErrorMessage = "The Contact Organization field is require and must be between 5 and 255 characters.")]
        public string ContactOrganization { get; set; }
        
        [StringLength(20, ErrorMessage = "The Contact Phone field has a maximum length of 20 characters.")]
        public string ContactPhone { get; set; }

        [StringLength(20, ErrorMessage = "The Entry Phone field has a maximum length of 20 characters.")]
        public string EntryPhone { get; set; }

        [StringLength(255, ErrorMessage = "The Contact Email field has a maximum length of 255 characters.")]
        public string ContactEmail { get; set; }

        [Range(1, 10, ErrorMessage = "The value for Maximum Buddies must be between 1 and 10.")]
        public int MaximumBuddies { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Member Scheduling Priority must be between 1 and 9.")]
        public int MemberSchedulingPriority { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Permit Scheduling Priority must be between 1 and 9.")]
        public int PermitSchedulingPriority { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Non-Member Scheduling Priority must be between 1 and 9.")]
        public int NonMemberSchedulingPriority { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Member Draw Priority must be between 1 and 9.")]
        public int MemberDrawPriority { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Permit Draw Priority must be between 1 and 9.")]
        public int PermitDrawPriority { get; set; }

        [Range(1, 9, ErrorMessage = "The value for Non-Member Draw Priority must be between 1 and 9.")]
        public int NonMemberDrawPriority { get; set; }
        public bool IsPartnerAllowedOutsideBuddyGroup { get; set; }
        public bool AllowPermitsToEnterOnline { get; set; }
        public bool AllowNonMembersToEnterOnline { get; set; }
        public bool IsContestantUserCreated { get; set; }
        public bool IsFirstPreferenceSlackAllowed { get; set; }
        public bool IsEntryFormDownLoaded { get; set; }
        public bool IsAgeVerified { get; set; }
        public bool IsPaymentVerified { get; set; }
        public bool IsPoints { get; set; }
        public bool IsStandingEntryShown { get; set; }
        public string DeletedById { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public virtual List<AssociationEvent> AssociationEvents { get; set; }
        public virtual List<Rodeo> Rodeos { get; set; }
        public virtual List<Contestant> Contestants { get; set; }

        public void Validate(IValidationHelper helper) {
            List<PropertyError> propertyErrors = helper.GetAnnotationErrors(this);
            if (propertyErrors == null || propertyErrors.Count <= 0) return;
            EnterUpDataException ex =
                new EnterUpDataException("Saving the Association failed.") {PropertyErrors = propertyErrors};
            throw ex;
        }
    }
}
