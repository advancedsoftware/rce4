﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data
{
    public class Rodeo : IUpdatedTracker, IPermanent, IValidator {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int AssociationId { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "The name must be between 5 and 100 characters.")]
        public string Name { get; set; }

        #region Dates
        public string TimeZoneId { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public DateTime? CallInStart { get; set; }
        public DateTime? CallInEnd { get; set; }
        public DateTime? LateEntryStart { get; set; }
        public DateTime? LateEntryEnd { get; set; }
        public DateTime? CallBackStart { get; set; }
        public DateTime? CallBackEnd { get; set; }
        #endregion

        #region Fees
        public decimal? OfficeFee { get; set; }
        public decimal? StallFee { get; set; }
        public decimal? RVHookUpFee { get; set; }
        public decimal? TotalPayout { get; set; }
        #endregion

        public string Message { get; set; }

        #region State Tracking
        public bool IsExternal { get; set; }
        public string ClosedById { get; set; }
        public DateTime? ClosedAt{ get; set; }
        public string SchedulingRunById { get; set; }
        public DateTime? SchedulingRunAt { get; set; }
        public string ScheduledById { get; set; }
        public DateTime? ScheduledAt { get; set; }
        public string ResultsPostedById { get; set; }
        public DateTime? ResultsPostedAt { get; set; }
        public string DeletedById { get; set; }
        public int? CopiedToId { get; set; }
        #endregion

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
        
        #region Navigation Properties
        public List<Performance> Performances { get; set; }
        public List<RodeoEvent> RodeoEvents { get; set; }
        public List<RodeoEntry> RodeoEntries { get; set; }
        public List<RodeoAssociation> ValidContestantAssociation { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }
        public EnterUpUser ClosedByUser() {
            return Helpers.GetUser(ClosedById);
        }
        public EnterUpUser ScheduledBy() {
            return Helpers.GetUser(ScheduledById);
        }
        public EnterUpUser SchedulingRunBy() {
            return Helpers.GetUser(SchedulingRunById);
        }
        public EnterUpUser ResultsPostedBy() {
            return Helpers.GetUser(ResultsPostedById);
        }
        #endregion

        [NotMapped]
        public bool IsSchedulingRun => !string.IsNullOrEmpty(SchedulingRunById);

        [NotMapped]
        public bool IsScheduled => !string.IsNullOrEmpty(ScheduledById);

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);
        [NotMapped]
        public bool IsClosed => !string.IsNullOrEmpty(ClosedById);

        public void Validate(IValidationHelper helper) {
            Message = string.IsNullOrEmpty(Message) ? string.Empty : Message;
            List<PropertyError> errors = helper.GetAnnotationErrors(this);
            if (!helper.IsRodeoNameUniqueInSeason(Id, Name, StartDate.Year)) {
                errors.Add(new PropertyError{ErrorString = "The rodeo name must be unique within a season.", PropertyName = "Name"});
            }

            if (StartDate >= EndDate) {
                errors.Add(new PropertyError { ErrorString = "The start and end dates don't form a valid range.", PropertyName = "Rodeo Dates" });
            }

            if (EndDate < DateTime.Now) {
                errors.Add(new PropertyError { ErrorString = "A rodeo cannot be edited after the rodeo is complete.", PropertyName = "End Date" });
            }

            ValidateCallInDates(errors, helper);
            ValidateLateEntryDates(errors, helper);
            ValidateClosedValues(errors);
            ValidateScheduledValues(errors);

            if (ResultsPostedAt.HasValue != !string.IsNullOrEmpty(ResultsPostedById)) {
                errors.Add(new PropertyError {
                    ErrorString = "Posting results for a rodeo requires a value for both the results posted by and results posted at properties.",
                    PropertyName = "Results Posted"
                });
            }

            if (Common.TimeZoneItem.GetApplicationItems().Find(t => t.ZoneId == TimeZoneId) == null) {
                errors.Add(new PropertyError { ErrorString = "The time zone identifier is not valid.", PropertyName = "Time Zone" });
            }

            if (errors.Count > 0) {
                throw new EnterUpDataException("The rodeo is invalid and cannot be saved.") {
                    PropertyErrors = errors
                };
            }
        }

        private void ValidateScheduledValues(List<PropertyError> errors) {
            if (!string.IsNullOrEmpty(ScheduledById) != ScheduledAt.HasValue) {
                errors.Add(new PropertyError {
                    ErrorString = "Scheduling a rodeo requires a value for both the scheduled by and scheduled at properties.",
                    PropertyName = "Scheduled"
                });
            }
            else {
                if (!string.IsNullOrEmpty(ScheduledById) && ScheduledAt.HasValue && ScheduledAt.Value > StartDate) {
                    errors.Add(new PropertyError {
                        ErrorString = "The date a rodeo is scheduled must be less than the rodeo start date.",
                        PropertyName = "Scheduled Date"
                    });
                }
            }
        }

        private void ValidateLateEntryDates(List<PropertyError> errors, IValidationHelper validationHelper) {
            if (!validationHelper.IsNullableDateRangeValid(LateEntryStart, LateEntryEnd)) {
                errors.Add(new PropertyError {
                    ErrorString = "The late entry dates don't form a valid range.",
                    PropertyName = "Late Entry Dates"
                });
                return;
            }
            if (!LateEntryStart.HasValue) return;
            if (LateEntryEnd >= StartDate) {
                errors.Add(new PropertyError {
                    ErrorString = "The late entry period must end before the rodeo starts.",
                    PropertyName = "Late Entry Dates"
                });
            }
            if (LateEntryStart.Value < CallInEnd) {
                errors.Add(new PropertyError {
                    ErrorString = "The late entry period must start after the call in period ends.",
                    PropertyName = "Late Entry Dates"
                });
            }
        }

        private void ValidateClosedValues(List<PropertyError> errors) {
            if (!string.IsNullOrEmpty(ClosedById) != ClosedAt.HasValue) {
                errors.Add(new PropertyError {
                    ErrorString = "Closing a rodeo requires a value for both the closed by and closed at properties.",
                    PropertyName = "Closed"
                });
            }
            else {
                if (!string.IsNullOrEmpty(ClosedById) && ClosedAt.HasValue && ClosedAt.Value > EndDate) {
                    errors.Add(new PropertyError {
                        ErrorString = "The date a rodeo is closed must be less than the rodeo end date.",
                        PropertyName = "Closed Date"
                    });
                }
            }
        }

        private void ValidateCallInDates(ICollection<PropertyError> errors, IValidationHelper validationHelper) {
            if (!validationHelper.IsNullableDateRangeValid(CallInStart, CallInEnd)) {
                errors.Add(new PropertyError { ErrorString = "The call in dates don't form a valid range.", PropertyName = "Call In Dates" });
            }
            else {
                if (CallInStart.HasValue && CallInEnd >= StartDate) {
                    errors.Add(new PropertyError { ErrorString = "The call in period must end before the rodeo starts.", PropertyName = "Call In Dates" });
                }
            }
            if (!validationHelper.IsNullableDateRangeValid(CallBackStart, CallBackEnd)) {
                errors.Add(new PropertyError { ErrorString = "The call back dates don't form a valid range.", PropertyName = "Call Back Dates" });
            }
            else {
                if (!CallBackStart.HasValue) return;
                if (CallBackEnd >= StartDate) {
                    errors.Add(new PropertyError {
                        ErrorString = "The call back period must end before the rodeo starts.",
                        PropertyName = "Call Back Dates"
                    });
                }
                if (CallBackStart.Value < CallInEnd) {
                    errors.Add(new PropertyError {
                        ErrorString = "The call back period must start after the call in period ends.",
                        PropertyName = "Call Back Dates"
                    });
                }
            }
        }
    }
}
