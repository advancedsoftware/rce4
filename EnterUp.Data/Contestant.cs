﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class Contestant : IUpdatedTracker, IPermanent, IValidator {
        public Contestant() {
            Email = string.Empty;
            IsActive = true;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int DemographicId { get; set; }
        public int AssociationId { get; set; }
        [Required(ErrorMessage = "The Card Number is required and must be between 2 and 10 characters in length.")]
        [StringLength(10, 
            MinimumLength = 2, 
            ErrorMessage = "The Card Number is required and must be between 2 and 10 characters in length.")]
        public string CardNumber { get; set; }
        [MaxLength(255, ErrorMessage = "The email address must be less than 256 characters.")]
        public string Email { get; set; }
        public int? DateOfBirthVerifiedBy { get; set; }
        internal DateTime? DateOfBirthVerifiedAt { get; set; }
        public bool IsMember { get; set; }
        public bool IsPermit { get; set; }
        public bool IsUserCreated { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        [NotMapped]
        public bool IsDateOfBirthVerified => DateOfBirthVerifiedBy.HasValue;

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public virtual Demographic ContestantDemographic { get; set; }
        public virtual EnterUpUser User { get; set; }
        public virtual EnterUpUser DateOfBirthVerifier { get; set; }
        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        public void Validate(IValidationHelper helper) {
            Email = string.IsNullOrEmpty(Email) ? string.Empty : Email.Trim();
            IsActive = !IsDeleted && IsActive;
            EnterUpDataException ex = new EnterUpDataException("The contestant is invalid.") {
                    PropertyErrors = helper.GetAnnotationErrors(this)
                };

            if (!helper.IsCardNumberDigitsUnique(AssociationId, UserId ?? -1, CardNumber)) {
                ex.PropertyErrors.Add(new PropertyError {
                    PropertyName = "Card Number",
                    ErrorString = "The card number must contain a unique identifier."
                });
            }
            if (!helper.DateOfBirthHasValue(DemographicId)) {
                DateOfBirthVerifiedAt = null;
                DateOfBirthVerifiedBy = null;
            }

            if (!DateOfBirthVerifiedBy.HasValue) {
                DateOfBirthVerifiedAt = null;
            }
            else {
                if (!DateOfBirthVerifiedAt.HasValue) {
                    DateOfBirthVerifiedAt = DateTime.Now;
                }
                else {
                    if (helper.HasPropertyChanged(this, "DateOfBirthVerifiedBy")) {
                        DateOfBirthVerifiedAt = DateTime.Now;
                    }
                }
            }

            if (ex.HasErrors) {
                throw ex;
            }
        }
    }
}