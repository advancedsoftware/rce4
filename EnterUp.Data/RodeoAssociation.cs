﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data
{
    /// <summary>
    /// This class is used to track which association contestants are allowed in each rodeo.
    /// </summary>
    public class RodeoAssociation : IUpdatedTracker, IPermanent {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int RodeoId { get; set; }
        public int AssociationId { get; set; }

        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }
        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }
        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        public Rodeo Rodeo { get; set; }
        public Association Association { get; set; }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);
    }
}
