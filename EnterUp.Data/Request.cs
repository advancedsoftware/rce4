﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class Request : IValidator, IUpdatedTracker {
        #region Data Properties
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string IpAddress { get; set; }
        [Required(ErrorMessage = "Url is required.")]
        public string Url { get; set; }
        public string RequestData { get; set; }
        public string ResponseData { get; set; }
        public int Elapsed { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }
        #endregion Data Properties

        public void Validate(IValidationHelper helper) {
            EnterUpDataException ex = new EnterUpDataException("The request object is invalid.") {
                PropertyErrors = helper.GetAnnotationErrors(this)
            };
            IPAddress ipAddress;
            if (!IPAddress.TryParse(IpAddress, out ipAddress)) {
                ex.PropertyErrors.Add(new PropertyError {
                    PropertyName = "Ip Address",
                    ErrorString = "The IP address provided is invalid."
                });
            }
            if (ex.PropertyErrors.Count > 0) {
                throw ex;
            }

        }
    }
}
