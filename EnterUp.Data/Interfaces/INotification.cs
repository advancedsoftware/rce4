﻿using System;

namespace EnterUp.Data.Interfaces {
    internal interface INotification {
        DateTime? Notified { get; set; }
        bool IsNotified { get; }
    }
}
