﻿using System;
using System.Collections.Generic;
using EnterUp.Common.Exceptions;

namespace EnterUp.Data.Interfaces {
    public interface IValidationHelper {
        RodeoContext Context { get; }

        bool DateOfBirthHasValue(int demographicId);
        List<PropertyError> GetAnnotationErrors(object entity);
        bool HasPropertyChanged(object entity, string propertyName);
        bool IsCardNumberDigitsUnique(int associationId, int userId, string cardNumber);
        bool IsUserNameValidOrNull(int? userId);

        bool IsPerformanceDateBetweenRodeoDates(int rodeoId, DateTime performanceDate);
        bool IsRodeoNameUniqueInSeason(int rodeoId, string rodeoName, int season);
        bool IsNullableDateRangeValid(DateTime? start, DateTime? end);
        bool IsSlackForAssociationEvent(int associationEventId);
        bool IsRodeoAndAssociationEventInSameAssociation(int rodeoId, int associationEventId);
        bool IsPerformanceInRodeo(int rodeoId, int performanceId);
        bool IsTeamEvent(int rodeoEventId);
        bool IsContestantEntryCountExcessive(int rodeoEventId, int entryId, int memberId);
        bool IsTeammateEntryCountExcessive(int rodeoEventId, int entryId, int teammateEntryMemberId);
        bool IsSwitchEndsRuleViolation(int rodeoEventId, int entryId, int contestantEntryMemberId, int teammateEntryMemberId);
        bool IsDuplicationEntryViolation(int rodeoEventId, int entryId, int contestantEntryMemberId, int teammateEntryMemberId);
        bool IsTeamEntryViolationOfSameGroupRule(int rodeoEventId, int contestantMemberId, int teammateMemberId);
        int GetEntryCountByRodeoEntryMemberId(int rodeoEntryId, int excludedEntryId, int memberId);
    }
}