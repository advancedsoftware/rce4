﻿namespace EnterUp.Data.Interfaces {
    public interface IValidator {
        /// <summary>
        /// This method is called on SaveChanges in the data layer.
        /// If the validation fails, it should throw an exception of type EnterUpDataException.
        /// </summary>
        /// <param name="helper">The validation helper supplied by the current data context.</param>
        void Validate(IValidationHelper helper);
    }
}
