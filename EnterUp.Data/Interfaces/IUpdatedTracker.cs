﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EnterUp.Data.Interfaces {
    public interface IUpdatedTracker {
        [Required]
        DateTime Updated { get; set; }
        [Required]
        string UpdatedBy { get; set; }
        [Required]
        DateTime Created { get; set; }
        [Required]
        string CreatedBy { get; set; }
        bool IsActive { get; set; }

        EnterUpUser CreatedByUser();
        EnterUpUser UpdatedByUser();
    }
}
