﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace EnterUp.Data.Interfaces {
    interface IRodeoContext {
        IValidationHelper ValidationHelper { get; set; }

//        #region Data Sets        
//        #region User Data Sets
//        IQueryable<EnterUpUser> Users { get; set; }
//        IQueryable<UserClaim> UserClaims { get; set; }
//        IQueryable<Demographic> Demographics { get; set; }
//        #endregion User Data Sets

//        #region Association Data Sets
//        IQueryable<Association> Associations { get; set; }
//        IQueryable<AssociationEvent> AssociationEvents { get; set; }
//        IQueryable<Contestant> Contestants { get; set; }
//        IQueryable<Rodeo> Rodeos { get; set; }
//        IQueryable<RodeoAssociation> RodeoAssociations { get; set; }
//        IQueryable<Performance> Performances { get; set; }
//        IQueryable<RodeoEvent> RodeoEvents { get; set; }
//        IQueryable<RodeoEntry> RodeoEntries { get; set; }
//        IQueryable<RodeoEntryMember> RodeoEntryMembers { get; set; }
//        IQueryable<RodeoEventEntry> RodeoEventEntries { get; set; }
//        #endregion Association Data Sets

//        #region Admin Data Sets
//        IQueryable<Error> Errors { get; set; }
//        IQueryable<Request> Requests { get; set; }
//        IQueryable<Suggestion> Suggestions { get; set; }
//        #endregion Admin Data Sets
//#endregion Data Sets
    }
}
