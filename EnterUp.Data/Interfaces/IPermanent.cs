﻿namespace EnterUp.Data.Interfaces {
    public interface IPermanent {
        bool IsDeleted { get; }
        string DeletedById { get; set; }
    }
}
