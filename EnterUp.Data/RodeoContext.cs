﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnterUp.Common.Exceptions;
using EnterUp.Data.Exceptions;
using EnterUp.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedMember.Global

namespace EnterUp.Data {
    public class RodeoContext : DbContext, IRodeoContext {
        #region Helper Properties
        public IValidationHelper ValidationHelper { get; set; }
        /// <summary>
        /// This property controls the validation of entities before saving.
        /// In production the value will always be false and validation will always be run.
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        public bool IsSkipValidation { get; internal set; }
        #endregion Helper Properties

        public RodeoContext(DbContextOptions options) : base(options) {
            ValidationHelper = new ValidationHelper(this);
        }

        public RodeoContext(DbContextOptions options, IValidationHelper validationHelper) : base(options) {
            ValidationHelper = validationHelper;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
#if DEBUG
            if (!optionsBuilder.IsConfigured) {
                ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
                configurationBuilder
                    .SetBasePath(@"e:\Code\rce4\EnterUp.Data.Test")
                    .AddJsonFile(@"appsettings.json", optional: false, reloadOnChange: true);
                ConfigurationRoot configuration = (ConfigurationRoot)configurationBuilder.Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("RodeoContext"));
            }
#endif
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            //Name any unique constraints carefully.
            //The name is used to create the exception message.
            //The returned message will be "The {tableName} {columnName} must be unique."    

            #region RodeoEventEntry
            //Contains the FK relationships for the RodeoEventEntries
            modelBuilder.Entity<RodeoEventEntry>()
                .HasOne(ree => ree.RodeoEvent)
                .WithMany(re => re.Entries)
                .HasForeignKey(ree => ree.RodeoEventId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RodeoEventEntry>()
                .HasOne(ree => ree.Contestant)
                .WithMany()
                .HasForeignKey(ree => ree.ContestantMemberId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RodeoEventEntry>()
                .HasOne(ree => ree.Teammate)
                .WithMany()
                .HasForeignKey(ree => ree.TeammateMemberId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion RodeoEventEntry

            #region RodeoEntryMember
            //This region holds the entity specific definition for RodeoEntryMember.
            modelBuilder.Entity<RodeoEntryMember>()
                .HasOne(ra => ra.Contestant)
                .WithMany()
                .HasForeignKey(ra => ra.ContestantId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RodeoEntryMember>()
                .HasOne(ra => ra.NonMemberFeeWaivedBy)
                .WithMany()
                .HasForeignKey(ra => ra.NonMemberFeeWaivedById)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RodeoEntryMember>()
                .HasOne(ra => ra.PaymentVerifiedBy)
                .WithMany()
                .HasForeignKey(ra => ra.PaymentVerifiedById)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion RodeoEntryMember

            #region RodeoAssociation
            //This region holds the entity specific definition for RodeoAssociation.
            modelBuilder.Entity<RodeoAssociation>()
                .HasOne(ra => ra.Association)
                .WithMany()
                .HasForeignKey(ra => ra.AssociationId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion RodeoAssociation

            #region RodeoEntry
            modelBuilder.Entity<RodeoEntry>()
                .HasOne(c => c.FirstPreference)
                .WithMany()
                .HasForeignKey(c => c.FirstPreferenceId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RodeoEntry>()
                .HasOne(c => c.SecondPreference)
                .WithMany()
                .HasForeignKey(c => c.SecondPreferenceId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion RodeoEntry

            #region Association
            modelBuilder.Entity<Association>()
                .HasIndex(a => a.Name)
                .IsUnique()
                .HasName("UX_Association_Name");

            modelBuilder.Entity<Association>()
                .HasMany(a => a.AssociationEvents)
                .WithOne()
                .HasForeignKey(a => a.AssociationId);

            modelBuilder.Entity<Association>()
                .HasMany(a => a.Rodeos)
                .WithOne()
                .HasForeignKey(r => r.AssociationId);

            modelBuilder.Entity<Association>()
                .HasMany(a => a.Contestants)
                .WithOne()
                .HasForeignKey(r => r.AssociationId);

            modelBuilder.Entity<Association>()
                .HasIndex(a => a.Abbreviation)
                .IsUnique()
                .HasName("UX_Association_Abbreviation");
            #endregion Association

            #region Association Event
            modelBuilder.Entity<AssociationEvent>()
                .HasIndex(a => new { a.AssociationId, a.Name })
                .IsUnique()
                .HasName("UX_AssociationEvent_Name");

            modelBuilder.Entity<AssociationEvent>()
                .HasMany(re => re.RodeoEvents)
                .WithOne(re => re.AssociationEvent)
                .HasForeignKey(re => re.AssociationEventId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion Association Event

            #region Contestant
            //This region holds all of the custom configuration for the contestant entity.
            modelBuilder.Entity<Contestant>()
                .HasOne(c => c.ContestantDemographic);

            modelBuilder.Entity<Contestant>()
                .HasOne(c => c.User);

            modelBuilder.Entity<Contestant>()
                .HasOne(c => c.DateOfBirthVerifier)
                .WithMany()
                .HasForeignKey(c => c.DateOfBirthVerifiedBy);

            modelBuilder.Entity<Contestant>()
                .HasIndex(c => new { c.AssociationId, c.CardNumber })
                .IsUnique()
                .HasName("UX_Contestant_Association_CardNumber");
            #endregion Contestant

            #region EnterUpUser
            modelBuilder.Entity<EnterUpUser>()
                .HasOne(c => c.UserDemographic)
                .WithOne()
                .HasForeignKey<EnterUpUser>(d => d.DemographicId);

            modelBuilder.Entity<EnterUpUser>()
                .HasMany(eu => eu.Claims)
                .WithOne()
                .HasForeignKey(c => c.UserId);

            modelBuilder.Entity<EnterUpUser>()
                .HasMany(c => c.Claims)
                .WithOne()
                .HasForeignKey(c => c.UserId);
            #endregion EnterUpUser

            #region RodeoEvent
            //This region holds all of the custom configuration for the Rodeo Event entity.
            
            #endregion RodeoEvent

            modelBuilder.Entity<RodeoEntry>()
                .HasMany(re => re.RodeoEntryMembers)
                .WithOne()
                .HasForeignKey(re => re.RodeoEntryId);

            modelBuilder.Entity<RodeoEntryMember>()
                .HasOne(re => re.Contestant)
                .WithOne()
                .HasForeignKey<RodeoEntryMember>(re => re.ContestantId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<RodeoEntryMember>()
                .HasMany(re => re.ContestantEntries)
                .WithOne()
                .HasForeignKey(rem => rem.ContestantMemberId);

            modelBuilder.Entity<RodeoEntryMember>()
                .HasMany(re => re.TeammateEntries)
                .WithOne()
                .HasForeignKey(rem => rem.TeammateMemberId);

            #region Rodeo
            modelBuilder.Entity<Rodeo>()
                .HasMany(r => r.RodeoEvents)
                .WithOne()
                .HasForeignKey(re => re.RodeoId);

            modelBuilder.Entity<Rodeo>()
                .HasMany(r => r.Performances)
                .WithOne(p => p.Rodeo)
                .HasForeignKey(p => p.RodeoId);

            modelBuilder.Entity<Rodeo>()
                .HasMany(r => r.ValidContestantAssociation)
                .WithOne(ra => ra.Rodeo)
                .HasForeignKey(ra => ra.RodeoId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Rodeo>()
                .HasMany(r => r.RodeoEntries)
                .WithOne()
                .HasForeignKey(re => re.RodeoId);

            modelBuilder.Entity<Rodeo>()
                .HasMany(re => re.RodeoEvents)
                .WithOne(re => re.Rodeo)
                .HasForeignKey(re => re.RodeoId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion Rodeo

            #region Performance
            modelBuilder.Entity<Performance>()
                .HasIndex(p => new {p.RodeoId, p.PerformanceDate, p.IsSlack})
                .IsUnique()
                .HasName("UX_Performances_RodeoId_PerformanceDate_IsSlack");
            #endregion Performance

            //Make all decimal values to dollars and cents
            foreach (IMutableProperty property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(decimal))) {
                property.Relational().ColumnType = "decimal(18, 6)";
            }

            base.OnModelCreating(modelBuilder);
        }

        //Required for migrations to work.
        public RodeoContext() {
            ValidationHelper = new ValidationHelper(this);
        }

        #region User Data Sets
        public DbSet<EnterUpUser> Users { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<Demographic> Demographics { get; set; }
        #endregion User Data Sets

        #region Association Data Sets
        public DbSet<Association> Associations { get; set; }
        public DbSet<AssociationEvent> AssociationEvents { get; set; }
        public DbSet<Contestant> Contestants { get; set; }
        public DbSet<Rodeo> Rodeos { get; set; }
        public DbSet<RodeoAssociation> RodeoAssociations { get; set; }
        public DbSet<Performance> Performances { get; set; }
        public DbSet<RodeoEvent> RodeoEvents { get; set; }
        public DbSet<RodeoEntry> RodeoEntries { get; set; }
        public DbSet<RodeoEntryMember> RodeoEntryMembers { get; set; }
        public DbSet<RodeoEventEntry> RodeoEventEntries { get; set; }
        #endregion Association Data Sets

        #region Admin Data Sets
        public DbSet<Error> Errors { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Suggestion> Suggestions { get; set; }
        #endregion Admin Data Sets

        #region Context Methods
        public override int SaveChanges() {
            throw new EnterUpException(
                "You must supply a user name to make changes to any data.");
        }

        public int SaveChanges(string userId) {
            if (1 < 
                ChangeTracker
                    .Entries<RodeoEventEntry>()
                    .Count(e => e.State == EntityState.Added || e.State == EntityState.Modified)) {
                throw new EnterUpDataException("Only one event entry can be saved at a time.");
            }
            HandleIPermanent(userId);
            HandleUpdatedBy(userId);
            //This condition was added to allow for unit testing.
            //It will not be change
            if (!IsSkipValidation) {
                HandleIValidators(GetIValidators());
            }
            try {
                return base.SaveChanges();
            }
            catch (DbUpdateException dbUpdateException) {
                throw ExceptionWrapper.EnterUpDataExceptionFromSql(dbUpdateException);
            }
        }
        #endregion

        #region Helper 
        //Test methods in IValidatorSaveTest.cs
        public virtual IEnumerable<IValidator> GetIValidators() {
            List<EntityEntry> entries = ChangeTracker.Entries()
                .Where(e => e.Entity is IValidator
                            && (e.State == EntityState.Added || e.State == EntityState.Modified))
                .ToList();

            return entries.Select(entityEntry => entityEntry.Entity as IValidator).ToList();
        }

        public virtual void HandleIValidators(IEnumerable<IValidator> validators) {
            foreach (IValidator validator in validators) {
                validator.Validate(ValidationHelper);
            }
        }

        private void HandleUpdatedBy(string userId) {
            foreach (EntityEntry entityEntry in ChangeTracker.Entries()
                .Where(e => e.Entity is IUpdatedTracker)
                .ToList()
            ) {
                if (entityEntry.State != EntityState.Added
                    && entityEntry.State != EntityState.Modified) {
                    continue;
                }
                IUpdatedTracker entity = (IUpdatedTracker)entityEntry.Entity;
                if (entityEntry.State == EntityState.Added) {
                    entity.Created = DateTime.Now;
                    entity.CreatedBy = userId;
                }
                else {
                    if (entity.Created != (DateTime)entityEntry.OriginalValues["Created"]) {
                        entity.Created = (DateTime)entityEntry.OriginalValues["Created"];
                    }
                    if (entity.CreatedBy != (string)entityEntry.OriginalValues["CreatedBy"]) {
                        entity.CreatedBy = (string)entityEntry.OriginalValues["CreatedBy"];
                    }
                }
                entity.Updated = DateTime.Now;
                entity.UpdatedBy = userId;
            }
        }

        private void HandleIPermanent(string userId) {
            List<EntityEntry> entries = ChangeTracker.Entries()
                .Where(e => e.Entity is IPermanent && e.State == EntityState.Deleted)
                .ToList();

            foreach (EntityEntry entry in entries) {
                IPermanent entity = entry.Entity as IPermanent;
                // ReSharper disable once PossibleNullReferenceException
                entity.DeletedById = userId;
                entry.State = EntityState.Modified;
            }
        }
        #endregion Helper Methods
    }
}
