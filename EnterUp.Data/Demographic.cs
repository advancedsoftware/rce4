﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterUp.Data.Interfaces;

namespace EnterUp.Data {
    public class Demographic : IUpdatedTracker, IPermanent, IValidator {
        public Demographic() {
            Address1 = string.Empty;
            Address2 = string.Empty;
            CellPhone = string.Empty;
            City = string.Empty;
            FirstName = string.Empty;
            HomePhone = string.Empty;
            LastName = string.Empty;
            State = string.Empty;
            Zipcode = string.Empty;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "The first name is required and must be between 1 and 50 characters.")]
        [MaxLength(50, ErrorMessage = "The first name is required and must be between 1 and 50 characters.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "The last name is required and must be between 1 and 50 characters.")]
        [MaxLength(50, ErrorMessage = "The last name is required and must be between 1 and 50 characters.")]
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [MaxLength(100, ErrorMessage = "The maximum length of the Address1 property is 100 characters.")]
        public string Address1 { get; set; }
        [MaxLength(100, ErrorMessage = "The maximum length of the Address2 property is 100 characters.")]
        public string Address2 { get; set; }
        [MaxLength(100, ErrorMessage = "The maximum length of the City property is 100 characters.")]
        public string City { get; set; }
        [MaxLength(5, ErrorMessage = "The maximum length of the State property is 5 characters.")]
        public string State { get; set; }
        [MaxLength(12, ErrorMessage = "The maximum length of the Zipcode property is 12 characters.")]
        public string Zipcode { get; set; }
        [MaxLength(25, ErrorMessage = "The maximum length of the HomePhone property is 25 characters.")]
        public string HomePhone { get; set; }
        [MaxLength(25, ErrorMessage = "The maximum length of the CellPhone property is 25 characters.")]
        public string CellPhone { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public string DeletedById { get; set; }

        public EnterUpUser CreatedByUser() {
            return Helpers.GetUser(CreatedBy);
        }
        public EnterUpUser UpdatedByUser() {
            return Helpers.GetUser(UpdatedBy);
        }

        public EnterUpUser DeletedByUser() {
            return Helpers.GetUser(DeletedById);
        }

        [NotMapped]
        public bool IsDeleted => !string.IsNullOrEmpty(DeletedById);

        public void Validate(IValidationHelper helper) {
            if (string.IsNullOrEmpty(Address1)) {
                Address1 = string.Empty;
            }
            if (string.IsNullOrEmpty(Address2)) {
                Address2 = string.Empty;
            }
            if (string.IsNullOrEmpty(Address1)) {
                Address1 = string.Empty;
            }
            if (string.IsNullOrEmpty(CellPhone)) {
                CellPhone = string.Empty;
            }
            if (string.IsNullOrEmpty(City)) {
                City = string.Empty;
            }
            if (string.IsNullOrEmpty(HomePhone)) {
                HomePhone = string.Empty;
            }
            if (string.IsNullOrEmpty(State)) {
                State = string.Empty;
            }
            if (string.IsNullOrEmpty(Zipcode)) {
                Zipcode = string.Empty;
            }
        }
    }
}