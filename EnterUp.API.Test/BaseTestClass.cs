﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EnterUp.Data;
using EnterUp.Test.Common;
using Microsoft.AspNetCore.Mvc;

namespace EnterUp.API.Test
{
    public class BaseTestClass : MockDbTestBase, IDisposable {
        public RodeoContext MockDataContext => MockContext();

        public void Dispose() {
            MockDataContext?.Dispose();
        }
        public void SimulateValidation(object model, Controller controller) {
            // mimic the behaviour of the model binder which is responsible for Validating the Model
            ValidationContext validationContext = new ValidationContext(model, null, null);
            List<ValidationResult> validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(model, validationContext, validationResults, true);
            foreach (ValidationResult validationResult in validationResults) {
                controller.ModelState.AddModelError(validationResult.MemberNames.First(), validationResult.ErrorMessage);
            }
        }
    }
}
