﻿using System.Collections.Generic;
using System.Linq;
using EnterUp.API.Controllers;
using EnterUp.Data;
using Xunit;

namespace EnterUp.API.Test.AssociationControllerTests
{
    // ReSharper disable once InconsistentNaming
    public class AssociationsController_GetAll_Tests : BaseTestClass {
        #region Setup
        protected AssociationsController Controller { get; set; }
        public AssociationsController_GetAll_Tests() {
            Controller = new AssociationsController(MockDataContext);
            FillDataContext();
        }

        private void FillDataContext() {
            using (RodeoContext context = MockContext()) {
                context.Associations.AddRange(new List<Association> {
                    new Association {
                        Id = 1,
                        IsActive = true
                    },
                    new Association {
                        Id = 2,
                        IsActive = true
                    },
                    new Association {
                        Id = 3,
                        IsActive = false
                    },
                    new Association {
                        Id = 4,
                        IsActive = true
                    },
                    new Association {
                        Id = 5,
                        IsActive = true
                    }
                });
                context.SaveChanges("1");
            }
        }
        #endregion Setup

        [Fact]
        public void All_Active_Associations_Returned() {
            Assert.Equal(4, Controller.GetAll().Count());
        }

        [Fact]
        public void All_Returned_When_IncludeInActive() {
            Assert.Equal(5, Controller.GetAll(true).Count());
        }

        [Fact]
        public void Only_Inactive_Returned_When_Not_IncludeInActive() {
            Assert.Equal(4, Controller.GetAll(false).Count());
        }
    }
}
