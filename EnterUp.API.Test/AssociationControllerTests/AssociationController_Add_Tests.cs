﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using EnterUp.API.Controllers;
using EnterUp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace EnterUp.API.Test.AssociationControllerTests {
    // ReSharper disable once InconsistentNaming
    public class AssociationsController_Add_Tests : BaseTestClass {
        #region Setup

        protected AssociationsController Controller { get; set; }
        protected Association Test { get; set; }

        public AssociationsController_Add_Tests() {
            Controller = new AssociationsController(MockDataContext) {
                ControllerContext = new ControllerContext {
                    HttpContext = new DefaultHttpContext()
                }
            };
            Controller.HttpContext.User = new GenericPrincipal(new GenericIdentity("Name"), new string[0]);
            Test = new Association {
                Id = 0,
                Abbreviation = "TA",
                ContactEmail = "Test@RodeoCentralEntry.com",
                ContactName = "Test Contact",
                ContactOrganization = "Rodeo Central",
                ContactPhone = "(208) 284-1007",
                EntryPhone = "(208) 284-1007",
                MaximumBuddies = 4,
                MemberDrawPriority = 1,
                MemberSchedulingPriority = 1,
                PermitDrawPriority = 2,
                PermitSchedulingPriority = 2,
                NonMemberDrawPriority = 4,
                NonMemberSchedulingPriority = 4,
                Name = "Test Association"
            };
        }
        #endregion Setup

        [Fact]
        public async Task Invalid_Object_Returns_400() {
            Test.Name = string.Empty;
            SimulateValidation(Test, Controller);
            IActionResult result = await Controller.Add(Test);
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
