﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EnterUp.API.Controllers;
using EnterUp.Data;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using NotFoundResult = Microsoft.AspNetCore.Mvc.NotFoundResult;

namespace EnterUp.API.Test.AssociationControllerTests {
    // ReSharper disable once InconsistentNaming
    public class AssociationsController_Get_Tests : BaseTestClass {
        #region Setup

        protected AssociationsController Controller { get; set; }

        public AssociationsController_Get_Tests() {
            Controller = new AssociationsController(MockDataContext);
            FillDataContext();
        }

        private void FillDataContext() {
            using (RodeoContext context = MockContext()) {
                context.Associations.AddRange(new List<Association> {
                    new Association {
                        Id = 1,
                        IsActive = true
                    },
                    new Association {
                        Id = 2,
                        IsActive = true
                    },
                    new Association {
                        Id = 3,
                        IsActive = false
                    },
                    new Association {
                        Id = 4,
                        IsActive = true
                    },
                    new Association {
                        Id = 5,
                        IsActive = true
                    }
                });
                context.SaveChanges("1");
            }
        }

        #endregion Setup

        [Fact]
        public async Task Valid_Id_Association_Returned() {
            IActionResult result = await Controller.Get(2);
            Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(result as OkObjectResult);
            Association association = (result as OkObjectResult).Value as Association;
            Assert.NotNull(association);
            Assert.Equal(2, association.Id);
        }

        [Fact]
        public async Task Invalid_Id_404_Returned() {
            IActionResult result = await Controller.Get(20);
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
