﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EnterUp.Data;
using Microsoft.EntityFrameworkCore;

namespace EnterUp.API.Controllers {
    [Produces("application/json")]
    [Route("api/Association")]
    public class AssociationsController : BaseController {
        public AssociationsController(RodeoContext rodeoContext) : base(rodeoContext) {
        }

        // GET: api/Association
        [HttpGet("{includeInActive?}")]
        public IEnumerable<Association> GetAll(bool includeInActive = false) {
            IQueryable<Association> associationsQueryable = DataContext.Associations;
            if (!includeInActive) {
                associationsQueryable = associationsQueryable.Where(a => a.IsActive);
            }
            return associationsQueryable.ToList();
        }

        //GET: api/Association/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id) {
            Association association = await DataContext.Associations.FirstOrDefaultAsync(m => m.Id == id);

            if (association == null) {
                return NotFound();
            }
            return Ok(association);
        }

        //// PUT: api/Association/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutAssociation([FromRoute] int id, [FromBody] Association association) {
        //    if (!ModelState.IsValid) {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != association.Id) {
        //        return BadRequest();
        //    }

        //    context.Entry(association).State = EntityState.Modified;

        //    try {
        //        await context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException) {
        //        if (!AssociationExists(id)) {
        //            return NotFound();
        //        }
        //        else {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Association
        //This is the method that adds a new record to the database.
        //If the Id is not zero we should return a 405.
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Association association) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            throw new NotImplementedException();
            DataContext.Associations.Add(association);
            await DataContext.SaveChangesAsync();

            return CreatedAtAction("Get", new { id = association.Id }, association);
        }

        //// DELETE: api/Association/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteAssociation([FromRoute] int id) {
        //    if (!ModelState.IsValid) {
        //        return BadRequest(ModelState);
        //    }

        //    var association = await context.Associations.SingleOrDefaultAsync(m => m.Id == id);
        //    if (association == null) {
        //        return NotFound();
        //    }

        //    context.Associations.Remove(association);
        //    await context.SaveChangesAsync();

        //    return Ok(association);
        //}

        //private bool AssociationExists(int id) {
        //    return context.Associations.Any(e => e.Id == id);
        //}
    }
}