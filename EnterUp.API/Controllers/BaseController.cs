﻿using EnterUp.Data;
using Microsoft.AspNetCore.Mvc;

namespace EnterUp.API.Controllers
{
    public class BaseController : Controller {
        protected RodeoContext DataContext { get; set; }

        public BaseController(RodeoContext context) {
            DataContext = context;
        }
    }
}